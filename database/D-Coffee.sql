--
-- File generated with SQLiteStudio v3.4.4 on Mon Oct 23 23:30:24 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: BRANCH
CREATE TABLE IF NOT EXISTS BRANCH (branch_code TEXT NOT NULL UNIQUE, branch_locate TEXT NOT NULL, branch_tel TEXT NOT NULL, PRIMARY KEY (branch_code));
INSERT INTO BRANCH (branch_code, branch_locate, branch_tel) VALUES ('BUU', 'ถ.ลงหาดบางแสน ต.แสนสุข อ.เมือง จ.ชลบุรี 20131', '02212224');
INSERT INTO BRANCH (branch_code, branch_locate, branch_tel) VALUES ('CENTRALCHON', 'ถ.สุขุมวิท ต.เสม็ด อ.เมือง จ.ชลบุรี 20000', '02312345');
INSERT INTO BRANCH (branch_code, branch_locate, branch_tel) VALUES ('SIAMPARAGON', 'แขวงปทุมวัน เขตปทุมวัน กรุงเทพมหานคร 10330', '02412456');
INSERT INTO BRANCH (branch_code, branch_locate, branch_tel) VALUES ('ICONSIAM', 'แขวงคลองต้นไทร เขตคลองสาน กรุงเทพมหานคร 10600', '02412567');
INSERT INTO BRANCH (branch_code, branch_locate, branch_tel) VALUES ('FUTUREPARK', 'ถ.พหลโยธิน ตําบล ประชาธิปัตย์ อำเภอธัญบุรี ปทุมธานี 12130', '02412345');

-- Table: CHECK_INOUT
CREATE TABLE IF NOT EXISTS CHECK_INOUT(
    checkTime_id INTEGER NOT NULL UNIQUE,
    checkTime_date TEXT NOT NULL,
    employee_id INTEGER NOT NULL,
    checkTime_in TEXT NOT NULL,
    checkTime_out TEXT NOT NULL,
    checkTime_totalWorked INTEGER NOT NULL,
    checkTime_type TEXT NOT NULL,
    PRIMARY KEY (checkTime_id),
    FOREIGN KEY (employee_id) REFERENCES EMPLOYEE
);
INSERT INTO CHECK_INOUT (checkTime_id, checkTime_date, employee_id, checkTime_in, checkTime_out, checkTime_totalWorked, checkTime_type) VALUES (1, '2021/02/03', 1, '10:00:00', '18:00:00', 8, 'normal');
INSERT INTO CHECK_INOUT (checkTime_id, checkTime_date, employee_id, checkTime_in, checkTime_out, checkTime_totalWorked, checkTime_type) VALUES (2, '2021/02/03', 2, '10:00:00', '16:00:00', 6, 'normal');
INSERT INTO CHECK_INOUT (checkTime_id, checkTime_date, employee_id, checkTime_in, checkTime_out, checkTime_totalWorked, checkTime_type) VALUES (3, '2021/02/03', 3, '10:00:00', '18:00:00', 8, 'normal');
INSERT INTO CHECK_INOUT (checkTime_id, checkTime_date, employee_id, checkTime_in, checkTime_out, checkTime_totalWorked, checkTime_type) VALUES (4, '2021/02/04', 2, '11:00:00', '20:00:00', 9, 'normal/OT');
INSERT INTO CHECK_INOUT (checkTime_id, checkTime_date, employee_id, checkTime_in, checkTime_out, checkTime_totalWorked, checkTime_type) VALUES (5, '2021/02/04', 4, '10:00:00', '18:00:00', 8, 'normal');
INSERT INTO CHECK_INOUT (checkTime_id, checkTime_date, employee_id, checkTime_in, checkTime_out, checkTime_totalWorked, checkTime_type) VALUES (6, '2021/02/04', 5, '11:00:00', '17:00:00', 6, 'normal');
INSERT INTO CHECK_INOUT (checkTime_id, checkTime_date, employee_id, checkTime_in, checkTime_out, checkTime_totalWorked, checkTime_type) VALUES (7, '2023/01/05', 2, '09:00:00', '17:00:00', 8, 'normal');
INSERT INTO CHECK_INOUT (checkTime_id, checkTime_date, employee_id, checkTime_in, checkTime_out, checkTime_totalWorked, checkTime_type) VALUES (8, '2023/08/01', 5, '10:00:00', '20:00:00', 10, 'normal/OT');
INSERT INTO CHECK_INOUT (checkTime_id, checkTime_date, employee_id, checkTime_in, checkTime_out, checkTime_totalWorked, checkTime_type) VALUES (9, '2023/08/02', 7, '11:00:00', '17:00:00', 6, 'normal');

-- Table: CHECK_STOCK
CREATE TABLE IF NOT EXISTS CHECK_STOCK (checkStock_id INTEGER NOT NULL UNIQUE, employee_id INTEGER NOT NULL, checkStock_description TEXT NOT NULL, checkStock_valueLoss INTEGER, checkStock_unitLoss INTEGER, checkStock_totalUnit INTEGER NOT NULL, checkStock_date TEXT NOT NULL, PRIMARY KEY (checkStock_id AUTOINCREMENT), FOREIGN KEY (employee_id) REFERENCES EMPLOYEE);
INSERT INTO CHECK_STOCK (checkStock_id, employee_id, checkStock_description, checkStock_valueLoss, checkStock_unitLoss, checkStock_totalUnit, checkStock_date) VALUES (1, 2, 'ตรวจสต็อกวัตถุดิบไม่พอใช้', 300, 0, 20, '2021/02/03');
INSERT INTO CHECK_STOCK (checkStock_id, employee_id, checkStock_description, checkStock_valueLoss, checkStock_unitLoss, checkStock_totalUnit, checkStock_date) VALUES (2, 2, 'ตรวจสต็อกวัตถุดิบหมดอายุ', 120, 15, 15, '2021/02/03');
INSERT INTO CHECK_STOCK (checkStock_id, employee_id, checkStock_description, checkStock_valueLoss, checkStock_unitLoss, checkStock_totalUnit, checkStock_date) VALUES (3, 4, 'ตรวจสต็อกวัตถุดิบไม่พอใช้', 320, 0, 12, '2022/02/20');
INSERT INTO CHECK_STOCK (checkStock_id, employee_id, checkStock_description, checkStock_valueLoss, checkStock_unitLoss, checkStock_totalUnit, checkStock_date) VALUES (4, 4, 'ตรวจสต็อกวัตถุดิบหมดอายุ', 150, 20, 20, '2022/02/22');
INSERT INTO CHECK_STOCK (checkStock_id, employee_id, checkStock_description, checkStock_valueLoss, checkStock_unitLoss, checkStock_totalUnit, checkStock_date) VALUES (5, 1, 'ตรวจสต็อกวัตถุดิบหมดอายุ', 250, 20, 20, '2023/08/21');
INSERT INTO CHECK_STOCK (checkStock_id, employee_id, checkStock_description, checkStock_valueLoss, checkStock_unitLoss, checkStock_totalUnit, checkStock_date) VALUES (6, 3, 'ตรรวจสต็อกวัตถุดิบไม่พอใช้', 220, 2, 15, '2023/08/21');
INSERT INTO CHECK_STOCK (checkStock_id, employee_id, checkStock_description, checkStock_valueLoss, checkStock_unitLoss, checkStock_totalUnit, checkStock_date) VALUES (7, 1, '4', 200, 12, 12, '2023/09/09');

-- Table: CHECK_STOCK_DETAIL
CREATE TABLE IF NOT EXISTS CHECK_STOCK_DETAIL (checkStockDetail_id INTEGER NOT NULL UNIQUE, ingrediant_id INTEGER NOT NULL REFERENCES INGREDIENT (ingredient_id), checkStock_id INTEGER NOT NULL, checkStockDetail_description TEXT NOT NULL, checkStockDetail_quantity INTEGER NOT NULL, checkStockDetail_unitExpire INTEGER NOT NULL, checkStockDetail_valueLoss INTEGER NOT NULL, checkStockDetail_date TEXT, PRIMARY KEY (checkStockDetail_id AUTOINCREMENT), FOREIGN KEY (ingrediant_id) REFERENCES INGREDIENT (ingredient_id), FOREIGN KEY (checkStock_id) REFERENCES CHECK_STOCK (checkStock_id));
INSERT INTO CHECK_STOCK_DETAIL (checkStockDetail_id, ingrediant_id, checkStock_id, checkStockDetail_description, checkStockDetail_quantity, checkStockDetail_unitExpire, checkStockDetail_valueLoss, checkStockDetail_date) VALUES (1, 1, 1, 'ถุงผลชาเขียวมีน้อยกว่าจำนวนที่ต้องใช้', 5, 0, 150, '2021/02/03');
INSERT INTO CHECK_STOCK_DETAIL (checkStockDetail_id, ingrediant_id, checkStock_id, checkStockDetail_description, checkStockDetail_quantity, checkStockDetail_unitExpire, checkStockDetail_valueLoss, checkStockDetail_date) VALUES (2, 2, 1, 'ถุงเมล็ดกาแฟมีน้อยกว่าจำนวนที่ต้องใช้', 5, 0, 150, '2021/02/03');
INSERT INTO CHECK_STOCK_DETAIL (checkStockDetail_id, ingrediant_id, checkStock_id, checkStockDetail_description, checkStockDetail_quantity, checkStockDetail_unitExpire, checkStockDetail_valueLoss, checkStockDetail_date) VALUES (3, 7, 2, 'ไข่มุกหมดอายุ', 15, 15, 120, '2021/02/03');
INSERT INTO CHECK_STOCK_DETAIL (checkStockDetail_id, ingrediant_id, checkStock_id, checkStockDetail_description, checkStockDetail_quantity, checkStockDetail_unitExpire, checkStockDetail_valueLoss, checkStockDetail_date) VALUES (4, 3, 3, 'ผงกาแฟมีน้อยกว่าจำนวนที่ต้องใช้', 6, 0, 160, '2023/08/21');
INSERT INTO CHECK_STOCK_DETAIL (checkStockDetail_id, ingrediant_id, checkStock_id, checkStockDetail_description, checkStockDetail_quantity, checkStockDetail_unitExpire, checkStockDetail_valueLoss, checkStockDetail_date) VALUES (5, 4, 3, 'ผงชามีน้อยกว่าจำนวนที่ต้องใช้', 6, 0, 160, '2023/08/21');
INSERT INTO CHECK_STOCK_DETAIL (checkStockDetail_id, ingrediant_id, checkStock_id, checkStockDetail_description, checkStockDetail_quantity, checkStockDetail_unitExpire, checkStockDetail_valueLoss, checkStockDetail_date) VALUES (6, 5, 4, 'นมจืดหมดอายุ', 10, 10, 70, '2023/08/21');
INSERT INTO CHECK_STOCK_DETAIL (checkStockDetail_id, ingrediant_id, checkStock_id, checkStockDetail_description, checkStockDetail_quantity, checkStockDetail_unitExpire, checkStockDetail_valueLoss, checkStockDetail_date) VALUES (7, 6, 4, 'นมข้นหวานหมดอายุ', 10, 10, 80, '2023/08/21');

-- Table: CUSTOMER
CREATE TABLE IF NOT EXISTS CUSTOMER (customer_id INTEGER NOT NULL UNIQUE, customer_status TEXT NOT NULL, customer_name TEXT NOT NULL, customer_lastname TEXT NOT NULL, customer_tel TEXT NOT NULL, customer_birth_date TEXT NOT NULL, customer_point INTEGER NOT NULL, customer_join_date TEXT NOT NULL, PRIMARY KEY (customer_id AUTOINCREMENT));
INSERT INTO CUSTOMER (customer_id, customer_status, customer_name, customer_lastname, customer_tel, customer_birth_date, customer_point, customer_join_date) VALUES (1, 'active', 'Peter', 'Parker', '0971543251', '2002/02/02', 35, '2021/01/01');
INSERT INTO CUSTOMER (customer_id, customer_status, customer_name, customer_lastname, customer_tel, customer_birth_date, customer_point, customer_join_date) VALUES (2, 'active', 'John', 'Doe', '0831517582', '2000/01/01', 60, '2021/03/15');
INSERT INTO CUSTOMER (customer_id, customer_status, customer_name, customer_lastname, customer_tel, customer_birth_date, customer_point, customer_join_date) VALUES (3, 'active', 'Satoru', 'Gojo', '0874942523', '1989/12/02', 75, '2020/12/01');
INSERT INTO CUSTOMER (customer_id, customer_status, customer_name, customer_lastname, customer_tel, customer_birth_date, customer_point, customer_join_date) VALUES (4, 'active', 'Suguru', 'Geto', '0925143251', '1989/10/25', 40, '2020/12/05');
INSERT INTO CUSTOMER (customer_id, customer_status, customer_name, customer_lastname, customer_tel, customer_birth_date, customer_point, customer_join_date) VALUES (5, 'active', 'Aii', 'Hoshino', '0972518923', '2003/04/01', 85, '2022/04/10');
INSERT INTO CUSTOMER (customer_id, customer_status, customer_name, customer_lastname, customer_tel, customer_birth_date, customer_point, customer_join_date) VALUES (6, 'active', 'Loid', 'Forger', '0853736255', '1997/03/06', 30, '2022/11/13');
INSERT INTO CUSTOMER (customer_id, customer_status, customer_name, customer_lastname, customer_tel, customer_birth_date, customer_point, customer_join_date) VALUES (7, 'active', 'Ben', 'Tennyson', '0814225898', '1998/12/27', 70, '2021/01/12');

-- Table: EMPLOYEE
CREATE TABLE IF NOT EXISTS EMPLOYEE (employee_id INTEGER NOT NULL UNIQUE, branch_code TEXT NOT NULL, employee_title TEXT NOT NULL, employee_name TEXT NOT NULL, employee_lastname TEXT NOT NULL, employee_tel TEXT NOT NULL, employee_birthDate TEXT NOT NULL, employee_minWork INTEGER NOT NULL, employee_qualification TEXT NOT NULL, employee_startDate TEXT NOT NULL, employee_moneyRate REAL NOT NULL, PRIMARY KEY (employee_id AUTOINCREMENT), FOREIGN KEY (branch_code) REFERENCES BRANCH);
INSERT INTO EMPLOYEE (employee_id, branch_code, employee_title, employee_name, employee_lastname, employee_tel, employee_birthDate, employee_minWork, employee_qualification, employee_startDate, employee_moneyRate) VALUES (1, 'BUU', 'เจ้าของร้าน', 'Jack', 'Maxman', '0888888888', '2003/10/05', 8, 'ปริญญาโท', '2020/10/05', 1000.0);
INSERT INTO EMPLOYEE (employee_id, branch_code, employee_title, employee_name, employee_lastname, employee_tel, employee_birthDate, employee_minWork, employee_qualification, employee_startDate, employee_moneyRate) VALUES (2, 'BUU', 'พนักงาน', 'Tony', 'Stark', '0812345680', '2003/08/03', 8, 'ปริญญาตรี', '2020/11/15', 450.0);
INSERT INTO EMPLOYEE (employee_id, branch_code, employee_title, employee_name, employee_lastname, employee_tel, employee_birthDate, employee_minWork, employee_qualification, employee_startDate, employee_moneyRate) VALUES (3, 'BUU', 'พนักงาน', 'Walt', 'Disney', '0812345679', '2000/02/15', 6, 'ปริญญาตรี', '2020/12/05', 450.0);
INSERT INTO EMPLOYEE (employee_id, branch_code, employee_title, employee_name, employee_lastname, employee_tel, employee_birthDate, employee_minWork, employee_qualification, employee_startDate, employee_moneyRate) VALUES (4, 'BUU', 'พนักงาน', 'David', 'King', '0812345681', '1998/05/18', 8, 'ปวส.', '2021/01/05', 400.0);
INSERT INTO EMPLOYEE (employee_id, branch_code, employee_title, employee_name, employee_lastname, employee_tel, employee_birthDate, employee_minWork, employee_qualification, employee_startDate, employee_moneyRate) VALUES (5, 'BUU', 'พนักงาน', 'Ran', 'Mouri', '0812345682', '2000/05/04', 6, 'ปริญญาตรี', '2021/01/15', 400.0);
INSERT INTO EMPLOYEE (employee_id, branch_code, employee_title, employee_name, employee_lastname, employee_tel, employee_birthDate, employee_minWork, employee_qualification, employee_startDate, employee_moneyRate) VALUES (6, 'BUU', 'พนักงาน', ' John', 'Cena', '0812345683', '2001/04/23', 6, 'ปริญญาตรี', '2023/07/17', 400.0);
INSERT INTO EMPLOYEE (employee_id, branch_code, employee_title, employee_name, employee_lastname, employee_tel, employee_birthDate, employee_minWork, employee_qualification, employee_startDate, employee_moneyRate) VALUES (7, 'BUU', 'พนักงาน', 'Bruce', 'Wayne', '0812345684', '2000/04/07', 6, 'ปริญญาตรี', '2023/07/17', 400.0);
INSERT INTO EMPLOYEE (employee_id, branch_code, employee_title, employee_name, employee_lastname, employee_tel, employee_birthDate, employee_minWork, employee_qualification, employee_startDate, employee_moneyRate) VALUES (10, 'BUU', 'พนักงาน', 'ศุภกร', 'แสงจิต', '0970850207', '2003/11/13', 8, 'มัธยมศึกษาปีที่ 6', '2023/01/01', 400.0);

-- Table: INGREDIENT
CREATE TABLE IF NOT EXISTS INGREDIENT (ingredient_id INTEGER NOT NULL UNIQUE, ingredient_name TEXT NOT NULL, ingredient_minNeed INTEGER NOT NULL, ingredient_quantity INTEGER NOT NULL, ingredient_value REAL NOT NULL, PRIMARY KEY (ingredient_id AUTOINCREMENT));
INSERT INTO INGREDIENT (ingredient_id, ingredient_name, ingredient_minNeed, ingredient_quantity, ingredient_value) VALUES (1, 'ผงชาเขียว', 10, 5, 0.0);
INSERT INTO INGREDIENT (ingredient_id, ingredient_name, ingredient_minNeed, ingredient_quantity, ingredient_value) VALUES (2, 'เมล็ดกาแฟ', 10, 5, 0.0);
INSERT INTO INGREDIENT (ingredient_id, ingredient_name, ingredient_minNeed, ingredient_quantity, ingredient_value) VALUES (3, 'ผงกาแฟ', 10, 14, 0.0);
INSERT INTO INGREDIENT (ingredient_id, ingredient_name, ingredient_minNeed, ingredient_quantity, ingredient_value) VALUES (4, 'ผงชา', 10, 10, 0.0);
INSERT INTO INGREDIENT (ingredient_id, ingredient_name, ingredient_minNeed, ingredient_quantity, ingredient_value) VALUES (5, 'นมจืด', 10, 12, 0.0);
INSERT INTO INGREDIENT (ingredient_id, ingredient_name, ingredient_minNeed, ingredient_quantity, ingredient_value) VALUES (6, 'นมข้นหวาน', 10, 8, 0.0);
INSERT INTO INGREDIENT (ingredient_id, ingredient_name, ingredient_minNeed, ingredient_quantity, ingredient_value) VALUES (7, 'ไข่มุก', 10, 13, 0.0);
INSERT INTO INGREDIENT (ingredient_id, ingredient_name, ingredient_minNeed, ingredient_quantity, ingredient_value) VALUES (8, 'ผงโกโก้', 10, 6, 0.0);
INSERT INTO INGREDIENT (ingredient_id, ingredient_name, ingredient_minNeed, ingredient_quantity, ingredient_value) VALUES (29, 'น้ำตาล', 5, 5, 5.0);

-- Table: INVOICE
CREATE TABLE IF NOT EXISTS INVOICE (invoice_id INTEGER NOT NULL UNIQUE, customer_id INTEGER NOT NULL, employee_id INTEGER NOT NULL, branch_code TEXT NOT NULL, promotion_id INTEGER, invoice_date DATETIME NOT NULL, invoice_totalUnit INTEGER NOT NULL, invoice_totalPrice REAL NOT NULL, invoice_discount REAL NOT NULL, invoice_netPrice REAL NOT NULL, invoice_moneyReceive REAL NOT NULL, invoice_moneyChange REAL NOT NULL, PRIMARY KEY (invoice_id AUTOINCREMENT), FOREIGN KEY (customer_id) REFERENCES CUSTOMER (customer_id) ON DELETE RESTRICT ON UPDATE CASCADE, FOREIGN KEY (employee_id) REFERENCES EMPLOYEE (employee_id) ON DELETE RESTRICT ON UPDATE CASCADE, FOREIGN KEY (branch_code) REFERENCES BRANCH (branch_code) ON DELETE RESTRICT ON UPDATE CASCADE, FOREIGN KEY (promotion_id) REFERENCES PROMOTION (promotion_id) ON DELETE RESTRICT ON UPDATE CASCADE);
INSERT INTO INVOICE (invoice_id, customer_id, employee_id, branch_code, promotion_id, invoice_date, invoice_totalUnit, invoice_totalPrice, invoice_discount, invoice_netPrice, invoice_moneyReceive, invoice_moneyChange) VALUES (2, -1, 1, 'BUU', NULL, '2023/10/23 23:11:08', 7, 350.0, 0.0, 350.0, 1000.0, -650.0);

-- Table: INVOICE_DETAIL
CREATE TABLE IF NOT EXISTS INVOICE_DETAIL (invoiceDetail_id INTEGER NOT NULL UNIQUE, product_id INTEGER, invoice_id INTEGER REFERENCES INVOICE (invoice_id) ON DELETE CASCADE ON UPDATE CASCADE, invoiceDetail_unit INTEGER NOT NULL, invoiceDetail_unitPrice REAL NOT NULL, invoiceDetail_discount REAL NOT NULL, invoiceDetail_netPrice REAL NOT NULL, PRIMARY KEY (invoiceDetail_id AUTOINCREMENT), FOREIGN KEY (product_id) REFERENCES PRODUCT);

-- Table: PRODUCT
CREATE TABLE IF NOT EXISTS PRODUCT (product_id INTEGER NOT NULL, product_name TEXT NOT NULL, product_price REAL NOT NULL, product_size TEXT NOT NULL, product_quantity INTEGER, product_category TEXT NOT NULL, product_subCategory TEXT NOT NULL, PRIMARY KEY (product_id AUTOINCREMENT));
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (1, 'กาแฟร้อน', 45.0, 'L', NULL, 'Drink', 'Hot');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (2, 'เอสเปรสโซ่เย็น', 50.0, 'L', NULL, 'Drink', 'Cold');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (3, 'คาปูชิโน่เย็น', 50.0, 'L', NULL, 'Drink', 'Cold');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (4, 'มอคค่าเย็น', 50.0, 'L', NULL, 'Drink', 'Cold');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (5, 'ลาเต้เย็น', 50.0, 'L', NULL, 'Drink', 'Cold');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (6, 'ชาเขียวเย็น', 40.0, 'L', NULL, 'Drink', 'Cold');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (7, 'ชาเย็น', 40.0, 'L', NULL, 'Drink', 'Cold');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (8, 'นมชมพูเย็น', 40.0, 'L', NULL, 'Drink', 'Cold');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (9, 'นมสด', 35.0, 'L', NULL, 'Drink', 'Cold');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (10, 'โอวัลตินเย็น', 40.0, 'L', NULL, 'Drink', 'Cold');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (11, 'ชานม', 40.0, 'L', NULL, 'Drink', 'Cold');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (12, 'โกโก้', 40.0, 'L', NULL, 'Drink', 'Cold');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (13, 'น้ำแดง', 35.0, 'L', NULL, 'Drink', 'Cold');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (14, 'โอเลี้ยง', 35.0, 'L', NULL, 'Drink', 'Cold');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (15, 'ชามะนาว', 35.0, 'L', NULL, 'Drink', 'Cold');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (16, 'มิ้นท์ช็อค', 40.0, 'L', NULL, 'Drink', 'Cold');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (17, 'เอสเปรสโซ่ร้อน', 45.0, 'L', NULL, 'Drink', 'Hot');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (18, 'คาปูชิโน่ร้อน', 45.0, 'L', NULL, 'Drink', 'Hot');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (19, 'มอคค่าร้อน', 45.0, 'L', NULL, 'Drink', 'Hot');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (20, 'ลาเต้ร้อน', 45.0, 'L', NULL, 'Drink', 'Hot');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (21, 'เค้กชิฟฟ่อนช็อกโกแลต', 40.0, '-', NULL, 'Bakery', '-');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (22, 'แซนด์วิช', 30.0, '-', NULL, 'Bakery', '-');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (23, 'บัตเตอร์เค้ก', 50.0, '-', NULL, 'Bakery', '-');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (24, 'ขนมปัง', 20.0, '-', NULL, 'Bakery', '-');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (25, 'ครัวซองต์', 35.0, '-', NULL, 'Bakery', '-');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (26, 'คัพเค้ก', 35.0, '-', NULL, 'Bakery', '-');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (27, 'คุกกี้', 25.0, '-', NULL, 'Bakery', '-');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (28, 'เค้ก', 120.0, '-', NULL, 'Bakery', '-');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (29, 'พายสับปะรด', 35.0, '-', NULL, 'Bakery', '-');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (30, 'มาการอง', 45.0, '-', NULL, 'Bakery', '-');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (31, '', 0.0, '', NULL, '', '');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (32, '', 0.0, '', NULL, '', '');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (33, '', 0.0, '', NULL, '', '');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (34, '', 0.0, '', NULL, '', '');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (35, '', 0.0, '', NULL, '', '');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (36, '', 0.0, '', NULL, '', '');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (37, '', 0.0, '', NULL, '', '');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (38, '', 0.0, '', NULL, '', '');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (39, '', 0.0, '', NULL, '', '');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (40, '', 0.0, '', NULL, '', '');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (41, '', 0.0, '', NULL, '', '');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (42, '', 0.0, '', NULL, '', '');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (43, '', 0.0, '', NULL, '', '');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (44, '', 0.0, '', NULL, '', '');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (45, '', 0.0, '', NULL, '', '');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (46, '', 0.0, '', NULL, '', '');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (47, '', 0.0, '', NULL, '', '');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (48, '', 0.0, '', NULL, '', '');
INSERT INTO PRODUCT (product_id, product_name, product_price, product_size, product_quantity, product_category, product_subCategory) VALUES (49, '', 0.0, '', NULL, '', '');

-- Table: PROMOTION
CREATE TABLE IF NOT EXISTS PROMOTION (promotion_id INTEGER NOT NULL UNIQUE, promotion_name TEXT NOT NULL, promotion_description TEXT NOT NULL, promotion_discount REAL, promotion_endDate TEXT NOT NULL, promotion_status TEXT NOT NULL, promotion_startDate TEXT NOT NULL, PRIMARY KEY (promotion_id AUTOINCREMENT));
INSERT INTO PROMOTION (promotion_id, promotion_name, promotion_description, promotion_discount, promotion_endDate, promotion_status, promotion_startDate) VALUES (1, 'Points', 'ลดราคาด้วยแต้มสมาชิก', 0.0, '2021/01/01', 'active', '2021/01/01');
INSERT INTO PROMOTION (promotion_id, promotion_name, promotion_description, promotion_discount, promotion_endDate, promotion_status, promotion_startDate) VALUES (2, 'Birthday', 'ลดราคาในวันเกิดของสมาชิก', 0.5, '2023/01/01', 'active', '2021/01/01');
INSERT INTO PROMOTION (promotion_id, promotion_name, promotion_description, promotion_discount, promotion_endDate, promotion_status, promotion_startDate) VALUES (3, 'Mother''s day', 'ลูกค้าสมาชิกสามารถพาคุณแม่มารับเครื่องดื่มฟรี 1 แก้ว', 0.12, '2023/08/13', 'inactive', '2023/08/12');
INSERT INTO PROMOTION (promotion_id, promotion_name, promotion_description, promotion_discount, promotion_endDate, promotion_status, promotion_startDate) VALUES (4, 'Father''s day', 'ลูกค้าสมาชิกสามารถพาคุณพ่อมารับเครื่องดื่มฟรี 1 แก้ว', 0.15, '2022/12/06', 'inactive', '2022/12/05');
INSERT INTO PROMOTION (promotion_id, promotion_name, promotion_description, promotion_discount, promotion_endDate, promotion_status, promotion_startDate) VALUES (5, 'Children day', 'ลูกค้าสมาชิกที่มีเด็กอายุต่ำกว่า 10 ปี รับของหวานฟรี 1 ชิ้น', 0.1, '2024/01/14', 'active', '');

-- Table: PROMOTION_DETAIL
CREATE TABLE IF NOT EXISTS PROMOTION_DETAIL (promotionDetail_id INTEGER NOT NULL UNIQUE, product_id INTEGER NOT NULL, promotion_id INTEGER NOT NULL, promotionDetail_status TEXT NOT NULL, PRIMARY KEY (promotionDetail_id AUTOINCREMENT), FOREIGN KEY (product_id) REFERENCES PRODUCT, FOREIGN KEY (promotion_id) REFERENCES PROMOTION_DETAIL);

-- Table: RECEIPT
CREATE TABLE IF NOT EXISTS RECEIPT (receipt_id INTEGER NOT NULL UNIQUE, employee_id INTEGER, vendor_code TEXT, receipt_quantity INT NOT NULL, receipt_total REAL NOT NULL, receipt_discount REAL NOT NULL, receipt_netPrice REAL NOT NULL, receipt_dateOrder TEXT NOT NULL, receipt_dateReceive TEXT NOT NULL, receipt_datePaid TEXT NOT NULL, receipt_change REAL NOT NULL, receipt_status TEXT NOT NULL, PRIMARY KEY (receipt_id AUTOINCREMENT), FOREIGN KEY (employee_id) REFERENCES EMPLOYEE, FOREIGN KEY (vendor_code) REFERENCES VENDOR);
INSERT INTO RECEIPT (receipt_id, employee_id, vendor_code, receipt_quantity, receipt_total, receipt_discount, receipt_netPrice, receipt_dateOrder, receipt_dateReceive, receipt_datePaid, receipt_change, receipt_status) VALUES (1, 1, '1', 1, 5.0, 0.0, 325.0, '2021/02/03', '2021/02/05', '2021/02/03', 0.0, 'recieved');
INSERT INTO RECEIPT (receipt_id, employee_id, vendor_code, receipt_quantity, receipt_total, receipt_discount, receipt_netPrice, receipt_dateOrder, receipt_dateReceive, receipt_datePaid, receipt_change, receipt_status) VALUES (2, 2, '2', 1, 5.0, 0.0, 500.0, '2021/03/03', '2021/03/05', '2021/03/03', 0.0, 'recieved');
INSERT INTO RECEIPT (receipt_id, employee_id, vendor_code, receipt_quantity, receipt_total, receipt_discount, receipt_netPrice, receipt_dateOrder, receipt_dateReceive, receipt_datePaid, receipt_change, receipt_status) VALUES (3, 3, '3', 1, 5.0, 0.0, 400.0, '2021/03/28', '2021/03/30', '2021/03/28', 0.0, 'recieved');
INSERT INTO RECEIPT (receipt_id, employee_id, vendor_code, receipt_quantity, receipt_total, receipt_discount, receipt_netPrice, receipt_dateOrder, receipt_dateReceive, receipt_datePaid, receipt_change, receipt_status) VALUES (4, 4, '4', 1, 5.0, 0.0, 450.0, '2021/04/14', '2021/04/16', '2021/04/14', 0.0, 'recieved');
INSERT INTO RECEIPT (receipt_id, employee_id, vendor_code, receipt_quantity, receipt_total, receipt_discount, receipt_netPrice, receipt_dateOrder, receipt_dateReceive, receipt_datePaid, receipt_change, receipt_status) VALUES (5, 5, '5', 1, 5.0, 0.0, 425.0, '2021/05/25', '2021/05/27', '2021/05/25', 0.0, 'recieved');
INSERT INTO RECEIPT (receipt_id, employee_id, vendor_code, receipt_quantity, receipt_total, receipt_discount, receipt_netPrice, receipt_dateOrder, receipt_dateReceive, receipt_datePaid, receipt_change, receipt_status) VALUES (6, 6, '2', 1, 5.0, 0.0, 350.0, '2022/02/20', '2022/02/22', '2022/02/20', 0.0, 'recieved');

-- Table: RECEIPT_DETAIL
CREATE TABLE IF NOT EXISTS RECEIPT_DETAIL (receiptDetail_id TEXT NOT NULL UNIQUE, receipt_id INTEGER, ingredient_id INTEGER, receiptDetail_quantity INTEGER NOT NULL, receiptDetail_description TEXT NOT NULL, receiptDetail_pricePerUnit REAL NOT NULL, receiptDetail_discount REAL NOT NULL, receiptDetail_netPrice REAL NOT NULL, PRIMARY KEY (receiptDetail_id), FOREIGN KEY (receipt_id) REFERENCES RECEIPT (receipt_id), FOREIGN KEY (ingredient_id) REFERENCES INGREDIENT (ingredient_id));
INSERT INTO RECEIPT_DETAIL (receiptDetail_id, receipt_id, ingredient_id, receiptDetail_quantity, receiptDetail_description, receiptDetail_pricePerUnit, receiptDetail_discount, receiptDetail_netPrice) VALUES ('1', 1, 1, 1, 'ผงชาเขียว 5 ถุง', 65.0, 0.0, 325.0);
INSERT INTO RECEIPT_DETAIL (receiptDetail_id, receipt_id, ingredient_id, receiptDetail_quantity, receiptDetail_description, receiptDetail_pricePerUnit, receiptDetail_discount, receiptDetail_netPrice) VALUES ('2', 2, 2, 2, 'เมล็ดกาแฟ 5 ถุง', 100.0, 0.0, 500.0);

-- Table: SALARY_PAYMENT
CREATE TABLE IF NOT EXISTS SALARY_PAYMENT (salaryPayment_id INTEGER NOT NULL UNIQUE, employee_id INTEGER REFERENCES EMPLOYEE (employee_id), salaryPayment_date TEXT NOT NULL, salaryPayment_type TEXT NOT NULL, salaryPayment_totalAmount REAL NOT NULL, PRIMARY KEY (salaryPayment_id AUTOINCREMENT), FOREIGN KEY (employee_id) REFERENCES EMPLOYEE);
INSERT INTO SALARY_PAYMENT (salaryPayment_id, employee_id, salaryPayment_date, salaryPayment_type, salaryPayment_totalAmount) VALUES (1, 1, '2021/01/25', 'รายเดือน', 50000.0);
INSERT INTO SALARY_PAYMENT (salaryPayment_id, employee_id, salaryPayment_date, salaryPayment_type, salaryPayment_totalAmount) VALUES (2, 2, '2021/01/25', 'รายเดือน', 12000.0);
INSERT INTO SALARY_PAYMENT (salaryPayment_id, employee_id, salaryPayment_date, salaryPayment_type, salaryPayment_totalAmount) VALUES (3, 3, '2021/01/25', 'รายเดือน', 12000.0);
INSERT INTO SALARY_PAYMENT (salaryPayment_id, employee_id, salaryPayment_date, salaryPayment_type, salaryPayment_totalAmount) VALUES (4, 4, '2021/01/25', 'รายเดือน', 12000.0);
INSERT INTO SALARY_PAYMENT (salaryPayment_id, employee_id, salaryPayment_date, salaryPayment_type, salaryPayment_totalAmount) VALUES (5, 3, '2022/01/25', 'รายเดือน', 15000.0);
INSERT INTO SALARY_PAYMENT (salaryPayment_id, employee_id, salaryPayment_date, salaryPayment_type, salaryPayment_totalAmount) VALUES (6, 4, '2022/01/25', 'รายเดือน', 15000.0);
INSERT INTO SALARY_PAYMENT (salaryPayment_id, employee_id, salaryPayment_date, salaryPayment_type, salaryPayment_totalAmount) VALUES (7, 5, '2022/01/25', 'รายเดือน', 12000.0);
INSERT INTO SALARY_PAYMENT (salaryPayment_id, employee_id, salaryPayment_date, salaryPayment_type, salaryPayment_totalAmount) VALUES (8, 6, '2023/08/25', 'รายเดือน', 12000.0);
INSERT INTO SALARY_PAYMENT (salaryPayment_id, employee_id, salaryPayment_date, salaryPayment_type, salaryPayment_totalAmount) VALUES (9, 7, '2023/08/25', 'รายเดือน', 12000.0);

-- Table: USER
CREATE TABLE IF NOT EXISTS USER (user_id INTEGER NOT NULL UNIQUE, employee_id INTEGER, user_login TEXT NOT NULL, user_password TEXT NOT NULL, user_title TEXT NOT NULL, user_gender TEXT NOT NULL, PRIMARY KEY (user_id AUTOINCREMENT), FOREIGN KEY (employee_id) REFERENCES EMPLOYEE);
INSERT INTO USER (user_id, employee_id, user_login, user_password, user_title, user_gender) VALUES (1, 1, 'admin', 'myadmin', 'owner', 'Male');
INSERT INTO USER (user_id, employee_id, user_login, user_password, user_title, user_gender) VALUES (2, 2, 'tony', 'tony12345', 'employee', 'Male');
INSERT INTO USER (user_id, employee_id, user_login, user_password, user_title, user_gender) VALUES (3, 3, 'walt', 'walt45678', 'employee', 'Male');
INSERT INTO USER (user_id, employee_id, user_login, user_password, user_title, user_gender) VALUES (4, 4, 'david', 'david004', 'employee', 'Male');
INSERT INTO USER (user_id, employee_id, user_login, user_password, user_title, user_gender) VALUES (5, 5, 'ranmouri', 'mouri005', 'employee', 'Female');
INSERT INTO USER (user_id, employee_id, user_login, user_password, user_title, user_gender) VALUES (6, 6, ' johnc', 'cena006', 'employee', 'Male');

-- Table: VENDOR
CREATE TABLE IF NOT EXISTS VENDOR (vendor_code TEXT NOT NULL UNIQUE, vendor_name TEXT NOT NULL, vendor_contact TEXT NOT NULL, vendor_locate TEXT NOT NULL, vendor_tel TEXT NOT NULL, vendor_state TEXT NOT NULL, vendor_orderCount INTEGER NOT NULL, PRIMARY KEY (vendor_code));
INSERT INTO VENDOR (vendor_code, vendor_name, vendor_contact, vendor_locate, vendor_tel, vendor_state, vendor_orderCount) VALUES ('1', 'MAKRO', 'store5@siammakro.co.th', '55/3 หมู่ 2 ถ.สุขุมวิท ต.เสม็ด อ.เมือง จ.ชลบุรี 20000', '0871234567', 'ชลบุรี', 3);
INSERT INTO VENDOR (vendor_code, vendor_name, vendor_contact, vendor_locate, vendor_tel, vendor_state, vendor_orderCount) VALUES ('2', 'LOTUS', 'tescolotus.com', '62 หมู่ 1 ถ.สุขุมวิท ต.เสม็ด อ.เมือง จ.ชลบุรี 20000', '038282572', 'ชลบุรี', 2);
INSERT INTO VENDOR (vendor_code, vendor_name, vendor_contact, vendor_locate, vendor_tel, vendor_state, vendor_orderCount) VALUES ('3', 'BIGC', 'bigc.co.th/contact', 'เลขที่ 49, 1 ตำบล ห้วยกะปิ อำเภอเมืองชลบุรี ชลบุรี 20000', '0-21465959', 'ชลบุรี', 2);
INSERT INTO VENDOR (vendor_code, vendor_name, vendor_contact, vendor_locate, vendor_tel, vendor_state, vendor_orderCount) VALUES ('4', 'CJ', 'cjexpress.co.th/contact', 'เลขที่ 25/42 หมู่ที่ 5 ตำบลบ้านปึก อำเภอเมืองชลบุรี จังหวัดชลบุรี 20130', '085-8491825', 'ชลบุรี', 1);
INSERT INTO VENDOR (vendor_code, vendor_name, vendor_contact, vendor_locate, vendor_tel, vendor_state, vendor_orderCount) VALUES ('5', 'TOPS', 'tops.co.th/th/contact', '278/2 ถนน เลียบ ตำบลแสนสุข อำเภอเมืองชลบุรี ชลบุรี 20130', '038384594', 'ชลบุรี', 3);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
