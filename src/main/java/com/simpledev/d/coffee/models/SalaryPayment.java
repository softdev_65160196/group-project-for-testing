package com.simpledev.d.coffee.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SalaryPayment {

    private int id;
    private int empID;
    private String date;
    private String type;
    private double totalAmount;

    public SalaryPayment(int id, int empID, String date, String type, double totalAmount) {
        this.id = id;
        this.empID = empID;
        this.date = date;
        this.type = type;
        this.totalAmount = totalAmount;
    }
    
    public SalaryPayment(int empID, String date, String type, double totalAmount) {
        this.id = -1;
        this.empID = empID;
        this.date = date;
        this.type = type;
        this.totalAmount = totalAmount;
    }
    
     public SalaryPayment() {
        this.id = -1;
        this.empID = 0;
        this.date = null;
        this.type = "";
        this.totalAmount = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmpID() {
        return empID;
    }

    public void setEmpID(int empID) {
        this.empID = empID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public String toString() {
        return "SaralySlip{" + "id=" + id + ", empID=" + empID + ", date=" + date + ", type=" + type + ", totalAmount=" + totalAmount + '}';
    }
    
    public static SalaryPayment fromRS(ResultSet rs) {
        SalaryPayment salarySlip = new SalaryPayment();
        try {
            salarySlip.setId(rs.getInt("salaryPayment_id"));
            salarySlip.setEmpID(rs.getInt("employee_id"));
            salarySlip.setDate(rs.getString("salaryPayment_date"));
            salarySlip.setType(rs.getString("salaryPayment_type"));
            salarySlip.setTotalAmount(rs.getDouble("salaryPayment_totalAmount"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salarySlip;
    }
    
}
