/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpledev.d.coffee.models;

import com.simpledev.d.coffee.dao.EmployeeDao;
import com.simpledev.d.coffee.dao.VendorDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class Receipt {

    private int id;
    private Employee employee;
    private Vendor vendor;
    private int quantity;
    private double total;
    private double discount;
    private double netPrice;
    private String dateOrder;
    private String dateReceive;
    private String datePaid;
    private double change;
    private String status;

    public Receipt(int id, Employee employee, Vendor vendor, int quantity, double total, double discount, double netPrice, String dateOrder, String dateReceive, String datePaid, double change, String status) {
        this.id = id;
        this.employee = employee;
        this.vendor = vendor;
        this.quantity = quantity;
        this.total = total;
        this.discount = discount;
        this.netPrice = netPrice;
        this.dateOrder = dateOrder;
        this.dateReceive = dateReceive;
        this.datePaid = datePaid;
        this.change = change;
        this.status = status;
    }

    public Receipt(Employee employee, Vendor vendor, int quantity, double total, double discount, double netPrice, String dateOrder, String dateReceive, String datePaid, double change, String status) {
        this.id = -1;
        this.employee = employee;
        this.vendor = vendor;
        this.quantity = quantity;
        this.total = total;
        this.discount = discount;
        this.netPrice = netPrice;
        this.dateOrder = dateOrder;
        this.dateReceive = dateReceive;
        this.datePaid = datePaid;
        this.change = change;
        this.status = status;
    }

    public Receipt() {
        this.id = -1;
        this.employee = new Employee();
        this.vendor = new Vendor();
        this.quantity = 0;
        this.total = 0;
        this.discount = 0;
        this.netPrice = 0;
        this.dateOrder = "";
        this.dateReceive = "";
        this.datePaid = "";
        this.change = 0;
        this.status = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(double netPrice) {
        this.netPrice = netPrice;
    }

    public String getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(String dateOrder) {
        this.dateOrder = dateOrder;
    }

    public String getDateReceive() {
        return dateReceive;
    }

    public void setDateReceive(String dateReceive) {
        this.dateReceive = dateReceive;
    }

    public String getDatePaid() {
        return datePaid;
    }

    public void setDatePaid(String datePaid) {
        this.datePaid = datePaid;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", employee=" + employee + ", vendor=" + vendor + ", quantity=" + quantity + ", total=" + total + ", discount=" + discount + ", netPrice=" + netPrice + ", dateOrder=" + dateOrder + ", dateReceive=" + dateReceive + ", datePaid=" + datePaid + ", change=" + change + ", status=" + status + '}';
    }

    public static Receipt fromRS(ResultSet rs) {
        Receipt receipt = new Receipt();
        EmployeeDao employeeDao = new EmployeeDao();
        VendorDao vendorDao = new VendorDao();
        int emp_id;
        String vd_code;
        try {
            emp_id = (rs.getInt("employee_id"));
            vd_code = (rs.getString("vendor_code"));
            receipt.setEmployee(employeeDao.get(emp_id));
            receipt.setVendor(vendorDao.get(vd_code));
            receipt.setId(rs.getInt("receipt_id"));
            receipt.setQuantity(rs.getInt("receipt_quantity"));
            receipt.setTotal(rs.getDouble("receipt_total"));
            receipt.setDiscount(rs.getDouble("receipt_discount"));
            receipt.setNetPrice(rs.getDouble("receipt_netPrice"));
            receipt.setDateOrder(rs.getString("receipt_dateOrder"));
            receipt.setDateReceive(rs.getString("receipt_dateReceive"));
            receipt.setDatePaid(rs.getString("receipt_datePaid"));
            receipt.setChange(rs.getDouble("receipt_change"));
            receipt.setStatus(rs.getString("receipt_status"));
            

        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receipt;
    }

}
