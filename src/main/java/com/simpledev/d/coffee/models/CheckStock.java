/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.models;

import com.simpledev.d.coffee.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sirikon
 */
public class CheckStock {
    private int id;
    private Employee employee;
    private String description;
    private int valueLoss;
    private int unitLoss;
    private int totalUnit;
    private String date;

    public CheckStock(int id, Employee employee, String description, int valueLoss, int unitLoss, int totalUnit, String date) {
        this.id = id;
        this.employee = employee;
        this.description = description;
        this.valueLoss = valueLoss;
        this.unitLoss = unitLoss;
        this.totalUnit = totalUnit;
        this.date = date;
    }

    public CheckStock(Employee employee, String description, int valueLoss, int unitLoss, int totalUnit, String date) {
        this.id = -1;
        this.employee = employee;
        this.description = description;
        this.valueLoss = valueLoss;
        this.unitLoss = unitLoss;
        this.totalUnit = totalUnit;
        this.date = date;
    }
    
    public CheckStock() {
        this.id = -1;
        this.employee = new Employee();
        this.description = "";
        this.valueLoss = 0;
        this.unitLoss = 0;
        this.totalUnit = 0;
        this.date = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getValueLoss() {
        return valueLoss;
    }

    public void setValueLoss(int valueLoss) {
        this.valueLoss = valueLoss;
    }

    public int getUnitLoss() {
        return unitLoss;
    }

    public void setUnitLoss(int unitLoss) {
        this.unitLoss = unitLoss;
    }

    public int getTotalUnit() {
        return totalUnit;
    }

    public void setTotalUnit(int totalUnit) {
        this.totalUnit = totalUnit;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "CheckStock{" + "id=" + id + ", employee=" + employee + ", description=" + description + ", valueLoss=" + valueLoss + ", unitLoss=" + unitLoss + ", totalUnit=" + totalUnit + ", date=" + date + '}';
    }
    
    public static CheckStock fromRS(ResultSet rs) {
        CheckStock chStock = new CheckStock();
        EmployeeDao empDao = new EmployeeDao();
        int employee_id;
        try {
            chStock.setId(rs.getInt("checkStock_id"));
            employee_id = rs.getInt("employee_id");
            chStock.setEmployee(empDao.get(employee_id));
            chStock.setDescription(rs.getString("checkStock_description"));
            chStock.setValueLoss(rs.getInt("checkStock_valueLoss"));
            chStock.setUnitLoss(rs.getInt("checkStock_unitLoss"));
            chStock.setTotalUnit(rs.getInt("checkStock_totalUnit"));
            chStock.setDate(rs.getString("checkStock_date"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckStock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return chStock;
    }
}
