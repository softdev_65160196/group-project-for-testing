package com.simpledev.d.coffee.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Vendor {

    private String code;
    private String name;
    private String contact;
    private String locate;
    private String tel;
    private String state;
    private int orderCount;

    public Vendor(String code, String name, String contact, String locate, String tel, String state, int orderCount) {
        this.code = code;
        this.name = name;
        this.contact = contact;
        this.locate = locate;
        this.tel = tel;
        this.state = state;
        this.orderCount = orderCount;
    }

    public Vendor() {
        this.code = "";
        this.name = "";
        this.contact = "";
        this.locate = "";
        this.tel = "";
        this.state = "";
        this.orderCount = 0;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getLocate() {
        return locate;
    }

    public void setLocate(String locate) {
        this.locate = locate;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(int orderCount) {
        this.orderCount = orderCount;
    }

    @Override
    public String toString() {
        return "Vendor{" + "code=" + code + ", name=" + name + ", contact=" + contact + ", locate=" + locate + ", tel=" + tel + ", state=" + state + ", orderCount=" + orderCount + '}';
    }

    public static Vendor fromRS(ResultSet rs) {
        String v = "vendor_";
        String[] c = {"code", "name", "contact", "locate", "tel", "state", "orderCount"};
        Vendor vendor = new Vendor();
        try {
            vendor.setCode(rs.getString(v + c[0]));
            vendor.setName(rs.getString(v + c[1]));
            vendor.setContact(rs.getString(v + c[2]));
            vendor.setLocate(rs.getString(v + c[3]));
            vendor.setTel(rs.getString(v + c[4]));
            vendor.setState(rs.getString(v + c[5]));
            vendor.setOrderCount(rs.getInt(v + c[6]));

        } catch (SQLException ex) {
            Logger.getLogger(Vendor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vendor;
    }
}
