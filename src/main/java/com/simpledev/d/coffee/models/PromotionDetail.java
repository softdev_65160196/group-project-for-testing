/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.models;

import com.simpledev.d.coffee.dao.ProductDao;
import com.simpledev.d.coffee.dao.PromotionDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sirikon
 */
public class PromotionDetail {
    private int id;
    private Product product;
    private Promotion promotion;
    private String status;

    public PromotionDetail(int id, Product product, Promotion promotion, String status) {
        this.id = id;
        this.product = product;
        this.promotion = promotion;
        this.status = status;
    }

    public PromotionDetail(Product product, Promotion promotion, String status) {
        this.id = -1;
        this.product = product;
        this.promotion = promotion;
        this.status = status;
    }
    
    public PromotionDetail() {
        this.id = -1;
        this.product = null;
        this.promotion = null;
        this.status = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "PromotionDetail{" + "id=" + id + ", product=" + product + ", promotion=" + promotion + ", status=" + status + '}';
    }
    
    public static PromotionDetail fromRS(ResultSet rs) {
        PromotionDetail promotionDetail = new PromotionDetail();
        ProductDao productDao = new ProductDao();
        PromotionDao promotionDao = new PromotionDao();
        int product_id;
        int promotion_id;
        try {
            promotionDetail.setId(rs.getInt("promotionDetail_id"));
            product_id = (rs.getInt("product_id"));
            promotion_id = (rs.getInt("promotion_id"));
            promotionDetail.setStatus(rs.getString("promotionDetail_status"));
            

        } catch (SQLException ex) {
            Logger.getLogger(PromotionDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotionDetail;
    }
    
    
}
