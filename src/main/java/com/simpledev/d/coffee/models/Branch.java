/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sirikon
 */
public class Branch {

    private String code;
    private String locate;
    private String tel;

    public Branch(String code, String locate, String tel) {
        this.code = code;
        this.locate = locate;
        this.tel = tel;
    }

    public Branch() {
        this.code = "";
        this.locate = "";
        this.tel = "";
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLocate() {
        return locate;
    }

    public void setLocate(String locate) {
        this.locate = locate;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "Branch{" + "id=" + code + ", locate=" + locate + ", tel=" + tel + '}';
    }

    public static Branch fromRS(ResultSet rs) {
        Branch branch = new Branch();
        try {
            branch.setCode(rs.getString("branch_code"));
            branch.setLocate(rs.getString("branch_locate"));
            branch.setTel(rs.getString("branch_tel"));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return branch;
    }
}
