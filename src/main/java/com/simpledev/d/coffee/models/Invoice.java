package com.simpledev.d.coffee.models;

import com.simpledev.d.coffee.dao.BranchDao;
import com.simpledev.d.coffee.dao.CustomerDao;
import com.simpledev.d.coffee.dao.EmployeeDao;
import com.simpledev.d.coffee.dao.PromotionDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Invoice {

    private int id;
    private Customer customer;
    private Employee employee;
    private Branch branch;
    private Promotion promotion;
    private LocalDateTime date;
    private int totalUnit;
    private double totalPrice;
    private double discount;
    private double netPrice;
    private double moneyReceive;
    private double moneyChange;
    private ArrayList<InvoiceDetail> invoiceDetails;

    public Invoice(int id, Customer customer, Employee employee, Branch branch, Promotion promotion, LocalDateTime date, int totalUnut, double totalPrice, double discount, double netPrice, double moneyReceive, double moneyChange, ArrayList<InvoiceDetail> invoiceDetails) {
        this.id = id;
        this.customer = customer;
        this.employee = employee;
        this.branch = branch;
        this.promotion = promotion;
        this.date = date;
        this.totalUnit = totalUnut;
        this.totalPrice = totalPrice;
        this.discount = discount;
        this.netPrice = netPrice;
        this.moneyReceive = moneyReceive;
        this.moneyChange = moneyChange;
        this.invoiceDetails = invoiceDetails;
    }

    public Invoice(Customer customer, Employee employee, Branch branch, Promotion promotion, LocalDateTime date, int totalUnit, double totalPrice, double discount, double netPrice, double moneyReceive, double moneyChange, ArrayList<InvoiceDetail> invoiceDetails) {
        this.id = -1;
        this.customer = customer;
        this.employee = employee;
        this.branch = branch;
        this.promotion = promotion;
        this.date = date;
        this.totalUnit = totalUnit;
        this.totalPrice = totalPrice;
        this.discount = discount;
        this.netPrice = netPrice;
        this.moneyReceive = moneyReceive;
        this.moneyChange = moneyChange;
        this.invoiceDetails = invoiceDetails;
    }

    public Invoice() {
        this.id = -1;
        this.customer = null;
        this.employee = null;
        this.branch = null;
        this.promotion = null;
        this.date = null;
        this.totalUnit = 0;
        this.totalPrice = 0;
        this.discount = 0;
        this.netPrice = 0;
        this.moneyReceive = 0;
        this.moneyChange = 0;
        this.invoiceDetails = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public LocalDateTime getDate() {
        return date;
    }
    public String getDateString() {
        return date.toString();
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public int getTotalUnit() {
        return totalUnit;
    }

    public void setTotalUnit(int totalUnit) {
        this.totalUnit = totalUnit;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(double netPrice) {
        this.netPrice = netPrice;
    }

    public double getMoneyReceive() {
        return moneyReceive;
    }

    public void setMoneyReceive(double moneyReceive) {
        this.moneyReceive = moneyReceive;
    }

    public double getMoneyChange() {
        return moneyChange;
    }

    public void setMoneyChange(double moneyChange) {
        this.moneyChange = moneyChange;
    }

    public ArrayList<InvoiceDetail> getInvoiceDetails() {
        return invoiceDetails;
    }

    public void setInvoiceDetails(ArrayList<InvoiceDetail> invoiceDetails) {
        this.invoiceDetails = invoiceDetails;
    }
    
    public void addInvoiceDetail(InvoiceDetail invoiceDetail) {
        invoiceDetails.add(invoiceDetail);
        calculateTotal();
    }

    public void delReceoptDetail(InvoiceDetail invoiceDetail) {
        invoiceDetails.remove(invoiceDetail);
        calculateTotal();
    }

    private void calculateTotal() {
        int qty_total = 0;
        float total = 0.0f;
        for (InvoiceDetail rd : invoiceDetails) {
            total += rd.getUnit() * rd.getUnitprice();
            qty_total += rd.getUnit();
        }
//        this.totalQty = qty_total;
        this.totalPrice = total;
    }
//
//        

    @Override
    public String toString() {
        return "Invoice{" + "id=" + id + ", customer=" + customer + ", employee=" + employee + ", branch=" + branch + ", promotion=" + promotion + ", date=" + date + ", totalUnit=" + totalUnit + ", totalPrice=" + totalPrice + ", discount=" + discount + ", netPrice=" + netPrice + ", moneyReceive=" + moneyReceive + ", moneyChange=" + moneyChange + ", invoiceDetails=" + invoiceDetails + '}';
    }

    
    public static Invoice fromRS(ResultSet rs) {
        // create new object standby
        Invoice invoice = new Invoice();
        // import needed DAO
        CustomerDao customerDao = new CustomerDao();
        EmployeeDao employeeDao = new EmployeeDao();
        BranchDao branchDao = new BranchDao();
        PromotionDao promotionDao = new PromotionDao();
        // local variable
        LocalDateTime date;
        int cus_id;
        int emp_id;
        String bra_code;
        int pro_id;
        try {
            // format date to "year/month/day"
            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            date = LocalDateTime.parse(rs.getString("invoice_date"), df);
            // get id
            cus_id = (rs.getInt("customer_id"));
            emp_id = (rs.getInt("employee_id"));
            bra_code = (rs.getString("branch_code"));
            pro_id = (rs.getInt("promotion_id"));
            //  set object
            invoice.setId(rs.getInt("invoice_id"));
            invoice.setTotalUnit(rs.getInt("invoice_totalUnit"));
            invoice.setTotalPrice(rs.getDouble("invoice_totalPrice"));
            invoice.setDiscount(rs.getDouble("invoice_discount"));
            invoice.setNetPrice(rs.getDouble("invoice_netPrice"));
            invoice.setMoneyReceive(rs.getDouble("invoice_moneyReceive"));
            invoice.setMoneyChange(rs.getFloat("invoice_moneyChange"));
            invoice.setCustomer(customerDao.get(cus_id));
            invoice.setEmployee(employeeDao.get(emp_id));
            invoice.setBranch(branchDao.get(bra_code));
            invoice.setPromotion(promotionDao.get(pro_id));
            invoice.setDate(date);

        } catch (SQLException ex) {
            Logger.getLogger(Invoice.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return invoice;
    }

}
