package com.simpledev.d.coffee.models;

import com.simpledev.d.coffee.dao.ProductDao;
import com.simpledev.d.coffee.dao.InvoiceDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InvoiceDetail {

    private int id;
    private Product product;
    private Invoice invoice;
    private int unit;
    private double unitprice;
    private double discount;
    private double netPrice;
    

    public InvoiceDetail(Product product, Invoice invoice, int unit, double unitprice, double discount, double netPrice) {
        this.id = -1;
        this.product = product;
        this.invoice = invoice;
        this.unit = unit;
        this.unitprice = unitprice;
        this.discount = discount;
        this.netPrice = netPrice;
    }
    
    public InvoiceDetail(int id, Product product, double productPrice, int unit, double netPrice, Invoice invoice) {
        this.id = id;
        this.product = product;
        this.invoice = invoice;
        this.unit = unit;
        this.unitprice = 0;
        this.discount = 0;
        this.netPrice = netPrice;
    }

    public InvoiceDetail() {
        this.id = -1;
        this.product = null;
        this.invoice = null;
        this.unit = 0;
        this.unitprice = 0;
        this.discount = 0;
        this.netPrice = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public double getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(double unitprice) {
        this.unitprice = unitprice;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getNetPrice() {
        return netPrice;
    }
    
    public double getTotalPrice() {
        return unit * unitprice;
    }

    public void setNetPrice(double netPrice) {
        this.netPrice = netPrice;
    }

    @Override
    public String toString() {
        return "InvoiceDetail{" + "id=" + id + ", product=" + product + ", invoice=" + invoice + ", unit=" + unit + ", unitprice=" + unitprice + ", discount=" + discount + ", netPrice=" + netPrice + '}';
    }

    public static InvoiceDetail fromRS(ResultSet rs) {
        InvoiceDetail invD = new InvoiceDetail();
        ProductDao productDao = new ProductDao();
        InvoiceDao invoiceDao = new InvoiceDao();
        int product_id;
        int invoice_id;

        try {
            // get only id for set object by check that id
            product_id = rs.getInt("product_id");
            invoice_id = rs.getInt("invoice_id");
            // set object
            invD.setId(rs.getInt("invoiceDetail_id"));
            invD.setUnit(rs.getInt("invoiceDetail_unit"));
            invD.setUnitprice(rs.getFloat("invoiceDetail_unitPrice"));
            invD.setDiscount(rs.getFloat("invoiceDetail_discount"));
            invD.setNetPrice(rs.getFloat("invoiceDetail_netPrice"));
            invD.setProduct(productDao.get(product_id));
            invD.setInvoice(invoiceDao.get(invoice_id));
        } catch (SQLException ex) {
            Logger.getLogger(Invoice.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return invD;
    }

}
