package com.simpledev.d.coffee.models;

import com.simpledev.d.coffee.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class User {

    private int id;
    private Employee employee;
    private String login;
    private String password;
    private String title;

    public User(int id, Employee employee, String login, String password, String title, String gender) {
        this.id = id;
        this.employee = employee;
        this.login = login;
        this.password = password;
        this.title = title;
    }

    public User(Employee employee, String login, String password, String title, String gender) {
        this.id = -1;
        this.employee = employee;
        this.login = login;
        this.password = password;
        this.title = title;
    }

    public User() {
        this.id = -1;
        this.employee = null;
        this.login = "";
        this.password = "";
        this.title = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", employee=" + employee + ", login=" + login + ", password=" + password + ", title=" + title + "}";
    }
    
    public static User fromRS(ResultSet rs) {
        User user = new User();
        EmployeeDao employeeDao = new EmployeeDao();
        int emp_id;
        try {
            user.setId(rs.getInt("user_id"));
            emp_id = (rs.getInt("employee_id"));
            user.setLogin(rs.getString("user_login"));
            user.setPassword(rs.getString("user_password"));
            user.setTitle(rs.getString("user_title"));
            user.setEmployee(employeeDao.get(emp_id)); 

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return user;
    }

}
