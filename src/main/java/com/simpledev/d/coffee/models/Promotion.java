/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.models;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sirikon
 */
public class Promotion {
    private int id;
    private String name;
    private String description;
    private double discount;
    private String startDate;
    private String endDate;
    private String status;

    public Promotion(int id, String name, String description, double discount,String startDate, String endDate, String status) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.discount = discount;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
    }

    public Promotion(String name, String description, double discount,String startDate, String endDate, String status) {
        this.id = -1;
        this.name = name;
        this.description = description;
        this.discount = discount;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
    }
    
    public Promotion() {
        this.id = -1;
        this.name = "";
        this.description = "";
        this.discount = 0;
        this.startDate = "";
        this.endDate = "";
        this.status = "";
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", name=" + name + ", description=" + description + ", discount=" + discount + ", startDate=" + startDate + ", endDate=" + endDate + ", status=" + status + '}';
    }


    
    public static Promotion fromRS(ResultSet rs) {
        Promotion promotion = new Promotion();
        try {
            promotion.setId(rs.getInt("promotion_id"));
            promotion.setName(rs.getString("promotion_name"));
            promotion.setDescription(rs.getString("promotion_description"));
            promotion.setDiscount(rs.getDouble("promotion_discount"));
            promotion.setStartDate(rs.getString("promotion_startDate"));
            promotion.setEndDate(rs.getString("promotion_endDate"));
            promotion.setStatus(rs.getString("promotion_status"));

        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotion;
    }

    
    
    
}
