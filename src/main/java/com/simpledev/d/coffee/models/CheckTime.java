package com.simpledev.d.coffee.models;

import com.simpledev.d.coffee.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CheckTime {
    private int id;
    private String date;
    private Employee employee;
    private String checkIn;
    private String checkOut;
    private int totalWorked;
    private String type;

    public CheckTime(int id, String date, Employee employee, String checkIn, String checkOut, int totalWorked, String type) {
        this.id = id;
        this.date = date;
        this.employee = employee;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.totalWorked = totalWorked;
        this.type = type;
    }
    
    public CheckTime(String date, Employee employee, String checkIn, String checkOut, int totalWorked, String type) {
        this.id = -1;
        this.date = date;
        this.employee = employee;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.totalWorked = totalWorked;
        this.type = type;
    }
    
    public CheckTime() {
        this.id = -1;
        this.date = "";
        this.employee = null;
        this.checkIn = "";
        this.checkOut = "";
        this.totalWorked = 0;
        this.type = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public int getTotalWorked() {
        return totalWorked;
    }

    public void setTotalWorked(int totalWorked) {
        this.totalWorked = totalWorked;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "CheckTime{" + "id=" + id + ", date=" + date + ", employee=" + employee + ", checkIn=" + checkIn + ", checkOut=" + checkOut + ", totalWorked=" + totalWorked + ", type=" + type + '}';
    }
    
    public static CheckTime fromRS(ResultSet rs) {
        CheckTime checkTime = new CheckTime();
        EmployeeDao employeeDao = new EmployeeDao();
        int emp_id;
        try {
            checkTime.setId(rs.getInt("checkTime_id"));
            checkTime.setDate(rs.getString("checkTime_date"));
            emp_id = (rs.getInt("employee_id"));
            checkTime.setCheckIn(rs.getString("checkTime_in"));
            checkTime.setCheckOut(rs.getString("checkTime_out"));
            checkTime.setTotalWorked(rs.getInt("checkTime_totalWorked"));
            checkTime.setType(rs.getString("checkTime_type"));
            checkTime.setEmployee(employeeDao.get(emp_id)); 

        } catch (SQLException ex) {
            Logger.getLogger(CheckTime.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkTime;
    }
    
}
