package com.simpledev.d.coffee.publisher;

import com.simpledev.d.coffee.models.Product;

public interface BuyProductable {

    public void buy(Product product, int qty);
}
