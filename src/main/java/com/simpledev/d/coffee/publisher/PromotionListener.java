package com.simpledev.d.coffee.publisher;

import com.simpledev.d.coffee.models.Promotion;

public interface PromotionListener {

    public void onPromotionSet(Promotion promotion);
}
