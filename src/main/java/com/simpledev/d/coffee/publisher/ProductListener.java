
package com.simpledev.d.coffee.publisher;

import com.simpledev.d.coffee.models.Product;

public interface ProductListener {
    void onAddProduct(Product p,int qty);
}
