
package com.simpledev.d.coffee.publisher;

import com.simpledev.d.coffee.models.User;


public interface LoginCallback {
    void onLoginSuccess(User user,int role);
}
