package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.Customer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CustomerDao implements Dao<Customer> {

    @Override
    public Customer get(int id) {
        Customer customer = new Customer();
        String sql = "SELECT * FROM CUSTOMER WHERE customer_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                customer = Customer.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return customer;
    }

    public Customer getByTel(String tel) {
        Customer customer = new Customer();
        String sql = "SELECT * FROM CUSTOMER WHERE customer_tel=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, tel);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                customer = Customer.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return customer;
    }
    
    public Customer getByFName(String fname) {
        Customer customer = new Customer();
        String sql = "SELECT * FROM CUSTOMER WHERE customer_name=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, fname);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                customer = Customer.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return customer;
    }

    @Override
    public ArrayList<Customer> getAll() {
        Customer customer;
        ArrayList<Customer> list = new ArrayList();
        String sql = "SELECT * FROM CUSTOMER";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                customer = Customer.fromRS(rs);
                list.add(customer);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Customer> getAll(String order) {
        Customer customer = new Customer();
        ArrayList<Customer> list = new ArrayList();
        String sql = "SELECT * FROM CUSTOMER ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                customer = Customer.fromRS(rs);
                list.add(customer);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Customer insert(Customer obj) {
        String sql = "INSERT INTO CUSTOMER (customer_status, customer_name, customer_lastname,"
                + "customer_tel, customer_birth_date, customer_point, customer_join_date)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getState());
            stmt.setString(2, obj.getFirstName());
            stmt.setString(3, obj.getLastName());
            stmt.setString(4, obj.getTel());
            stmt.setString(5, obj.getBirthDate());
            stmt.setInt(6, obj.getPoint());
            stmt.setString(7, obj.getJoindate());
//            System.out.println(stmt);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Customer update(Customer obj) {
        String sql = "UPDATE CUSTOMER"
                + " SET customer_status = ?, customer_name = ?, customer_lastname = ?, customer_tel = ?, customer_birth_date = ?, customer_point = ?, customer_join_date = ?"
                + " WHERE customer_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getState());
            stmt.setString(2, obj.getFirstName());
            stmt.setString(3, obj.getLastName());
            stmt.setString(4, obj.getTel());
            stmt.setString(5, obj.getBirthDate());
            stmt.setInt(6, obj.getPoint());
            stmt.setString(7, obj.getJoindate());
            stmt.setInt(8, obj.getId());

//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Customer obj) {
        String sql = "DELETE FROM CUSTOMER WHERE customer_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Customer> getAll(String where, String order) {
        Customer customer;
        ArrayList<Customer> list = new ArrayList();
        String sql = "SELECT * FROM CUSTOMER ORDER BY" + where + " " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                customer = Customer.fromRS(rs);
                list.add(customer);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
