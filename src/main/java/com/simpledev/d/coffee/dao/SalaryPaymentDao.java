
package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.SalaryPayment;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class SalaryPaymentDao implements Dao<SalaryPayment>{

    @Override
    public SalaryPayment get(int id) {
      SalaryPayment salaryPayment = new SalaryPayment();
        String sql = "SELECT * FROM SALARY_PAYMENT WHERE salaryPayment_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                salaryPayment = salaryPayment.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return salaryPayment;
    }

    @Override
    public List<SalaryPayment> getAll() {
        SalaryPayment salaryPaymentloyee = new SalaryPayment();
        ArrayList<SalaryPayment> list = new ArrayList();
        String sql = "SELECT * FROM SALARY_PAYMENT";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                salaryPaymentloyee = salaryPaymentloyee.fromRS(rs);
                list.add(salaryPaymentloyee);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public ArrayList<SalaryPayment> getByMonth(int month) {
            String formattedMonth = null;
            if (month >= 1 && month <= 12) {
                formattedMonth = String.format("%02d", month);
            }
//            SalaryPayment salaryPayment = new SalaryPayment();
            ArrayList<SalaryPayment> list = new ArrayList();
            String sql = "SELECT * FROM SALARY_PAYMENT WHERE strftime('%m',salaryPayment_date)='?'";
            Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, formattedMonth);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                SalaryPayment salaryPayment = SalaryPayment.fromRS(rs);
                list.add(salaryPayment);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public SalaryPayment insert(SalaryPayment obj) {
        String sql = "INSERT INTO SALARY_PAYMENT (employee_id=?, salaryPayment_type=?, salaryPayment_totalAmount=?)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmpID());
            stmt.setString(2, obj.getType());
            stmt.setDouble(3, obj.getTotalAmount());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }


    @Override
    public SalaryPayment update(SalaryPayment obj) {
        String sql = "UPDATE SALARY_PAYMENT"
                + " SET employee_id = ?,  salaryPayment_type = ?, salary_totalAmount = ?"
                + " WHERE salaryPayment_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmpID());
            stmt.setString(2, obj.getType());
            stmt.setDouble(3, obj.getTotalAmount());
            stmt.setInt(4, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }}

    @Override
    public int delete(SalaryPayment obj) {
        String sql = "DELETE FROM SALARY_PAYMENT WHERE salaryPayment_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        }

    @Override
    public List<SalaryPayment> getAll(String where, String order) {

        ArrayList<SalaryPayment> list = new ArrayList<SalaryPayment>();
        String sql = "SELECT * FROM SALARY_PAYMENT ORDER BY " + where + " " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SalaryPayment salaryPayment = SalaryPayment.fromRS(rs);
                list.add(salaryPayment);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
}
