/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.CheckStock;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class CheckStockDao implements Dao<CheckStock> {
    
    @Override
    public CheckStock get(int id) {
        CheckStock chStock = new CheckStock();
        String sql = "SELECT * FROM CHECK_STOCK WHERE checkStock_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                chStock = chStock.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return chStock;
    }

    @Override
    public List<CheckStock> getAll() {
        ArrayList<CheckStock> list = new ArrayList();
        String sql = "SELECT * FROM CHECK_STOCK";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStock chStock = CheckStock.fromRS(rs);
                list.add(chStock);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckStock insert(CheckStock obj) {
        String sql = "INSERT INTO CHECK_STOCK (employee_id, checkStock_description,"
                + "checkStock_valueLoss, checkStock_unitLoss, checkStock_totalUnit, checkStock_date)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployee().getId());
            stmt.setString(2, obj.getDescription());
            stmt.setInt(3, obj.getValueLoss());
            stmt.setInt(4, obj.getUnitLoss());
            stmt.setInt(5, obj.getTotalUnit());
            stmt.setString(6,obj.getDate());
//            System.out.println(stmt);
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckStock update(CheckStock obj) {
        String sql = "UPDATE CHECK_STOCK"
                + " SET employee_id = ?, checkStock_description = ?,  checkStock_valueLoss = ?, checkStock_unitLoss = ?, checkStock_totalUnit = ?, checkStock_date = ?"
                + " WHERE checkStock_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployee().getId());
            stmt.setString(2, obj.getDescription());
            stmt.setInt(3, obj.getValueLoss());
            stmt.setInt(4, obj.getUnitLoss());
            stmt.setInt(5, obj.getTotalUnit());
            stmt.setString(6, obj.getDate());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckStock obj) {
        String sql = "DELETE FROM CHECK_STOCK WHERE checkStock_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;   
    }

    @Override
    public List<CheckStock> getAll(String where, String order) {
        CheckStock chStock = new CheckStock();
        ArrayList<CheckStock> list = new ArrayList();
        String sql = "SELECT * FROM CHECK_STOCK  ORDER BY " + where + " " +  order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                chStock = CheckStock.fromRS(rs);
                list.add(chStock);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
