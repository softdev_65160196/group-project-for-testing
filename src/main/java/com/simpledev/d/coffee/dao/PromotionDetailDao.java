/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.PromotionDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



/**
 *
 * @author ASUS
 */
public class PromotionDetailDao implements Dao<PromotionDetail>{
    
    @Override
    public PromotionDetail get(int id) {
        PromotionDetail promotionDetail = null;
        String sql = "SELECT * FROM PROMOTION_DETAIL WHERE promotionDetail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                promotionDetail = PromotionDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promotionDetail;
    }

    @Override
    public ArrayList<PromotionDetail> getAll() {
        ArrayList<PromotionDetail> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTION_DETAIL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PromotionDetail promotionDetail = PromotionDetail.fromRS(rs);
                list.add(promotionDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public PromotionDetail insert(PromotionDetail obj) {
        String sql = "INSERT INTO PROMOTION_DETAIL (product_id, promotion_id, promotionDetail_status)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProduct().getId());
            stmt.setInt(2, obj.getPromotion().getId());
            stmt.setString(3, obj.getStatus());
            
//            System.out.println(stmt);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public PromotionDetail update(PromotionDetail obj) {
        String sql = "UPDATE PROMOTION_DETAIL"
                + " SET product_id = ?, promotion_id = ?, promotionDetail_status = ?,"
                + " WHERE promotionDetail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProduct().getId());
            stmt.setInt(2, obj.getPromotion().getId());
            stmt.setString(3, obj.getStatus());
            stmt.setInt(4, obj.getId());

//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
    }
    }

    @Override
    public int delete(PromotionDetail obj) {
        String sql = "DELETE FROM PROMOTION_DETAIL WHERE promotionDetail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;   
    }

    @Override
    public List<PromotionDetail> getAll(String where, String order) {
        PromotionDetail promotionDetail = new PromotionDetail();
        ArrayList<PromotionDetail> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTION_DETAIL ORDER BY " + where + " " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                promotionDetail = promotionDetail.fromRS(rs);
                list.add(promotionDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
}
