
package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.Promotion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PromotionDao implements Dao<Promotion> {

    @Override
    public Promotion get(int id) {
        Promotion promotion = null;
        String sql = "SELECT * FROM PROMOTION WHERE promotion_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                promotion = Promotion.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promotion;
    }
    
    public Promotion getByName(String name) {
        Promotion promotion = null;
        String sql = "SELECT * FROM PROMOTION WHERE promotion_name=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                promotion = Promotion.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promotion;
    }

    @Override
    public List<Promotion> getAll() {

        ArrayList< Promotion> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTION";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                list.add(promotion);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    @Override
    public Promotion insert(Promotion obj) {
        String sql = "INSERT INTO PROMOTION "
                + "(promotion_name, promotion_description, promotion_discount,"
                + "promotion_startDate, promotion_endDate,"
                + "promotion_status)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getDescription());
            stmt.setDouble(3, obj.getDiscount());
            stmt.setString(4, obj.getStartDate());            
            stmt.setString(5, obj.getEndDate());
            stmt.setString(6, obj.getStatus());

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Promotion update(Promotion obj) {
        String sql = "UPDATE PROMOTION"
                + " SET promotion_name = ?,  promotion_description = ?, promotion_discount = ?, promotion_startDate = ?, promotion_endDate = ?, promotion_status = ?"
                + " WHERE promotion_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getDescription());
            stmt.setDouble(3, obj.getDiscount());
            stmt.setString(4, obj.getStartDate());
            stmt.setString(5, obj.getEndDate());
            stmt.setString(6, obj.getStatus());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Promotion obj) {
        String sql = "DELETE FROM PROMOTION WHERE promotion_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1; 
    }

    @Override
    public List<Promotion> getAll(String where, String order) {
        ArrayList<Promotion> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTION ORDER BY " + where +  " " + order ;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                list.add(promotion);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
