package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import java.util.List;
import com.simpledev.d.coffee.models.Vendor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class VendorDao implements Dao<Vendor> {

    @Override
    public Vendor get(int code) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Vendor get(String code) {
        Vendor vendor = new Vendor();
        String sql = "SELECT * FROM VENDOR WHERE vendor_code =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, code);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                vendor = Vendor.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return vendor;
    }

    @Override
    public ArrayList<Vendor> getAll() {
        Vendor vendor;
        ArrayList<Vendor> list = new ArrayList();
        String sql = "SELECT * FROM VENDOR";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                vendor = Vendor.fromRS(rs);
                list.add(vendor);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Vendor insert(Vendor obj) {
        String sql = "INSERT INTO VENDOR (vendor_name, vendor_contact, vendor_locate, vendor_tel, vendor_state, vendor_orderCount)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getContact());
            stmt.setString(3, obj.getLocate());
            stmt.setString(4, obj.getTel());
            stmt.setString(5, obj.getState());
            stmt.setInt(6, obj.getOrderCount());
//            System.out.println(stmt);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Vendor update(Vendor obj) {
        String sql = "UPDATE VENDOR"
                + " SET vendor_name = ?, vendor_contact = ?, vendor_locate = ?, vendor_tel = ?, vendor_state = ?, vendor_orderCount = ?"
                + " WHERE vendor_code = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getContact());
            stmt.setString(3, obj.getLocate());
            stmt.setString(4, obj.getTel());
            stmt.setString(5, obj.getState());
            stmt.setInt(6, obj.getOrderCount());
            stmt.setString(7, obj.getCode());

//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Vendor obj) {
        String sql = "DELETE FROM VENDOR WHERE vendor_code=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getCode());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Vendor> getAll(String where, String order) {
        Vendor vendor;
        ArrayList<Vendor> list = new ArrayList();
        String sql = "SELECT * FROM VENDOR ORDER BY "+ where + " " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                vendor = Vendor.fromRS(rs);
                list.add(vendor);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
