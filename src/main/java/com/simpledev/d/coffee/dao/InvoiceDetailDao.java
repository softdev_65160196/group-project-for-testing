
package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.InvoiceDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class InvoiceDetailDao implements Dao<InvoiceDetail> {

    @Override
    public InvoiceDetail get(int id) {
        InvoiceDetail invoiceDetail = new InvoiceDetail();
        String sql = "SELECT * FROM INVOICE_DETAIL WHERE invoiceDetail_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                invoiceDetail = InvoiceDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return invoiceDetail;
    }

    @Override
    public List<InvoiceDetail> getAll() {
        ArrayList<InvoiceDetail> list = new ArrayList<InvoiceDetail>();
        String sql = "SELECT * FROM INVOICE_DETAIL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                InvoiceDetail invoiceDetail = new InvoiceDetail();
                list.add(invoiceDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public InvoiceDetail insert(InvoiceDetail obj) {
        String sql = "INSERT INTO INVOICE_DETAIL (product_id, invoice_id, invoiceDetail_unit, invoiceDetail_unitPrice, invoiceDetail_discount, invoiceDetail_netPrice)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProduct().getId());
            stmt.setInt(2, obj.getInvoice().getId());
            stmt.setInt(3, obj.getUnit());
            stmt.setDouble(4, obj.getUnitprice());
            stmt.setDouble(5, obj.getDiscount());
            stmt.setDouble(6,obj.getNetPrice());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public InvoiceDetail update(InvoiceDetail obj) {
        String sql = "UPDATE INVOICE_DETAIL"
                + " SET product_id = ?, invoice_id = ?, invoiceDetail_unit = ?, invoiceDetail_unitPrice = ?, invoiceDetail_discount = ?, invoiceDetail_netPrice = ?"
                + " WHERE invoiceDetail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProduct().getId());
            stmt.setInt(2, obj.getInvoice().getId());
            stmt.setInt(3, obj.getUnit());
            stmt.setDouble(4, obj.getUnitprice());
            stmt.setDouble(5, obj.getDiscount());
            stmt.setDouble(6,obj.getNetPrice());
            stmt.setInt(7,obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(InvoiceDetail obj) {
        String sql = "DELETE FROM INVOICE_DETAIL WHERE invoiceDetail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;  
    }

    @Override
    public List<InvoiceDetail> getAll(String where, String order) {
        InvoiceDetail invoiceDetail;
        ArrayList<InvoiceDetail> list = new ArrayList();
        String sql = "SELECT * FROM INVOICE_DETAIL  ORDER BY " + where + " " + order ;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                invoiceDetail = InvoiceDetail.fromRS(rs);
                list.add(invoiceDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}