
package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import java.util.List;
import com.simpledev.d.coffee.models.Branch;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class BranchDao implements Dao<Branch>{

    @Override
    public Branch get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    public Branch get(String code) {
        Branch branch = new Branch();
        String sql = "SELECT * FROM BRANCH WHERE branch_code =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, code);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                branch = Branch.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return branch;
    }


    @Override
    public List<Branch> getAll() {
        Branch branch;
        ArrayList<Branch> list = new ArrayList();
        String sql = "SELECT * FROM BRANCH";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                branch = Branch.fromRS(rs);
                list.add(branch);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Branch insert(Branch obj) {
        String sql = "INSERT INTO BRANCH (branch_id, branch_locate, branch_tel)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getCode());
            stmt.setString(2, obj.getLocate());
            stmt.setString(3, obj.getTel());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Branch update(Branch obj) {
        String sql = "UPDATE BRANCH"
                + " SET branch_locate = ?, branch_tel = ?"
                + " WHERE branch_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getLocate());
            stmt.setString(2, obj.getTel());
            stmt.setString(3, obj.getCode());

            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }}

    @Override
    public int delete(Branch obj) {
        String sql = "DELETE FROM BRANCH WHERE branch_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getCode());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;    
    }

    @Override
    public List<Branch> getAll(String where, String order) {
        Branch branch;
        ArrayList<Branch> list = new ArrayList();
        String sql = "SELECT * FROM BRANCH ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                branch = Branch.fromRS(rs);
                list.add(branch);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }}

