package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.CheckTime;
import com.simpledev.d.coffee.models.Employee;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CheckTimeDao implements Dao<CheckTime> {

    @Override
    public CheckTime get(int id) {
        CheckTime checkTime = new CheckTime();
        String sql = "SELECT * FROM CHECK_INOUT WHERE checkTime_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkTime = CheckTime.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkTime;
    }

    @Override
    public List<CheckTime> getAll() {
        CheckTime checkTime;
        ArrayList<CheckTime> list = new ArrayList();
        String sql = "SELECT * FROM CHECK_INOUT";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                checkTime = CheckTime.fromRS(rs);
                list.add(checkTime);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckTime insert(CheckTime obj) {
        String sql = "INSERT INTO CHECK_INOUT (employee_id, checkTime_in, checkTime_out, checkTime_totalWorked, checkTime_type, checkTime_date"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployee().getId());
            stmt.setString(2, obj.getCheckIn());
            stmt.setString(3, obj.getCheckOut());
            stmt.setInt(4, obj.getTotalWorked());
            stmt.setString(5, obj.getType());
            stmt.setString(6, obj.getDate());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckTime update(CheckTime obj) {
        String sql = "UPDATE CHECK_INOUT"
                + " SET employee_id = ?, checkTime_in = ?, checkTime_out = ?, checkTime_totalWorked = ?, checkTime_type = ?"
                + " WHERE checkTime_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployee().getId());
            stmt.setString(2, obj.getCheckIn());
            stmt.setString(3, obj.getCheckOut());
            stmt.setInt(4, obj.getTotalWorked());
            stmt.setString(5, obj.getType());
            stmt.setInt(6, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public void updateCheckOut(Employee emp, String time, int totalWorked, String type) {
        String sql = "UPDATE CHECK_INOUT"
                + " SET checkTime_out = ?, checkTime_totalWorked = ?, checkTime_type = ?"
                + " WHERE employee_id = ?";
        Connection conn;
        PreparedStatement stmt;
        try {
            conn = DatabaseHelper.getConnect();
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, time);
            stmt.setInt(2, totalWorked);
            stmt.setString(3, type);
            stmt.setInt(4, emp.getId());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } 
    }

    @Override
    public int delete(CheckTime obj) {
        String sql = "DELETE FROM CHECK_INOUT WHERE checkTime_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<CheckTime> getAll(String where, String order) {
        ArrayList<CheckTime> list = new ArrayList();
        String sql = "SELECT * FROM CHECK_INOUT ORDER BY " + where + " " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckTime checkTime = CheckTime.fromRS(rs);
                list.add(checkTime);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public CheckTime getByEmpId(int id) {
        CheckTime checkTime = new CheckTime();
        String sql = "SELECT * FROM CHECK_INOUT WHERE employee_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkTime = CheckTime.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkTime;
    }
}
