
package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.CheckStockDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class CheckStockDetailDao implements Dao<CheckStockDetail>{
    
    @Override
    public CheckStockDetail get(int id) {
        CheckStockDetail chStockDetail = new CheckStockDetail();
        String sql = "SELECT * FROM CHECK_STOCK_DETAIL WHERE checkStockDetail_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                chStockDetail = chStockDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return chStockDetail;
    }

    @Override
    public List<CheckStockDetail> getAll() {
        ArrayList<CheckStockDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM CHECK_STOCK_DETAIL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockDetail chStockDetail = new CheckStockDetail();
                list.add(chStockDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }


    @Override
    public CheckStockDetail insert(CheckStockDetail obj) {
        String sql = "INSERT INTO CHECK_STOCK_DETAIL (ingrediant_code, checkStock_id, checkStockDetail_description, checkStockDetail_quantity, checkStockDetail_unitExpire, checkStockDetail_valueLoss, checkStockDetail_date)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getStock().getId());
            stmt.setInt(2, obj.getCheckStock().getId());
            stmt.setString(3, obj.getDescription());
            stmt.setInt(4, obj.getQuantity());
            stmt.setInt(5, obj.getUnitExpire());
            stmt.setInt(6, obj.getValueLoss());
            stmt.setString(7,obj.getDate());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckStockDetail update(CheckStockDetail obj) {
        String sql = "UPDATE CHECK_STOCK_DETAIL"
                + " SET ingredient_code = ?, checkStock_id = ?, checkStockDetail_description = ?,  checkStockDetail_quantity = ?, checkStockDetail_unitExpire = ?, checkStockDetail_valueLoss = ?, checkStockDetail_date = ?"
                + " WHERE checkStockDetail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getStock().getId());
            stmt.setInt(2, obj.getCheckStock().getId());
            stmt.setString(3, obj.getDescription());
            stmt.setInt(4, obj.getQuantity());
            stmt.setInt(5, obj.getUnitExpire());
            stmt.setInt(6, obj.getValueLoss());
            stmt.setString(7,obj.getDate());
            stmt.setInt(7,obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckStockDetail obj) {
        String sql = "DELETE FROM CHECK_STOCK_DETAIL WHERE checkStockDetail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;     
    }
    

    @Override
    public List<CheckStockDetail> getAll(String where, String order) {
        CheckStockDetail chStockDetail;
        ArrayList<CheckStockDetail> list = new ArrayList();
        String sql = "SELECT * FROM CHECK_STOCK_DETAIL  ORDER BY "  + where + " " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                chStockDetail = CheckStockDetail.fromRS(rs);
                list.add(chStockDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
