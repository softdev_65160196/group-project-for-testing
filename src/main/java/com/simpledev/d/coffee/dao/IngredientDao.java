/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.Ingredient;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class IngredientDao implements Dao<Ingredient> {
    //*** this class is familiar as Ingredient class***
    
    @Override
    public Ingredient get(int id) {
        Ingredient igd = new Ingredient();
        String sql = "SELECT * FROM INGREDIENT WHERE ingredient_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                igd = igd.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return igd;
    }

    public ArrayList<Ingredient> getAll() {
        ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM INGREDIENT";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ingredient igd = Ingredient.fromRS(rs);

                list.add(igd);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public List<Ingredient> getAll(String order) {
        ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM ingredient ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ingredient ingredient = Ingredient.fromRS(rs);
                list.add(ingredient);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override

    public Ingredient insert(Ingredient obj) {
        String sql = "INSERT INTO ingredient (ingredient_name, ingredient_minNeed, ingredient_quantity, ingredient_value)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getMinNeed());
            stmt.setInt(3, obj.getQuantity());
            stmt.setDouble(4, obj.getValue());
//            System.out.println(stmt);
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override

    public Ingredient update(Ingredient obj) {
        String sql = "UPDATE INGREDIENT"
                + " SET ingredient_name = ?,  ingredient_minNeed = ?, ingredient_quantity = ?, ingredient_value = ?"
                + " WHERE ingredient_id = ?";

        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getMinNeed());
            stmt.setInt(3, obj.getQuantity());
            stmt.setDouble(4, obj.getValue());
            stmt.setInt(5, obj.getId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override

    public int delete(Ingredient obj) {
        String sql = "DELETE FROM ingredient WHERE ingredient_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override

    public List<Ingredient> getAll(String where, String order) {
        Ingredient igd = new Ingredient();
        ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM ingredient  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                igd = igd.fromRS(rs);
                list.add(igd);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
