package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.Receipt;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ReceiptDao implements Dao<Receipt> {

    @Override
    public Receipt get(int id) {
        Receipt receipt = null;

        String sql = "SELECT * FROM RECEIPT WHERE receipt_id=?";

        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                receipt = Receipt.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return receipt;

    }

    @Override
    public List<Receipt> getAll() {

        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    @Override
    public Receipt insert(Receipt obj) {

        String sql = "INSERT INTO RECEIPT "
                + "(employee_id, "
                + "vendor_code, receipt_quantity, "
                + "receipt_total, receipt_discount, receipt_netPrice "
                + "receipt_dateOrder, receipt_dateReceive, receipt_datePaid "
                + "receipt_change, receipt_status)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployee().getId());
            stmt.setString(2, obj.getVendor().getCode());
            stmt.setInt(3, obj.getQuantity());
            stmt.setDouble(4, obj.getTotal());
            stmt.setDouble(5, obj.getDiscount());
            stmt.setDouble(6, obj.getNetPrice());
            stmt.setString(7, obj.getDateOrder());
            stmt.setString(8, obj.getDateReceive());
            stmt.setString(9, obj.getDatePaid());
            stmt.setDouble(10, obj.getChange());
            stmt.setString(11, obj.getStatus());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;

    }

    @Override
    public Receipt update(Receipt obj) {

        String sql = "UPDATE RECEIPT"
                + " SET employee_id = ?,  vendor_code = ?, receipt_quantity = ?, receipt_total = ?, receipt_discount = ?, receipt_netPrice = ?, receipt_dateOrder = ?, receipt_dateReceive = ?, receipt_datePaid = ?, receipt_change = ?, receipt_status = ? "
                + " WHERE receipt_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployee().getId());
            stmt.setString(2, obj.getVendor().getCode());
            stmt.setInt(3, obj.getQuantity());
            stmt.setDouble(4, obj.getTotal());
            stmt.setDouble(5, obj.getDiscount());
            stmt.setDouble(6, obj.getNetPrice());
            stmt.setString(7, obj.getDateOrder());
            stmt.setString(8, obj.getDateReceive());
            stmt.setString(9, obj.getDatePaid());
            stmt.setDouble(10, obj.getChange());
            stmt.setString(11, obj.getStatus());
            stmt.setInt(12, obj.getId());
            
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    @Override
    public int delete(Receipt obj) {

        String sql = "DELETE FROM RECEIPT WHERE receipt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;

    }

    @Override
    public List<Receipt> getAll(String where, String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT ORDER BY " + where + " " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}

