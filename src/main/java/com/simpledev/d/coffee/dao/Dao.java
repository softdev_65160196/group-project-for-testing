package com.simpledev.d.coffee.dao;

import java.util.List;

interface Dao<T> {

    T get(int id);
    
    List<T> getAll();

    T insert(T obj);

    T update(T obj);

    int delete(T obj);

    List<T> getAll(String where, String order);
}
