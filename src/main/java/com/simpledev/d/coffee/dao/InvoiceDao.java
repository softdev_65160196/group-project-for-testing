package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import java.util.List;
import com.simpledev.d.coffee.models.Invoice;
import com.simpledev.d.coffee.models.InvoiceDetail;
import com.simpledev.d.coffee.models.Promotion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class InvoiceDao implements Dao<Invoice> {

    @Override
    public Invoice get(int id) {
        Invoice invoice = new Invoice();
        InvoiceDetailDao rdd = new InvoiceDetailDao();
        String sql = "SELECT * FROM INVOICE WHERE invoice_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                // create rc and rest but no rcd yet
                invoice = Invoice.fromRS(rs);
                // get rc details list
                ArrayList<InvoiceDetail> invoiceDetails = new ArrayList<>();
                while (!invoiceDetails.isEmpty()) {
                    invoiceDetails.add(rdd.get(invoice.getId()));
                }
                //set rc details o rc
                invoice.setInvoiceDetails(invoiceDetails);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return invoice;

    }

    @Override
    public List<Invoice> getAll() {
        ArrayList<Invoice> list = new ArrayList();
        String sql = "SELECT * FROM INVOICE";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Invoice invoice = Invoice.fromRS(rs);
                list.add(invoice);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

//    @Override
    @Override
    public Invoice insert(Invoice obj) {
        String sql = "INSERT INTO INVOICE (customer_id, employee_id, branch_code, promotion_id, invoice_date, invoice_totalUnit, invoice_totalPrice, invoice_discount, invoice_netPrice, invoice_moneyReceive, invoice_moneyChange)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

        try {
            Promotion promotion = obj.getPromotion();
            PreparedStatement stmt = conn.prepareStatement(sql);

            LocalDateTime currentDateTime = obj.getDate();
            String formmattedDateTime = currentDateTime.format(formatter);
            stmt.setString(5, formmattedDateTime);

            stmt.setInt(1, obj.getCustomer().getId());
            stmt.setInt(2, obj.getEmployee().getId());
            stmt.setString(3, obj.getBranch().getCode());
            stmt.setInt(4, obj.getPromotion().getId());
            stmt.setInt(6, obj.getTotalUnit());
            stmt.setDouble(7, obj.getTotalPrice());
            stmt.setDouble(8, obj.getDiscount());
            stmt.setDouble(9, obj.getNetPrice());
            stmt.setDouble(10, obj.getMoneyReceive());
            stmt.setDouble(11, obj.getMoneyChange());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Invoice update(Invoice obj) {
        String sql = "UPDATE INVOICE"
                + " SET customer_id = ?, employee_id = ?, branch_code = ?, promotion_id = ?, invoice_date = ?, invoice_totalUnit = ?, invoice_totalPrice = ?, invoice_discount = ?, invoice_netPrice = ?, invoice_moneyReceive = ?, invoice_moneyChange = ?"
                + " WHERE invoice_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCustomer().getId());
            stmt.setInt(2, obj.getEmployee().getId());
            stmt.setString(3, obj.getBranch().getCode());
            stmt.setInt(4, obj.getPromotion().getId());
            stmt.setString(5, obj.getDateString());
            stmt.setInt(6, obj.getTotalUnit());
            stmt.setDouble(7, obj.getTotalPrice());
            stmt.setDouble(8, obj.getDiscount());
            stmt.setDouble(9, obj.getNetPrice());
            stmt.setDouble(10, obj.getMoneyReceive());
            stmt.setDouble(11, obj.getMoneyChange());
            stmt.setInt(12, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Invoice obj) {
        String sql = "DELETE FROM INVOICE WHERE invoice_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Invoice> getAll(String where, String order) {
        Invoice rcdao;
        ArrayList<Invoice> list = new ArrayList();
        String sql = "SELECT * FROM INVOICE  ORDER BY " + where + " " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                rcdao = Invoice.fromRS(rs);
                list.add(rcdao);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
