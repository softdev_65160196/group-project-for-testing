package com.simpledev.d.coffee.component;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.RoundRectangle2D;
import javax.swing.JPanel;

public class RoundedPanel extends JPanel {

    public RoundedPanel() {
        super();
        setOpaque(false);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g.create();

        int width = getWidth();
        int height = getHeight();
        
        //set for HQ
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        //set round 20
        RoundRectangle2D roundedRect = new RoundRectangle2D.Float(0, 0, width, height, 20, 20);
        g2d.setClip(roundedRect);
        g2d.setColor(getBackground());
        g2d.fillRect(0, 0, width, height);
        //for release graphic
        g2d.dispose();
    }

}
