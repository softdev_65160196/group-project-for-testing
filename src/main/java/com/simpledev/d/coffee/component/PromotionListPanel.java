package com.simpledev.d.coffee.component;

import com.simpledev.d.coffee.models.Promotion;
import com.simpledev.d.coffee.services.PromotionService;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import com.simpledev.d.coffee.publisher.PromotionListener;

public class PromotionListPanel extends javax.swing.JPanel {

    private final PromotionService promotionService;
    private final ArrayList<PromotionListener> subscribers = new ArrayList<>();

    public PromotionListPanel() {
        initComponents();

        promotionService = new PromotionService();
        List<Promotion> promotion = promotionService.getAllPromotions();
        int promotionSize = promotion.size();
        for (Promotion p : promotion) {
            PromotionItemPanel pnlPromotionItem = new PromotionItemPanel(p);
            pnlPromotionList.add(pnlPromotionItem);

        }
        pnlPromotionList.setLayout(new GridLayout((promotionSize / 3) + ((promotionSize % 3 != 0) ? 1 : 0), 3, 0, 0));
  

    }

    public void addOnBuyPromotion(PromotionListener subscriber) {
        subscribers.add(subscriber);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlPromotionList = new javax.swing.JPanel();

        javax.swing.GroupLayout pnlPromotionListLayout = new javax.swing.GroupLayout(pnlPromotionList);
        pnlPromotionList.setLayout(pnlPromotionListLayout);
        pnlPromotionListLayout.setHorizontalGroup(
            pnlPromotionListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 350, Short.MAX_VALUE)
        );
        pnlPromotionListLayout.setVerticalGroup(
            pnlPromotionListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 418, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 9, Short.MAX_VALUE)
                .addComponent(pnlPromotionList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 9, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 9, Short.MAX_VALUE)
                .addComponent(pnlPromotionList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 9, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel pnlPromotionList;
    // End of variables declaration//GEN-END:variables

}
