package com.simpledev.d.coffee.component;

import com.simpledev.d.coffee.publisher.BuyProductable;
import com.simpledev.d.coffee.models.Product;
import com.simpledev.d.coffee.services.ProductServices;
import java.awt.GridLayout;
import java.util.ArrayList;

/**
 *
 * @author Windows10
 */
public class ProductListBakeryPanel extends javax.swing.JPanel implements BuyProductable {

    private final ProductServices productService;
    private final ArrayList<Product> products;
    private final ArrayList<BuyProductable> subscribers = new ArrayList<>();

    /**
     * Creates new form ProductListPanel
     */
    public ProductListBakeryPanel() {
        initComponents();

        productService = new ProductServices();
        products = productService.getProductsCategoryBakery();
        int productSize = products.size();
        for (Product p : products) {
            ProductBakeryItemPanel pnlProductItem = new ProductBakeryItemPanel(p);
            pnlProductItem.addOnBuyProduct(this);
            pnlProductList.add(pnlProductItem);

        }
        pnlProductList.setLayout(new GridLayout((productSize / 3) + ((productSize % 3 != 0) ? 1 : 0), 3, 0, 0));
    }

    public void addOnBuyProduct(BuyProductable subscriber) {
        subscribers.add(subscriber);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlProductList = new javax.swing.JPanel();

        javax.swing.GroupLayout pnlProductListLayout = new javax.swing.GroupLayout(pnlProductList);
        pnlProductList.setLayout(pnlProductListLayout);
        pnlProductListLayout.setHorizontalGroup(
            pnlProductListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 350, Short.MAX_VALUE)
        );
        pnlProductListLayout.setVerticalGroup(
            pnlProductListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 418, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 9, Short.MAX_VALUE)
                .addComponent(pnlProductList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 9, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 9, Short.MAX_VALUE)
                .addComponent(pnlProductList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 9, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel pnlProductList;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Product product, int qty) {
//        System.out.println("" + product.getName() + " " + qty); 
        for (BuyProductable s : subscribers) {
            s.buy(product, qty);
        }
    }

}
