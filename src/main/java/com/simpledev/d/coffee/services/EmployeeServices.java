
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.EmployeeDao;
import com.simpledev.d.coffee.models.Employee;
import java.util.ArrayList;
import java.util.List;


public class EmployeeServices {
    public ArrayList<Employee> getAll() {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll();
    }
    public List<Employee> getUsersByOrder(String where, String order) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll(where, order);
    }
    
    public List<Employee> getEmployeeByOrder() {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll("employee_id", "DESC");
    }
    
    public Employee addNew(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.insert(editedEmployee);
    }
    public Employee update(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.update(editedEmployee);
    }
    public int delete(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.delete(editedEmployee);
    }

    public Employee getById(int id) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.get(id);
    }

}
