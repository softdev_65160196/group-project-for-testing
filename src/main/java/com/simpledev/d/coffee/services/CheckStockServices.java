/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.CheckStockDao;
import com.simpledev.d.coffee.dao.CustomerDao;
import com.simpledev.d.coffee.models.CheckStock;
import com.simpledev.d.coffee.models.Customer;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class CheckStockServices {
    
    public Customer getById(int id) {
        CustomerDao customerDao = new CustomerDao();
        Customer customer = customerDao.get(id);
        return customer;
    }
    
    public ArrayList<CheckStock> getAll() {
        CheckStockDao chStockDao = new CheckStockDao();
        return (ArrayList<CheckStock>) chStockDao.getAll();
    }
    public List<CheckStock> getCheckStockByOrder(String where, String order) {
        CheckStockDao chStockDao = new CheckStockDao();
        return chStockDao.getAll(where, order);
    }
    
    public List<CheckStock> getCheckStockByOrderDESC() {
        CheckStockDao chStockDao = new CheckStockDao();
        return chStockDao.getAll("checkStock_id", "DESC");
    }
    
    public CheckStock addNew(CheckStock editedCheckStock) {
        CheckStockDao checkStockDao = new CheckStockDao();
        return checkStockDao.insert(editedCheckStock);
    }
    public CheckStock update(CheckStock editedCheckStock) {
        CheckStockDao checkStockDao = new CheckStockDao();
        return checkStockDao.update(editedCheckStock);
    }
    public void delete(CheckStock editedStock) {
        CheckStockDao chStockDao = new CheckStockDao();
        chStockDao.delete(editedStock);
    }
}
