/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.IngredientDao;
import com.simpledev.d.coffee.models.Ingredient;
import java.util.ArrayList;

/**
 *
 * @author informatics
 */
public class IngredientService {
    private IngredientDao ingredientDao = new IngredientDao();
    public ArrayList<Ingredient> getIngredientOrderById(){
        return (ArrayList<Ingredient>) ingredientDao.getAll("ingredient_id DESC");
    }
    
    public ArrayList<Ingredient> getAll() {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.getAll();
    }
    
    public Ingredient getIngredient(int id) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.get(id);
    }
    
    public Ingredient addNew(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.insert(editedIngredient);    
    }
    
    public Ingredient update(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.update(editedIngredient);  
    }
    
    public void delete(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        ingredientDao.delete(editedIngredient);
    }
}