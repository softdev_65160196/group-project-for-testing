
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.PromotionDao;
import com.simpledev.d.coffee.models.Promotion;
import java.util.ArrayList;
import java.util.List;

public class PromotionService {

    public List<Promotion> getAllPromotions() {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.getAll();
    }

    public ArrayList<Promotion> getPromotionOrderById() {
        PromotionDao promotionDao = new PromotionDao();
        return (ArrayList<Promotion>) promotionDao.getAll("promotion_id", "ASC");
    }
    
    public ArrayList<Promotion> getPromotionOrderByIdDESC() {
        PromotionDao promotionDao = new PromotionDao();
        return (ArrayList<Promotion>) promotionDao.getAll("promotion_id", "DESC");
    }

    public List<Promotion> getPromotionsByOrder(String where, String order) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.getAll(where, order);
    }
    
    public Promotion getByName(String name) {
        PromotionDao promotionDao = new PromotionDao();
        Promotion promotion= promotionDao.getByName(name);
        return promotion;
    }

    public void addNew(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        promotionDao.insert(editedPromotion);
    }

    public void update(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        promotionDao.update(editedPromotion);
    }

    public void delete(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        promotionDao.delete(editedPromotion);
    }
}
