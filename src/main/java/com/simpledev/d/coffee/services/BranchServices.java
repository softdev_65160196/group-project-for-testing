/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.BranchDao;
import com.simpledev.d.coffee.models.Branch;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class BranchServices {

    public ArrayList<Branch> getAll() {
        BranchDao branchDao = new BranchDao();
        return (ArrayList<Branch>) branchDao.getAll();
    }

    public List<Branch> getAllByOrder(String where, String order) {
        BranchDao branchDao = new BranchDao();
        return branchDao.getAll(where, order);
    }
    
    public List<Branch> getAllByOrderDESC() {
        BranchDao branchDao = new BranchDao();
        return branchDao.getAll("branch_code", "DESC");
    }

    public void addNew(Branch editedStock) {
        BranchDao branchDao = new BranchDao();
        branchDao.insert(editedStock);
    }

    public void update(Branch editedStock) {
        BranchDao branchDao = new BranchDao();
        branchDao.update(editedStock);
    }

    public void delete(Branch editedStock) {
        BranchDao branchDao = new BranchDao();
        branchDao.delete(editedStock);
    }

    public Branch get(String b) {
        BranchDao branchDao = new BranchDao();
        return branchDao.get(b);
    }
}
