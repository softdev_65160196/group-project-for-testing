/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.ProductDao;
import com.simpledev.d.coffee.models.Product;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author ASUS
 */
public class ProductServices {
    private ProductDao productDao = new ProductDao();
    public ArrayList<Product> getProductsOrderById(){
        return (ArrayList<Product>) productDao.getAll("product_id ASC");
    }
    public ArrayList<Product> getProductsCategoryDrink(){
        return (ArrayList<Product>) productDao.getAllCategory("Drink");
    }
    
     public ArrayList<Product> getProductsCategoryBakery(){
        return (ArrayList<Product>) productDao.getAllCategory("Bakery");
    }
    
    public Product getProduct(int id) {
        ProductDao productDao = new ProductDao();
        return productDao.get(id);
    }
    
    public ArrayList<Product> getAll() {
        ProductDao productDao = new ProductDao();
        return productDao.getAll();
    }
    
    public ArrayList<Product> getAllDESC() {
        ProductDao productDao = new ProductDao();
        return (ArrayList<Product>) productDao.getAll("product_id DESC");
    }
    
    public Product addNew(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.insert(editedProduct);
    }

    public Product update(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.update(editedProduct);
    }

    public void delete(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        productDao.delete(editedProduct);
    }}
