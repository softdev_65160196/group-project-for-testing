/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.InvoiceDetailDao;
import com.simpledev.d.coffee.models.InvoiceDetail;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class InvoiceDetailServices {
    public ArrayList<InvoiceDetail> getAll() {
        InvoiceDetailDao invoiceDetailDao = new InvoiceDetailDao();
        return (ArrayList<InvoiceDetail>) invoiceDetailDao.getAll();
    }
    public List<InvoiceDetail> getUsersByOrder(String where, String order) {
        InvoiceDetailDao invoiceDetailDao = new InvoiceDetailDao();
        return invoiceDetailDao.getAll(where, order);
    }
    
    public List<InvoiceDetail> getInvoiceDetailByOrderDESC() {
        InvoiceDetailDao invoiceDetailDao = new InvoiceDetailDao();
        return invoiceDetailDao.getAll("invoiceDetail_id", "DESC");
    }
    
    public void addNew(InvoiceDetail editedStockDetail) {
       InvoiceDetailDao invoiceDetailDao = new InvoiceDetailDao();
       invoiceDetailDao.insert(editedStockDetail);
    }
    public void update(InvoiceDetail editedStockDetail) {
        InvoiceDetailDao invoiceDetailDao = new InvoiceDetailDao();
        invoiceDetailDao.update(editedStockDetail);
    }
    public void delete(InvoiceDetail editedStockDetail) {
        InvoiceDetailDao invoiceDetailDao = new InvoiceDetailDao();
        invoiceDetailDao.delete(editedStockDetail);
    }
}
