/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.PromotionDetailDao;
import com.simpledev.d.coffee.models.PromotionDetail;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class PromotionDetailService {

    public List<PromotionDetail> getAllPromotionDetail() {
        PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
        return promotionDetailDao.getAll();
    }
    
    public List<PromotionDetail> getPromotionDetailsByOrder(String where, String order) {
        PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
        return promotionDetailDao.getAll(where, order);
    }
    
    public List<PromotionDetail> getPromotionDetailsByOrder() {
        PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
        return promotionDetailDao.getAll("promotionDetail_id", "DESC");
    }

    public void addNew(PromotionDetail editedPromotionDetail) {
        PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
        promotionDetailDao.insert(editedPromotionDetail);
    }

    public void update(PromotionDetail editedPromotionDetail) {
        PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
        promotionDetailDao.update(editedPromotionDetail);
    }

    public void delete(PromotionDetail editedPromotionDetail) {
        PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
        promotionDetailDao.delete(editedPromotionDetail);
    }
}
