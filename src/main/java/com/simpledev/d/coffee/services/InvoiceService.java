/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.InvoiceDao;
import com.simpledev.d.coffee.models.Invoice;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class InvoiceService {
    public ArrayList<Invoice> getAll() {
        InvoiceDao inv= new InvoiceDao();
        return (ArrayList<Invoice>) inv.getAll();
    }
    public List<Invoice> getUsersByOrder(String where, String order) {
        InvoiceDao inv= new InvoiceDao();
        return inv.getAll(where, order);
    }
    
    public List<Invoice> getInvoiceByOrderDESC() {
        InvoiceDao inv= new InvoiceDao();
        return inv.getAll("invoice_id", "DESC");
    }
    
    public void addNew(Invoice editedStockDetail) {
       InvoiceDao inv= new InvoiceDao();
       inv.insert(editedStockDetail);
    }
    public void update(Invoice editedStockDetail) {
        InvoiceDao inv= new InvoiceDao();
        inv.update(editedStockDetail);
    }
    public void delete(Invoice editedStockDetail) {
        InvoiceDao inv= new InvoiceDao();
        inv.delete(editedStockDetail);
    }
}
