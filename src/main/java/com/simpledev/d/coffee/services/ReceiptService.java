
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.EmployeeDao;
import com.simpledev.d.coffee.dao.ReceiptDao;
import com.simpledev.d.coffee.models.Employee;
import com.simpledev.d.coffee.models.Receipt;
import java.util.ArrayList;
import java.util.List;

public class ReceiptService {
    public ArrayList<Receipt> getAll() {
        ReceiptDao receiptDao = new ReceiptDao();
        return (ArrayList<Receipt>) receiptDao.getAll();
    }
    public List<Receipt> getUsersByOrder(String where, String order) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.getAll(where, order);
    }
    
    public List<Receipt> getReceiptByOrder() {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.getAll("receipt_id", "DESC");
    }
    
    public Receipt addNew(Receipt editedReceipt) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.insert(editedReceipt);
    }
    public Receipt update(Receipt editedReceipt) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.update(editedReceipt);
    }
    public int delete(Receipt editedReceipt) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.delete(editedReceipt);
    }

}

