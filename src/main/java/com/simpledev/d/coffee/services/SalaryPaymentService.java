/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.SalaryPaymentDao;
import com.simpledev.d.coffee.models.SalaryPayment;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class SalaryPaymentService {
    public ArrayList<SalaryPayment> getAll() {
        SalaryPaymentDao salaryPaymentDao = new SalaryPaymentDao();
        return (ArrayList<SalaryPayment>) salaryPaymentDao.getAll();
    }
    
    public List<SalaryPayment> getSalaryPaymentsByOrder(String where, String order) {
        SalaryPaymentDao salaryPaymentDao = new SalaryPaymentDao();
        return salaryPaymentDao.getAll(where, order);
    }
    
    public List<SalaryPayment> getSalaryPaymentsByOrderDESC() {
        SalaryPaymentDao salaryPaymentDao = new SalaryPaymentDao();
        return salaryPaymentDao.getAll("salaryPayment_id", "DESC");
    }

    public void addNew(SalaryPayment editedSalaryPayment) {
        SalaryPaymentDao salaryPaymentDao = new SalaryPaymentDao();
        salaryPaymentDao.insert(editedSalaryPayment);
    }

    public void update(SalaryPayment editedSalaryPayment) {
        SalaryPaymentDao salaryPaymentDao = new SalaryPaymentDao();
        salaryPaymentDao.update(editedSalaryPayment);
    }

    public void delete(SalaryPayment editedSalaryPayment) {
        SalaryPaymentDao salaryPaymentDao = new SalaryPaymentDao();
        salaryPaymentDao.delete(editedSalaryPayment);
    }
    
    public ArrayList<SalaryPayment> getByMonth(int month) {
        SalaryPaymentDao salaryPaymentDao = new SalaryPaymentDao();
        return salaryPaymentDao.getByMonth(month);
    }
}
