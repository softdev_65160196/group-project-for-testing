package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.UserDao;
import com.simpledev.d.coffee.models.User;
import java.util.ArrayList;
import java.util.List;

public class UserServices {

    public User login(String login, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByLogin(login);
        if (user != null && user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }

    public ArrayList<User> getAll() {
        UserDao userDao = new UserDao();
        return (ArrayList<User>) userDao.getAll();
    }

    public List<User> getAllByOrder(String where, String order) {
        UserDao userDao = new UserDao();
        return userDao.getAll(where, order);
    }
    
    public List<User> getAllByOrderDESC() {
        UserDao userDao = new UserDao();
        return userDao.getAll("user_id", "DESC");
    }

    public void addNew(User editedUser) {
        UserDao userDao = new UserDao();
        userDao.insert(editedUser);
    }

    public void update(User editedUser) {
        UserDao userDao = new UserDao();
        userDao.update(editedUser);
    }

    public void delete(User editedUser) {
        UserDao userDao = new UserDao();
        userDao.delete(editedUser);
    }

    public User getByLogin(String name) {
        UserDao userDao = new UserDao();
        return userDao.getByLogin(name);
    }

}
