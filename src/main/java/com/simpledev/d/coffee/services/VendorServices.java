
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.VendorDao;
import com.simpledev.d.coffee.models.Vendor;
import java.util.ArrayList;
import java.util.List;


public class VendorServices {
    public ArrayList<Vendor> getAll() {
        VendorDao vendorDao = new VendorDao();
        return vendorDao.getAll();
    }
    public List<Vendor> getUsersByOrder(String where, String order) {
        VendorDao vendorDao = new VendorDao();
        return vendorDao.getAll(where, order);
    }
    
    public List<Vendor> getVendorByOrderDESC() {
        VendorDao vendorDao = new VendorDao();
        return vendorDao.getAll("vendor_code", "DESC");
    }
    
    public void addNew(Vendor editedVendor) {
        VendorDao vendorDao = new VendorDao();
        vendorDao.insert(editedVendor);
    }
    public void update(Vendor editedVendor) {
        VendorDao vendorDao = new VendorDao();
        vendorDao.update(editedVendor);
    }
    public void delete(Vendor editedVendor) {
        VendorDao vendorDao = new VendorDao();
        vendorDao.delete(editedVendor);
    }

}
