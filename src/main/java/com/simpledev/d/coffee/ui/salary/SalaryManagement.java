
package com.simpledev.d.coffee.ui.salary;

import com.simpledev.d.coffee.common.CommonFont;
import com.simpledev.d.coffee.common.SearchSystem;
import com.simpledev.d.coffee.models.SalaryPayment;
import com.simpledev.d.coffee.services.SalaryPaymentService;
import java.util.ArrayList;
import java.util.function.Predicate;
import javax.swing.DefaultComboBoxModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;

public class SalaryManagement extends javax.swing.JPanel {
    
    private SalaryPaymentService salaryPaymentService;
    private ArrayList<SalaryPayment> slpList;
    
    private Predicate<SalaryPayment> filterFunction;
    private SearchSystem<SalaryPayment> searchSystem;
    private String searchValue;
    private int searchValueInt;
    
    /**
     * Creates new form SalaryManagement
     */
    public SalaryManagement() {
        initComponents();
        salaryPaymentService = new SalaryPaymentService();
        slpList = salaryPaymentService.getAll();       
        initTables();
        initSearchSystem();
    }    
    private void setFilter() {
        switch (searchFilter.getSelectedIndex()) {
            case 0 -> {
                // SLP ID
                filterFunction = slp -> slp.getId() == searchValueInt;
            }
            case 1 -> {
                // EMP ID
                filterFunction = slp -> slp.getEmpID()== searchValueInt;
            }
            case 2 -> {
                // DATE
                filterFunction = slp -> slp.getDate().contains(searchValue);
            }
            default ->
                throw new AssertionError();
        }
    }
    
    private int parseInputAsInt(String input) {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            // Handle invalid input here, e.g., display an error message
            return 0; // Default value or any suitable value for invalid input
        }
    }
    
        
        
        
    private void initSearchSystem() {
        searchValue = ""; // Initialize searchValue
        searchValueInt = 0; // Initialize searchValueInt
        setFilter(); // Initialize the filter function
        searchSystem = new SearchSystem<>(searchBar, slpList);
        

        searchBar.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // Handle changedUpdate if needed
            }
        });
    }  
    
    
    private void updateSearchAndReloadTables() {
        searchValue = searchBar.getText().toLowerCase();
        searchValueInt = parseInputAsInt(searchValue);
        setFilter(); // Update the filter function
        searchSystem.update(searchValue, filterFunction);
        reloadTables();
    }
    
    private void reloadTables() {
        slpList = searchSystem.getResults();
        tableEmployeeList.revalidate();
        tableEmployeeList.repaint();
    }
    
     private void refreshTables() {
        slpList = salaryPaymentService.getAll();
        tableEmployeeList.revalidate();
        tableEmployeeList.repaint();
    }
     
    private void refreshTablesForMonth() {
        tableEmployeeList.revalidate();
        tableEmployeeList.repaint();
    } 
        
    private void initTables() {
        String[] filterOptions0 = {"SLP ID", "EMP ID", "DATE"};
        searchFilter.setModel(new DefaultComboBoxModel<>(filterOptions0));
        String[] filterOptions1 = {"All", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        cmbMonth.setModel(new DefaultComboBoxModel<>(filterOptions1));
        cmbMonth.setSelectedIndex(0);
        tableEmployeeList.setFont(CommonFont.getFont());
        
        tableEmployeeList.setModel(new AbstractTableModel() {
            
            String[] headers = {"SalaryPaymentID",
                "EmployeeID",
                "Paid Date",
                "Payment Type",
                "Total Amount"
            };
            @Override
            public String getColumnName(int column) {
                return headers[column];
            }
            @Override
            public int getRowCount() {
                return slpList.size();
            }

            @Override
            public int getColumnCount() {
                return headers.length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                SalaryPayment slp = slpList.get(rowIndex);
                
                switch (columnIndex) {
                    case 0:
                        return slp.getId();
                    case 1:
                        return slp.getEmpID();
                    case 2:
                        return slp.getDate();
                    case 3:
                        return slp.getType();
                    case 4:
                        return slp.getTotalAmount();
                        
                    default :
                        return "unknown";
                }
            }   
        });   
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCategory = new javax.swing.ButtonGroup();
        btnGroupStatusPayment = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        searchBar = new javax.swing.JTextField();
        cmbMonth = new javax.swing.JComboBox<>();
        btnAllItem = new javax.swing.JRadioButton();
        btnPaid = new javax.swing.JRadioButton();
        btnPending = new javax.swing.JRadioButton();
        btnCreate = new javax.swing.JButton();
        searchFilter = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableEmployeeList = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 204, 204));

        jPanel1.setBackground(new java.awt.Color(255, 153, 153));

        jLabel3.setText("Employee Search");

        searchBar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchBarActionPerformed(evt);
            }
        });

        cmbMonth.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" }));
        cmbMonth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbMonthActionPerformed(evt);
            }
        });

        btnGroupStatusPayment.add(btnAllItem);
        btnAllItem.setSelected(true);
        btnAllItem.setText("All items");

        btnGroupStatusPayment.add(btnPaid);
        btnPaid.setText("Paid");

        btnGroupStatusPayment.add(btnPending);
        btnPending.setText("Pending");

        btnCreate.setText("Create");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });

        searchFilter.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        searchFilter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchFilterActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(searchBar)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(cmbMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnAllItem)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnPaid)
                                .addGap(18, 18, 18)
                                .addComponent(btnPending)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 535, Short.MAX_VALUE)
                                .addComponent(btnCreate)
                                .addGap(14, 14, 14))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAllItem)
                    .addComponent(btnPaid)
                    .addComponent(btnPending)
                    .addComponent(btnCreate))
                .addGap(0, 9, Short.MAX_VALUE))
        );

        tableEmployeeList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Period ", "Paid Date", "Employee ID", "Name", "Time Worked", "Number of days", "Net Salary", "State"
            }
        ));
        jScrollPane1.setViewportView(tableEmployeeList);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 948, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    
    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnCreateActionPerformed

    private void cmbMonthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbMonthActionPerformed
        switch (cmbMonth.getSelectedIndex()) {
            case 0:
                slpList = salaryPaymentService.getAll();
                refreshTablesForMonth();
                break;            
            case 1:
                slpList = salaryPaymentService.getByMonth(1);
                refreshTablesForMonth();
                break;
            case 2:
                slpList = salaryPaymentService.getByMonth(2);
                refreshTablesForMonth();
                break;
            case 3:
                slpList = salaryPaymentService.getByMonth(3);
                refreshTablesForMonth();
                break;
            case 4:
                slpList = salaryPaymentService.getByMonth(4);
                refreshTablesForMonth();
                break;
            case 5:
                slpList = salaryPaymentService.getByMonth(5);
                refreshTablesForMonth();
                break;
            case 6:
                slpList = salaryPaymentService.getByMonth(6);
                refreshTablesForMonth();
                break;
            case 7:
                slpList = salaryPaymentService.getByMonth(7);
                refreshTablesForMonth();
                break;
            case 8:
                slpList = salaryPaymentService.getByMonth(8);
                refreshTablesForMonth();
                break;
            case 9:
                slpList = salaryPaymentService.getByMonth(9);
                refreshTablesForMonth();
                break;
            case 10:
                slpList = salaryPaymentService.getByMonth(10);
                refreshTablesForMonth();
                break;
            case 11:
                slpList = salaryPaymentService.getByMonth(11);
                refreshTablesForMonth();
                break;
            case 12:
                slpList = salaryPaymentService.getByMonth(12);
                refreshTablesForMonth();
                break;
        }
    }//GEN-LAST:event_cmbMonthActionPerformed

    private void searchFilterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchFilterActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchFilterActionPerformed

    private void searchBarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchBarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchBarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton btnAllItem;
    private javax.swing.ButtonGroup btnCategory;
    private javax.swing.JButton btnCreate;
    private javax.swing.ButtonGroup btnGroupStatusPayment;
    private javax.swing.JRadioButton btnPaid;
    private javax.swing.JRadioButton btnPending;
    private javax.swing.JComboBox<String> cmbMonth;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField searchBar;
    private javax.swing.JComboBox<String> searchFilter;
    private javax.swing.JTable tableEmployeeList;
    // End of variables declaration//GEN-END:variables
}
