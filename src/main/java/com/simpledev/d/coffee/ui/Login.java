package com.simpledev.d.coffee.ui;

import com.simpledev.d.coffee.common.CommonFont;
import com.simpledev.d.coffee.component.RoundedButton;
import com.simpledev.d.coffee.models.Employee;
import com.simpledev.d.coffee.models.User;
import com.simpledev.d.coffee.publisher.LoginCallback;
import com.simpledev.d.coffee.services.UserServices;
import com.simpledev.d.coffee.component.RoundedPanel;
import java.awt.Color;
import java.awt.Image;
import java.awt.geom.RoundRectangle2D;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Login extends javax.swing.JFrame {

    private User user;
    private final UserServices userServices;
    private static LoginCallback callback;

    public Login(LoginCallback callback) {
        initFrame();
        initComponents();
        initBoxLogin();
        userServices = new UserServices();
        Login.callback = callback;
        initImage();
        this.getRootPane().setDefaultButton(btnLogin);
        this.setLocationRelativeTo(null);
    }
    
    public static void setListenr(LoginCallback callback){
        Login.callback = callback;
    }

    private void initBoxLogin() {
        pnlFormLogin.setBackground(new Color(95, 51, 29));
        lbHeader.setForeground(Color.white);
        lbUsername.setForeground(Color.white);
        lbPassword.setForeground(Color.white);
        lbHeader.setFont(CommonFont.getFontTitle());
        lbUsername.setFont(CommonFont.getFontTitle());
        lbPassword.setFont(CommonFont.getFontTitle());
        tfUsername.setFont(CommonFont.getFontTitle());
        tfPassword.setFont(CommonFont.getFontTitle());
    }

    private void initFrame() {
        int width = 815;
        int hight = 495;
        setUndecorated(true);
        setShape(new RoundRectangle2D.Double(0, 0, width, hight, 20, 20));
        setResizable(false);

    }

    private void initImage() {
        try {
            ImageIcon icon = new ImageIcon("assets/LoginLogo.png");
            Image img = icon.getImage();
            Image imgScale = img.getScaledInstance(labelImage.getWidth(), labelImage.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon scaledIcon = new ImageIcon(imgScale);
            labelImage.setIcon(scaledIcon);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        labelImage = new javax.swing.JLabel();
        pnlFormLogin = new RoundedPanel();
        btnLogin = new javax.swing.JButton();
        tfPassword = new javax.swing.JPasswordField();
        tfUsername = new javax.swing.JTextField();
        lbHeader = new javax.swing.JLabel();
        lbUsername = new javax.swing.JLabel();
        lbPassword = new javax.swing.JLabel();
        btnExit = new RoundedButton("x", new Color(255, 255, 255, 70), new Color(10, 10, 10));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Login");
        setBackground(new java.awt.Color(30, 13, 3));
        setName("LoginFrame"); // NOI18N
        setResizable(false);

        jPanel2.setBackground(new java.awt.Color(30, 13, 3));

        labelImage.setBackground(new java.awt.Color(204, 204, 255));
        labelImage.setMaximumSize(new java.awt.Dimension(100, 100));

        btnLogin.setText("Login");
        btnLogin.addActionListener(this::btnLoginActionPerformed);

        tfPassword.setFont(new java.awt.Font("DB Adman X", 0, 14)); // NOI18N
        tfPassword.addActionListener((java.awt.event.ActionEvent evt) -> {
        });

        tfUsername.setFont(new java.awt.Font("DB Adman X", 0, 24)); // NOI18N

        lbHeader.setFont(new java.awt.Font("DB Adman X", 1, 24)); // NOI18N
        lbHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbHeader.setText("Welcome");

        lbUsername.setFont(new java.awt.Font("DB Adman X", 0, 24)); // NOI18N
        lbUsername.setText("Username :");

        lbPassword.setFont(new java.awt.Font("DB Adman X", 0, 24)); // NOI18N
        lbPassword.setText("Password :");

        javax.swing.GroupLayout pnlFormLoginLayout = new javax.swing.GroupLayout(pnlFormLogin);
        pnlFormLogin.setLayout(pnlFormLoginLayout);
        pnlFormLoginLayout.setHorizontalGroup(
                pnlFormLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlFormLoginLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(lbHeader)
                                .addGap(162, 162, 162))
                        .addGroup(pnlFormLoginLayout.createSequentialGroup()
                                .addGroup(pnlFormLoginLayout
                                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(pnlFormLoginLayout.createSequentialGroup()
                                                .addGap(112, 112, 112)
                                                .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 175,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(pnlFormLoginLayout.createSequentialGroup()
                                                .addGap(18, 18, 18)
                                                .addGroup(pnlFormLoginLayout
                                                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING,
                                                                false)
                                                        .addComponent(lbUsername)
                                                        .addComponent(lbPassword)
                                                        .addComponent(tfUsername, javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                360, Short.MAX_VALUE)
                                                        .addComponent(tfPassword))))
                                .addContainerGap(22, Short.MAX_VALUE))

                        .addGap(0, 104, Short.MAX_VALUE)
                        .addComponent(lbHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 194,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(102, 102, 102));
        pnlFormLoginLayout.setVerticalGroup(
                pnlFormLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlFormLoginLayout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addComponent(lbHeader)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lbUsername)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(tfUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 40,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addComponent(lbPassword)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tfPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 40,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32)
                                .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 35,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(17, 17, 17)));

        btnExit.addActionListener(this::btnExitActionPerformed);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(labelImage, javax.swing.GroupLayout.PREFERRED_SIZE, 380,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(pnlFormLogin, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 20,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(17, 17, 17)));
        jPanel2Layout.setVerticalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(labelImage, javax.swing.GroupLayout.DEFAULT_SIZE, 466, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 21,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(pnlFormLogin, javax.swing.GroupLayout.PREFERRED_SIZE,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE));
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)));

        pack();
    }// </editor-fold>                        

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnLoginActionPerformed
        char[] passwordChars = tfPassword.getPassword();
        String password = new String(passwordChars);
        user = userServices.login(tfUsername.getText(), password);

        if (user == null) {
            JOptionPane.showMessageDialog(this, "User not found, Please check your username and password.",
                    "Login Failed", JOptionPane.WARNING_MESSAGE);
        } else {
            Employee emp = user.getEmployee();
            if (emp.getTitle().equals("เจ้าของร้าน")) {
                callback.onLoginSuccess(user, 0);
            } else {
                callback.onLoginSuccess(user, 1);
            }
            dispose();
        }
    }

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnExitActionPerformed
        dispose();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnLogin;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel labelImage;
    private javax.swing.JLabel lbHeader;
    private javax.swing.JLabel lbPassword;
    private javax.swing.JLabel lbUsername;
    private javax.swing.JPanel pnlFormLogin;
    private javax.swing.JPasswordField tfPassword;
    private javax.swing.JTextField tfUsername;
    // End of variables declaration//GEN-END:variables

}
