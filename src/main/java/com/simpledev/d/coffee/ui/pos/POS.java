package com.simpledev.d.coffee.ui.pos;

import com.simpledev.d.coffee.component.ProductListPanel;
import com.simpledev.d.coffee.models.Product;
import com.simpledev.d.coffee.models.InvoiceDetail;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import com.simpledev.d.coffee.common.CommonFont;
import com.simpledev.d.coffee.component.ProductListBakeryPanel;
import com.simpledev.d.coffee.models.Branch;
import com.simpledev.d.coffee.models.Customer;
import com.simpledev.d.coffee.models.Invoice;
import com.simpledev.d.coffee.models.Promotion;
import com.simpledev.d.coffee.models.User;
import com.simpledev.d.coffee.publisher.BuyProductable;
import com.simpledev.d.coffee.services.CustomerServices;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import com.simpledev.d.coffee.publisher.ConfirmListener;
import com.simpledev.d.coffee.services.BranchServices;
import com.simpledev.d.coffee.services.PromotionService;
import com.simpledev.d.coffee.ui.customer.CustomerDialog;
import com.simpledev.d.coffee.ui.invoice.InvoicePosDialog;
import com.simpledev.d.coffee.ui.promotion.PromotionDialogPos;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.LocalDateTime;
import javax.swing.JOptionPane;

public class POS extends javax.swing.JPanel implements BuyProductable, ConfirmListener<Promotion> {

    private ArrayList<InvoiceDetail> invoiceDetail;

    // ArrayList<Product> products;
    // ProductServices productService = new ProductServices();
    // ReceiptService receiptService = new ReceiptService();
    // Receipt receipt;
    // private ProductListPanel productListPanel;
    private Invoice invoice;
    private final User user;
    private ProductListPanel productListPanel;
    private ProductListBakeryPanel productListBakeryPanel;
    private final CustomerServices customerServices = new CustomerServices();
    private double totalPrice = 0;
    private double discountAmount = 0;
    private double netPrice = 0;
    private int getMoney = 0;
    private double change = 0;

    int point;
    int usedPoint;
    int qty2point = 0;
    int totalQty = 0;
    private int queue = 1;

    private Promotion selectedPromotion;

    public POS(User user) {
        this.user = user;
//        this.setPreferredSize(new Dimension(1155, 581));
        this.setBounds(0, 0, 1155, 581);
        initComponents();
        selectedPromotion = new Promotion();
        // initProductTable();
        // receipt = new Receipt();
        initReceiptTables();
        // init product list panel
        productListPanel = new ProductListPanel();
        productListBakeryPanel = new ProductListBakeryPanel();
        scrProductList.setViewportView(productListPanel);
        Customer customer = new Customer();

        // set listener
        PromotionDialogPos.setListener(this);
        productListPanel.addOnBuyProduct(this);
        productListBakeryPanel.addOnBuyProduct(this);

        txtTel.setEnabled(false);
        txtPoint.setEnabled(false);
        btnPromotion.setEnabled(false);
        addTxtPayMouseListener();
    }

    private void addTxtPayMouseListener() {
        // txtPay.setEnabled(false);
        // if(cbPay.getSelectedItem() =="PromptPay"){
        // txtPay.setEnabled(false);
        // }else if(cbPay.getSelectedItem() =="Cash"){
        // txtPay.setEnabled(false);
        // }else if(cbPay.getSelectedItem() =="Cash"){
        // txtPay.setEnabled(true);
        // }
        txtPay.addMouseListener(new  MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                txtPay.setText(""); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
            }
            
        });
        txtPoint.addMouseListener(new  MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                txtPoint.setText(""); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
            }
            
        });
    }

    private void initReceiptTables() {
        tblReceiptDetail.setFont(CommonFont.getFont());
        invoiceDetail = new ArrayList<>();
        tblReceiptDetail.setModel(new AbstractTableModel() {
            String[] headers = {"List Order", "Price", "Amount", "Total"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return invoiceDetail.size();
            }

            @Override
            public int getColumnCount() {
                return headers.length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                InvoiceDetail inv = invoiceDetail.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return inv.getProduct().getName();
                    case 1:
                        return inv.getUnitprice();
                    case 2:
                        return inv.getUnit();
                    case 3:
                        return inv.getUnit() * inv.getUnitprice();
                    default:
                        return "";
                }
            }

        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        btnDrink = new javax.swing.JButton();
        btnBakery = new javax.swing.JButton();
        scrProductList = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblReceiptDetail = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        labelPoint = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        labelMember = new javax.swing.JLabel();
        labelHBD = new javax.swing.JLabel();
        txtTel = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        labelPromotion = new javax.swing.JLabel();
        labelRemainingPoint = new javax.swing.JLabel();
        labelGivenPoint = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtPoint = new javax.swing.JTextField();
        jPanel8 = new javax.swing.JPanel();
        labelNetPrice = new javax.swing.JLabel();
        labelDiscount = new javax.swing.JLabel();
        labelTotal = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        labelTotalUnit = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        cbPay = new javax.swing.JComboBox<>();
        jLabel18 = new javax.swing.JLabel();
        labelChange = new javax.swing.JLabel();
        labelPaymentStatus = new javax.swing.JLabel();
        txtPay = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        PanelBtn = new javax.swing.JPanel();
        btnRegister = new javax.swing.JButton();
        btnPromotion = new javax.swing.JButton();
        btnConfirm = new javax.swing.JButton();
        btnReset = new javax.swing.JButton();
        btnCalculate = new javax.swing.JButton();
        btnMemberSearch = new javax.swing.JButton();

        setBackground(new java.awt.Color(204, 204, 204));

        jPanel1.setBackground(new java.awt.Color(204, 153, 0));
        jPanel1.setForeground(new java.awt.Color(255, 204, 204));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Product List");

        btnDrink.setText("Drink");
        btnDrink.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDrinkActionPerformed(evt);
            }
        });

        btnBakery.setText("Bakery");
        btnBakery.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBakeryActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrProductList)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnDrink)
                        .addGap(18, 18, 18)
                        .addComponent(btnBakery))
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(357, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDrink)
                    .addComponent(btnBakery))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(scrProductList, javax.swing.GroupLayout.PREFERRED_SIZE, 514, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(209, 209, 209))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 204));

        tblReceiptDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "No.", "List Order", "Amount", "Price (Baht)"
            }
        ));
        jScrollPane1.setViewportView(tblReceiptDetail);

        jPanel3.setBackground(new java.awt.Color(255, 255, 204));

        jLabel3.setText("Member :");

        jLabel7.setText("Point :");

        labelPoint.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelPoint.setText("-");

        jLabel16.setText("Point ");

        jLabel4.setText("Birthdate :");

        jLabel6.setText("Tel. :");

        labelMember.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        labelMember.setText("-");

        labelHBD.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        labelHBD.setText("-");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel3)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(labelPoint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(12, 12, 12)
                                .addComponent(jLabel16))
                            .addComponent(labelMember, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(labelHBD, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(23, 23, 23))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(34, 34, 34)
                        .addComponent(txtTel, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(labelMember))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(labelHBD))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtTel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(labelPoint)
                    .addComponent(jLabel16))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 204));

        jLabel8.setText("Point ");

        jLabel21.setText("Point ");

        jLabel19.setText("Point ");

        labelPromotion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelPromotion.setText("-");

        labelRemainingPoint.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelRemainingPoint.setText("-");

        labelGivenPoint.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelGivenPoint.setText("-");

        jLabel9.setText("Given Point :");

        jLabel10.setText("Used Point:");

        jLabel11.setText("Remaining Point :");

        jLabel12.setText("Promotion:");

        txtPoint.setForeground(new java.awt.Color(153, 153, 153));
        txtPoint.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtPoint.setText("0");
        txtPoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPointActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(labelGivenPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(58, 58, 58))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtPoint)
                                .addGap(56, 56, 56))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(labelRemainingPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(64, 64, 64)))
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel19)
                            .addComponent(jLabel8)
                            .addComponent(jLabel21))
                        .addGap(18, 18, 18))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 53, Short.MAX_VALUE)
                                .addComponent(labelPromotion, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel11))
                        .addGap(39, 39, 39))))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel8)
                        .addComponent(labelGivenPoint)))
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel19)
                            .addComponent(jLabel10))
                        .addGap(21, 21, 21))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelRemainingPoint)
                            .addComponent(jLabel21))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelPromotion))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel12)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel8.setBackground(new java.awt.Color(255, 255, 204));

        labelNetPrice.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelNetPrice.setText("-");

        labelDiscount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelDiscount.setText("-");

        labelTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelTotal.setText("-");

        jLabel22.setText("฿");

        jLabel23.setText("฿");

        jLabel24.setText("฿");

        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel15.setText("Net Price");

        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel14.setText("Discount");

        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel13.setText("Total");

        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel20.setText("Total Unit");

        labelTotalUnit.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelTotalUnit.setText("-");

        jLabel25.setText("Item");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addGap(23, 23, 23))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(21, 21, 21))
                            .addComponent(jLabel15))
                        .addGap(17, 17, 17))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(8, 8, 8)))
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelTotalUnit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelNetPrice, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelTotal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelDiscount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel24, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel23, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel22, javax.swing.GroupLayout.Alignment.TRAILING))
                    .addComponent(jLabel25))
                .addGap(26, 26, 26))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(jLabel25)
                    .addComponent(labelTotalUnit))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(jLabel23)
                    .addComponent(labelTotal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jLabel24)
                    .addComponent(labelDiscount))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jLabel22)
                    .addComponent(labelNetPrice)))
        );

        jPanel9.setBackground(new java.awt.Color(255, 255, 204));

        jLabel17.setText("Change :");

        cbPay.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Cash", "PromptPay", "Credit Card" }));

        jLabel18.setText("Payment Status :");

        labelChange.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelChange.setText("-");

        labelPaymentStatus.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelPaymentStatus.setText("-");

        txtPay.setForeground(new java.awt.Color(153, 153, 153));
        txtPay.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtPay.setText("0.00");

        jLabel26.setText("฿");

        jLabel28.setText("฿");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(cbPay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18))
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGap(66, 66, 66)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(labelChange, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelPaymentStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtPay, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)))
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel26))
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbPay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtPay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(labelChange)
                    .addComponent(jLabel28))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelPaymentStatus, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(6, 6, 6)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        PanelBtn.setBackground(new java.awt.Color(255, 204, 102));

        btnRegister.setText("Register");
        btnRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterActionPerformed(evt);
            }
        });

        btnPromotion.setText("Promotion");
        btnPromotion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPromotionActionPerformed(evt);
            }
        });

        btnConfirm.setText("Confirm");
        btnConfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmActionPerformed(evt);
            }
        });

        btnReset.setText("Reset Order");
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });

        btnCalculate.setText("Calculate");
        btnCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalculateActionPerformed(evt);
            }
        });

        btnMemberSearch.setText("Member Search");
        btnMemberSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMemberSearchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelBtnLayout = new javax.swing.GroupLayout(PanelBtn);
        PanelBtn.setLayout(PanelBtnLayout);
        PanelBtnLayout.setHorizontalGroup(
            PanelBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelBtnLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelBtnLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelBtnLayout.createSequentialGroup()
                        .addComponent(btnMemberSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(PanelBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnConfirm, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(PanelBtnLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnRegister, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnPromotion, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnCalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(34, 34, 34))
        );
        PanelBtnLayout.setVerticalGroup(
            PanelBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelBtnLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCalculate)
                    .addComponent(btnRegister)
                    .addComponent(btnPromotion)
                    .addComponent(btnMemberSearch))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnConfirm)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnReset)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PanelBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PanelBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnResetActionPerformed
        invoiceDetail.clear();
        toZero();
        qty2point = 0;
        totalQty = 0;
        toZero();
        reloadPromoText();
        refreshList();
        txtTel.setEnabled(false);
        labelPromotion.setText("-");
        btnPromotion.setEnabled(false);
    }// GEN-LAST:event_btnResetActionPerformed

    private void btnConfirmActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnConfirmActionPerformed
        this.point = point + totalQty;
        if (user != null) {
            if (cbPay.getSelectedItem() == "Cash") {
                // netPrice = Double.parseDouble(labelNetPrice.getText());
                // getMoney = Integer.parseInt(txtPay.getText());
                // change = Double.parseDouble(labelChange.getText());

                BranchServices branchServices = new BranchServices();
                Branch branch = branchServices.get("BUU");

                LocalDateTime currentDateTime = LocalDateTime.now();

                invoice = new Invoice(customerServices.getByTel(txtTel.getText()),
                        user.getEmployee(), branch, selectedPromotion, currentDateTime,
                        totalQty, point, discountAmount, netPrice, getMoney, change,
                        invoiceDetail);

                openDialogInvoice();

            } else if (cbPay.getSelectedItem() == "PromptPay") {
                openDialogPromptPay();
            } else if (cbPay.getSelectedItem() == "Credit Card") {
                openDialogCredit();

            }
        }

        queue += +1;
        // RandomTaxID();

    }// GEN-LAST:event_btnConfirmActionPerformed

    private void btnRegisterActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnRegisterActionPerformed
        openDialogRegister();
    }// GEN-LAST:event_btnRegisterActionPerformed

    private void btnPromotionActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnPromotionActionPerformed
        openDialogPromotion();
    }// GEN-LAST:event_btnPromotionActionPerformed

    private void btnCalculateActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnCalculateActionPerformed
        if (getMoney <= 0) {
            JOptionPane.showConfirmDialog(this, "Please input your money.", "Money",
                    JOptionPane.OK_OPTION, JOptionPane.WARNING_MESSAGE);
        }
        // calculate();
        labelTotalUnit.setText(Integer.toString(totalQty));
        this.getMoney = Integer.parseInt(txtPay.getText());
        this.change = getMoney - netPrice;

        if (cbPay.getSelectedItem() == "Cash") {
            labelChange.setText(Double.toString(change));
        } else {
            labelPaymentStatus.setFont(CommonFont.getFont());
            labelPaymentStatus.setText("รอดำเนินการ...");
        }
        labelGivenPoint.setText(Integer.toString(totalQty));
        labelRemainingPoint.setText(Integer.toString(totalQty + point));

        promotionPoint(getMoney);

    }// GEN-LAST:event_btnCalculateActionPerformed

    private void promotionPoint(double money) throws NumberFormatException {
        if (point >= 10) {
            txtPoint.setEnabled(true);
            txtPoint.setText("");
            txtPoint.requestFocus();
            txtPoint.setFont(CommonFont.getFont());
            this.usedPoint = Integer.parseInt(txtPoint.getText());

            if (usedPoint >= 100) {
                discountAmount = 50;
                money -= discountAmount;
                point -= 100;
                discountCalPromotion();

                // Double.parseDouble(labelDiscount.setText());
            } else if (usedPoint >= 50) {
                discountAmount = 25;
                money -= discountAmount;
                point -= 50;
                discountCalPromotion();
                System.out.println(money);
                System.out.println(point);
            } else if (usedPoint >= 25) {
                discountAmount = 15;
                money -= discountAmount;
                point -= 25;
                discountCalPromotion();
                System.out.println("promotionPoint : " + point);
            }
        }
    }

    private void discountCalPromotion() throws NumberFormatException {
        labelDiscount.setText("-" + discountAmount);
        labelRemainingPoint.setText(Integer.toString(totalQty + point));
        getMoney = Integer.parseInt(txtPay.getText());
        netPrice = totalPrice - discountAmount;
        change = getMoney - netPrice;
        labelChange.setText(Double.toString(change));
        labelNetPrice.setText(Double.toString(totalPrice - discountAmount));
    }

    private void btnMemberSearchActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnMemberSearchActionPerformed
        txtTel.setEnabled(true);

        Customer currentCustomer = customerServices.getByTel(txtTel.getText());
        if (currentCustomer != null) {
            labelMember.setText(currentCustomer.getFirstName() + " " + currentCustomer.getLastName());
            labelHBD.setText(currentCustomer.getBirthDate());
            labelPoint.setText(Integer.toString(currentCustomer.getPoint()));
        } else {
            System.out.println("not found");
        }
        // fName =
        // String member;
        // member = labelMember.getText();
//        System.out.println(customerServices.getByTel(txtTel.getText()));
//        System.out.println(customerServices.getByFName(labelMember.getText()));
        point = currentCustomer.getPoint();
        // tel = txtTel.getText();
//         searchMember(tel);
        // fName = labelMember.getText();
        if (currentCustomer.getFirstName() != "") {
            btnPromotion.setEnabled(true);
        }

    }// GEN-LAST:event_btnMemberSearchActionPerformed

    private void btnDrinkActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnDrinkActionPerformed
        scrProductList.setViewportView(productListPanel);
    }// GEN-LAST:event_btnDrinkActionPerformed

    private void btnBakeryActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnBakeryActionPerformed

        // TODO add your handling code here:
        scrProductList.setViewportView(productListBakeryPanel);

    }// GEN-LAST:event_btnBakeryActionPerformed

    private void txtPointActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_txtPointActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_txtPointActionPerformed

    private void calculate() {
        invoiceDetail.forEach((n) -> calculateTotal(n.getUnit() * n.getUnitprice()));
        invoiceDetail.forEach((n) -> calculateDiscount(n.getDiscount()));
        invoiceDetail.forEach((n) -> addTotalQty(n.getUnit()));
        netPrice = totalPrice - discountAmount;
    }

    private double calculateTotal(double total) {
        return totalPrice += total;
    }

    private double calculateDiscount(double discount) {
        return discountAmount += discount;
    }

    private int addTotalQty(int qty) {
        return totalQty += qty;
    }

    private void toZero() {
        totalPrice = 0;
        discountAmount = 0;
        netPrice = 0;
        totalQty = 0;
        change = 0;
        labelTotalUnit.setText("-");
        txtPay.setText("0.00");
        labelChange.setText("-");
        labelPaymentStatus.setText("-");
        labelMember.setText("-");
        labelHBD.setText("-");
        txtTel.setText("");
        labelPoint.setText("-");
        labelGivenPoint.setText("-");
        labelRemainingPoint.setText("-");
        txtPoint.setText("0");
        labelPromotion.setText("-");
        selectedPromotion = null;
        labelDiscount.setForeground(Color.BLACK);
        labelNetPrice.setForeground(Color.BLACK);
    }

    // private void openDialog() {
    // JFrame frame = (JFrame) SwingUtilities.getRoot(this);
    // RegisterDialog registerDialog = new RegisterDialog();
    // RegisterDialog.setLocationRelativeTo(this);
    // RegisterDialog.setVisible(true);
    // RegisterDialog.addWindowListener(new WindowAdapter() {
    // @Override
    // public void windowClosed(WindowEvent e) {
    // refreshTable();
    // }
    //
    // });
    // }
    private void openDialogRegister() {
        Customer editedCustomer = new Customer();

        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        CustomerDialog customerDialog = new CustomerDialog(frame, editedCustomer);
        customerDialog.setTitle("Register Member");
        customerDialog.setLocationRelativeTo(this);
        customerDialog.setVisible(true);

    }

    private void openDialogPromotion() {
        // Promotion selectPromotion = new Promotion();
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        PromotionDialogPos promotionDialogPos = new PromotionDialogPos(frame);
        promotionDialogPos.setTitle("Promotion");
        promotionDialogPos.setLocationRelativeTo(this);
        promotionDialogPos.setVisible(true);
        promotionDialogPos.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                reloadPromoText();
                txtPoint.setForeground(Color.black);
                txtPoint.setText("");
                txtPoint.requestFocus();
            }
        });
    }

    private void openDialogPromptPay() {
        PromptPayDialog promptpayDialog = new PromptPayDialog();
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        promptpayDialog.setTitle("Payment");
        promptpayDialog.setLocationRelativeTo(this);
        promptpayDialog.setVisible(true);
        promptpayDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                openDialogInvoice();
            }
        });

    }

    private void openDialogCredit() {
        CreditDialog creditDialog = new CreditDialog();
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        creditDialog.setTitle("Payment");
        creditDialog.setLocationRelativeTo(this);
        creditDialog.setVisible(true);
        creditDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                openDialogInvoice();
            }
        });

    }

    private void openDialogInvoice() {

        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        InvoicePosDialog invoiceDialog = new InvoicePosDialog(frame, invoice, (String) cbPay.getSelectedItem(), qty2point, usedPoint, point, getMoney, queue);
        invoiceDialog.setTitle("Confirm Receipt");
        invoiceDialog.setLocationRelativeTo(this);
        invoiceDialog.setVisible(true);
        labelPaymentStatus.setFont(CommonFont.getFont());
        labelPaymentStatus.setText("ชำระเงินสำเร็จ");
        labelPaymentStatus.setForeground(Color.green);

        // invoiceDialog.add(this);
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel PanelBtn;
    private javax.swing.JButton btnBakery;
    private javax.swing.JButton btnCalculate;
    private javax.swing.JButton btnConfirm;
    private javax.swing.JButton btnDrink;
    private javax.swing.JButton btnMemberSearch;
    private javax.swing.JButton btnPromotion;
    private javax.swing.JButton btnRegister;
    private javax.swing.JButton btnReset;
    private javax.swing.JComboBox<String> cbPay;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelChange;
    private javax.swing.JLabel labelDiscount;
    private javax.swing.JLabel labelGivenPoint;
    private javax.swing.JLabel labelHBD;
    private javax.swing.JLabel labelMember;
    private javax.swing.JLabel labelNetPrice;
    private javax.swing.JLabel labelPaymentStatus;
    private javax.swing.JLabel labelPoint;
    private javax.swing.JLabel labelPromotion;
    private javax.swing.JLabel labelRemainingPoint;
    private javax.swing.JLabel labelTotal;
    private javax.swing.JLabel labelTotalUnit;
    private javax.swing.JScrollPane scrProductList;
    private javax.swing.JTable tblReceiptDetail;
    private javax.swing.JTextField txtPay;
    private javax.swing.JTextField txtPoint;
    private javax.swing.JTextField txtTel;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Product p, int qty) {
        // create rd for insert to list<rd>
        InvoiceDetail invd = new InvoiceDetail();
        invd.setProduct(p);
        invd.setUnit(qty);
        invd.setUnitprice(p.getPrice());
        invd.setNetPrice(netPrice);
        this.invoiceDetail.add(invd);
        toZero();
        calculate();
        reloadPromoText();
        refreshList();
        qty2point += qty;
        qty2point += point;
    }

    // public boolean isContain(InvoiceDetail n, Product p) {
    // return n.getProduct().getId() == p.getId();
    // }
    private void refreshList() {
        // reloading tables
        tblReceiptDetail.revalidate();
        tblReceiptDetail.repaint();
    }

    private void enableForm(boolean status) {
        txtTel.setEditable(status);
    }

    public void searchMember(Customer customer) {
        labelMember.setText(customer.getFirstName() + " " + customer.getLastName());
        labelHBD.setText(customer.getBirthDate());
        labelPoint.setText(String.valueOf(customer.getPoint()));

        System.out.println(point);

    }

    private void reloadPromoText() {
        if (selectedPromotion != null) {
            labelPromotion.setText(selectedPromotion.getName());

        }

        labelTotal.setText(Double.toString(totalPrice));
        labelDiscount.setText(Double.toString(discountAmount));
        labelNetPrice.setText(Double.toString(netPrice));
        labelTotalUnit.setText(Integer.toString(totalQty));
        labelChange.setText(Double.toString(change));
        if (totalPrice != netPrice) {
            labelDiscount.setForeground(Color.red);
            labelNetPrice.setForeground(Color.green);
        }

        // System.out.println(promotionName);
        // labelPromotion.setText(promotionName);
        // promotionService.getByName(promotionName);
        // System.out.println(promotionService.getByName(promotionName));
    }
    // public void RandomTaxID() {
    //
    // Random random = new Random();
    // StringBuilder tax_Id = new StringBuilder();
    //
    // for (int i = 0; i < 13; i++) {
    // int digit = random.nextInt(10);
    // tax_Id.append(digit);
    // }
    //
    // System.out.println("Tax ID : " + tax_Id.toString());
    // taxId = tax_Id.toString();
    //
    // }

    // on promotion confirm
    @Override
    public void onSetConfirm(Promotion promotion) {
        discountAmount = totalPrice * promotion.getDiscount();
        netPrice = totalPrice - discountAmount;
        selectedPromotion = promotion;
    }

}
