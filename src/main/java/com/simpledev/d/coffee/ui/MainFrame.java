package com.simpledev.d.coffee.ui;

import com.simpledev.d.coffee.common.CommonDateTime;
import com.simpledev.d.coffee.ui.receipt.ReceiptRecords;
import com.simpledev.d.coffee.ui.invoice.InvoiceManagement;
import com.simpledev.d.coffee.ui.promotion.PromotionManagement;
import com.simpledev.d.coffee.ui.product.ProductManagement;
import com.simpledev.d.coffee.ui.customer.CustomerManagement;
import com.simpledev.d.coffee.ui.stock.StockManagement;
import com.simpledev.d.coffee.ui.pos.POS;
import com.simpledev.d.coffee.ui.employee.EmployeeManagement;
import com.simpledev.d.coffee.ui.stock.CheckStockRecords;
import com.simpledev.d.coffee.ui.check_time.CheckTimeRecords;
import com.simpledev.d.coffee.ui.report.Dashboard;
import com.simpledev.d.coffee.common.CommonFont;
import com.simpledev.d.coffee.component.RoundedButton;
import com.simpledev.d.coffee.models.Employee;
import com.simpledev.d.coffee.models.User;
import com.simpledev.d.coffee.publisher.LoginCallback;
import com.simpledev.d.coffee.ui.salary.SalaryManagement;
import com.simpledev.d.coffee.ui.user.UserManagement;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.RoundRectangle2D;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class MainFrame extends javax.swing.JFrame implements LoginCallback {

    private User user;
//    private final int role;

    public MainFrame(User user) {
        this.user = user;
        initFrame();
        initComponents();
        reloadUser();
        lbDateTime.setForeground(Color.white);
        panelMenu.setLayout(new GridLayout(13, 0, 0, 6));
        initFrameOperator();
        mainView.setViewportView(new Dashboard());
        initLogoImg();
        labelStatus.setFont(CommonFont.getFont());

        Timer timer = new Timer(1000, (ActionEvent e) -> {
            lbDateTime.setText("Date: " + CommonDateTime.getDateTimeNowString());
        });
        timer.start();

        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void initFrameOperator() {
//        panelTitle.setOpaque(false);
        panelTitle.setBackground(new Color(30, 13, 3));
        btnExit.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                btnExit.setText("x");
                btnHide.setText("-");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                btnExit.setText("");
                btnHide.setText("");
            }

        });

        btnHide.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                setState(ICONIFIED);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                btnExit.setText("x");
                btnHide.setText("-");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                btnExit.setText("");
                btnHide.setText("");
            }
        });

    }

    private void initFrame() {
        final int width = 1366;
        final int height = 768;
        setPreferredSize(new Dimension(width, height));
        setUndecorated(true);
        setShape(new RoundRectangle2D.Double(0, 0, width, height, 20, 20));
        setResizable(false);
//        setBackground(new Color(0, 0, 0, 0));

    }

    private void reloadUser() {
        if (user != null) {
            btnLogin.setText("Logout");
            Employee emp = user.getEmployee();
            labelName.setText(emp.getFirstName() + " " + emp.getLastName());
            labelStatus.setText(emp.getTitle());
        } else {
            btnLogin.setText("Login");
            labelName.setText("");
            labelStatus.setText("");

        }
    }

    private void initLogoImg() {
        try {
            ImageIcon icon = new ImageIcon("assets/MainLogo.png");
            Image img = icon.getImage();
            Image imgScale = img.getScaledInstance(lbLogo.getWidth(), lbLogo.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon scaledIcon = new ImageIcon(imgScale);
            lbLogo.setIcon(scaledIcon);

            Image imgScaleTitle = img.getScaledInstance(lbLogoTitle.getWidth(), lbLogoTitle.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon scaledIconTitle = new ImageIcon(imgScaleTitle);
            lbLogoTitle.setIcon(scaledIconTitle);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelTop = new javax.swing.JPanel();
        btnLogin = new javax.swing.JButton();
        lbMainMenu = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lbUser = new javax.swing.JLabel();
        labelName = new javax.swing.JLabel();
        lbUser1 = new javax.swing.JLabel();
        labelStatus = new javax.swing.JLabel();
        mainView = new javax.swing.JScrollPane();
        panelTitle = new javax.swing.JPanel();
        btnExit = new RoundedButton("",new Color(255, 102, 102) , new Color(10,10,10));
        lbTitleBar = new javax.swing.JLabel();
        lbLogoTitle = new javax.swing.JLabel();
        btnHide = new RoundedButton("", new Color(255, 189, 68), new Color(10,10,10));
        lbDateTime = new javax.swing.JLabel();
        panelMenuList = new javax.swing.JPanel();
        panelLogo = new javax.swing.JPanel();
        lbLogo = new javax.swing.JLabel();
        scrPanelMenu = new javax.swing.JScrollPane();
        panelMenu = new javax.swing.JPanel();
        btnMainMenu = new javax.swing.JButton();
        btnPos = new javax.swing.JButton();
        btnStock = new javax.swing.JButton();
        btnCustomer = new javax.swing.JButton();
        btnOrder = new javax.swing.JButton();
        btnSalary = new javax.swing.JButton();
        btnUser = new javax.swing.JButton();
        btnEmp = new javax.swing.JButton();
        btnProductMangement = new javax.swing.JButton();
        btnPromotionMangement = new javax.swing.JButton();
        btnCheckTime = new javax.swing.JButton();
        btnCsRecords = new javax.swing.JButton();
        btnRcRecords = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("D-Coffee");
        setBackground(new java.awt.Color(127, 69, 51));
        setUndecorated(true);
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                formMouseDragged(evt);
            }
        });
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                formMousePressed(evt);
            }
        });

        panelTop.setBackground(new java.awt.Color(127, 69, 51));

        btnLogin.setBackground(new java.awt.Color(249, 224, 187));
        btnLogin.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnLogin.setText("Login");
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });

        lbMainMenu.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        lbMainMenu.setForeground(new java.awt.Color(255, 255, 255));
        lbMainMenu.setText("Main Menu");

        jPanel1.setBackground(new java.awt.Color(127, 69, 51));

        lbUser.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbUser.setForeground(new java.awt.Color(255, 255, 255));
        lbUser.setText("User :");

        labelName.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        labelName.setForeground(new java.awt.Color(255, 255, 255));
        labelName.setText("--");

        lbUser1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbUser1.setForeground(new java.awt.Color(255, 255, 255));
        lbUser1.setText("Status :");

        labelStatus.setFont(new java.awt.Font("TH Sarabun New", 0, 14)); // NOI18N
        labelStatus.setForeground(new java.awt.Color(255, 255, 255));
        labelStatus.setText("--");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbUser1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbUser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelName, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(167, 167, 167))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbUser)
                    .addComponent(labelName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbUser1)
                    .addComponent(labelStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(19, 19, 19))
        );

        javax.swing.GroupLayout panelTopLayout = new javax.swing.GroupLayout(panelTop);
        panelTop.setLayout(panelTopLayout);
        panelTopLayout.setHorizontalGroup(
            panelTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTopLayout.createSequentialGroup()
                .addGap(99, 99, 99)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbMainMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 176, Short.MAX_VALUE)
                .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
        );
        panelTopLayout.setVerticalGroup(
            panelTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTopLayout.createSequentialGroup()
                .addGroup(panelTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelTopLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelTopLayout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelTopLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(lbMainMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(30, Short.MAX_VALUE))
        );

        mainView.setBackground(new Color(0,0,0,0));
        mainView.setBorder(null);
        mainView.setViewportView(null);

        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        lbTitleBar.setFont(new java.awt.Font("Helvetica Neue", 1, 13)); // NOI18N
        lbTitleBar.setForeground(new java.awt.Color(255, 255, 255));
        lbTitleBar.setText("D-Coffee");

        lbLogoTitle.setBackground(new java.awt.Color(255, 204, 255));
        lbLogoTitle.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        lbLogoTitle.setForeground(new java.awt.Color(255, 255, 255));
        lbLogoTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        btnHide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHideActionPerformed(evt);
            }
        });

        lbDateTime.setText("Date ");

        javax.swing.GroupLayout panelTitleLayout = new javax.swing.GroupLayout(panelTitle);
        panelTitle.setLayout(panelTitleLayout);
        panelTitleLayout.setHorizontalGroup(
            panelTitleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelTitleLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(lbLogoTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbTitleBar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbDateTime, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnHide, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8))
        );
        panelTitleLayout.setVerticalGroup(
            panelTitleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelTitleLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelTitleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnHide, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbLogoTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelTitleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbTitleBar)
                        .addComponent(lbDateTime)))
                .addGap(5, 5, 5))
        );

        panelMenuList.setBackground(new java.awt.Color(162, 108, 62));

        panelLogo.setOpaque(false);

        lbLogo.setBackground(new java.awt.Color(255, 204, 204));
        lbLogo.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        lbLogo.setForeground(new java.awt.Color(255, 255, 255));
        lbLogo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout panelLogoLayout = new javax.swing.GroupLayout(panelLogo);
        panelLogo.setLayout(panelLogoLayout);
        panelLogoLayout.setHorizontalGroup(
            panelLogoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLogoLayout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(lbLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45))
        );
        panelLogoLayout.setVerticalGroup(
            panelLogoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLogoLayout.createSequentialGroup()
                .addContainerGap(9, Short.MAX_VALUE)
                .addComponent(lbLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        scrPanelMenu.setBorder(null);
        scrPanelMenu.setOpaque(false);

        panelMenu.setBackground(new Color(162, 108, 62));
        panelMenu.setMinimumSize(new java.awt.Dimension(194, 637));

        btnMainMenu.setBackground(new java.awt.Color(249, 224, 187));
        btnMainMenu.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnMainMenu.setText("Main Menu");
        btnMainMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMainMenuActionPerformed(evt);
            }
        });

        btnPos.setBackground(new java.awt.Color(249, 224, 187));
        btnPos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnPos.setText("POS");
        btnPos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPosActionPerformed(evt);
            }
        });

        btnStock.setBackground(new java.awt.Color(249, 224, 187));
        btnStock.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnStock.setText("Stock");
        btnStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStockActionPerformed(evt);
            }
        });

        btnCustomer.setBackground(new java.awt.Color(249, 224, 187));
        btnCustomer.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        btnCustomer.setText("Customers");
        btnCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustomerActionPerformed(evt);
            }
        });

        btnOrder.setBackground(new java.awt.Color(249, 224, 187));
        btnOrder.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        btnOrder.setText("Invoices");
        btnOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOrderActionPerformed(evt);
            }
        });

        btnSalary.setBackground(new java.awt.Color(249, 224, 187));
        btnSalary.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnSalary.setText("Salaries");
        btnSalary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalaryActionPerformed(evt);
            }
        });

        btnUser.setBackground(new java.awt.Color(249, 224, 187));
        btnUser.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        btnUser.setText("Users");
        btnUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUserActionPerformed(evt);
            }
        });

        btnEmp.setBackground(new java.awt.Color(249, 224, 187));
        btnEmp.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnEmp.setText("Employees");
        btnEmp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEmpActionPerformed(evt);
            }
        });

        btnProductMangement.setBackground(new java.awt.Color(249, 224, 187));
        btnProductMangement.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnProductMangement.setText("Products");
        btnProductMangement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProductMangementActionPerformed(evt);
            }
        });

        btnPromotionMangement.setBackground(new java.awt.Color(249, 224, 187));
        btnPromotionMangement.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnPromotionMangement.setText("Promotion");
        btnPromotionMangement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPromotionMangementActionPerformed(evt);
            }
        });

        btnCheckTime.setBackground(new java.awt.Color(249, 224, 187));
        btnCheckTime.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnCheckTime.setText("Check Time");
        btnCheckTime.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckTimeActionPerformed(evt);
            }
        });

        btnCsRecords.setBackground(new java.awt.Color(249, 224, 187));
        btnCsRecords.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnCsRecords.setText("Check Stock");
        btnCsRecords.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCsRecordsActionPerformed(evt);
            }
        });

        btnRcRecords.setBackground(new java.awt.Color(249, 224, 187));
        btnRcRecords.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnRcRecords.setText("Receipt");
        btnRcRecords.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRcRecordsActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelMenuLayout = new javax.swing.GroupLayout(panelMenu);
        panelMenu.setLayout(panelMenuLayout);
        panelMenuLayout.setHorizontalGroup(
            panelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnMainMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnPos, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnStock, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnSalary, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnUser, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnProductMangement, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnPromotionMangement, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnCheckTime, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnCsRecords, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnRcRecords, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        panelMenuLayout.setVerticalGroup(
            panelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenuLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(btnMainMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(btnPos, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(btnStock, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(btnCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(btnOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(btnSalary, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(btnUser, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(btnEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(btnProductMangement, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(btnPromotionMangement, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(btnCheckTime, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(btnCsRecords, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(btnRcRecords, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        scrPanelMenu.setViewportView(panelMenu);

        javax.swing.GroupLayout panelMenuListLayout = new javax.swing.GroupLayout(panelMenuList);
        panelMenuList.setLayout(panelMenuListLayout);
        panelMenuListLayout.setHorizontalGroup(
            panelMenuListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenuListLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(panelMenuListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelLogo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(scrPanelMenu)))
        );
        panelMenuListLayout.setVerticalGroup(
            panelMenuListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenuListLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelLogo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrPanelMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 602, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panelMenuList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(0, 0, 0)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(mainView, javax.swing.GroupLayout.DEFAULT_SIZE, 1163, Short.MAX_VALUE)
                            .addComponent(panelTop, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(panelTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panelTop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(mainView))
                    .addComponent(panelMenuList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRcRecordsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRcRecordsActionPerformed
        mainView.setViewportView(new ReceiptRecords());
        lbMainMenu.setText("Receipt Records");
    }//GEN-LAST:event_btnRcRecordsActionPerformed

    private void formMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseDragged
        setLocation(evt.getXOnScreen() - x, evt.getYOnScreen() - y);
    }//GEN-LAST:event_formMouseDragged

    private int x;
    private int y;
    private void formMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMousePressed
        x = evt.getX();
        y = evt.getY();
    }//GEN-LAST:event_formMousePressed

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        dispose();
    }//GEN-LAST:event_btnExitActionPerformed

    private void btnHideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHideActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnHideActionPerformed

    private void btnPromotionMangementActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPromotionMangementActionPerformed
        mainView.setViewportView(new PromotionManagement());
        lbMainMenu.setText("Promotion Management");
    }//GEN-LAST:event_btnPromotionMangementActionPerformed

    private void btnPosActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnPosActionPerformed
        mainView.setViewportView(new POS(user));
        lbMainMenu.setText("Point of Sale");
    }// GEN-LAST:event_btnPosActionPerformed

    private void btnMainMenuActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnMainMenuActionPerformed
        mainView.setViewportView(new Dashboard());
        lbMainMenu.setText("Main Menu");

    }

    private void btnStockActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnStockActionPerformed
        mainView.setViewportView(new StockManagement());
        lbMainMenu.setText("Stock Management");
    }// GEN-LAST:event_btnStockActionPerformed

    private void btnCustomerActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnCustomerActionPerformed
        mainView.setViewportView(new CustomerManagement());
        lbMainMenu.setText("Customer Management");
    }// GEN-LAST:event_btnCustomerActionPerformed

    private void btnOrderActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnOrderActionPerformed
        mainView.setViewportView(new InvoiceManagement());
        lbMainMenu.setText("Order Management");
    }// GEN-LAST:event_btnOrderActionPerformed

    private void btnCheckTimeActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnCheckTimeActionPerformed
        mainView.setViewportView(new CheckTimeRecords());
        lbMainMenu.setText("Check Time");
    }// GEN-LAST:event_btnCheckTimeActionPerformed

    private void btnSalaryActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnSalaryActionPerformed
        mainView.setViewportView(new SalaryManagement());
        lbMainMenu.setText("Salary Management");
    }// GEN-LAST:event_btnSalaryActionPerformed

    private void btnUserActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnUserActionPerformed
        mainView.setViewportView(new UserManagement());
        lbMainMenu.setText("User Management");
    }// GEN-LAST:event_btnUserActionPerformed

    private void btnEmpActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnEmpActionPerformed
        mainView.setViewportView(new EmployeeManagement());
        lbMainMenu.setText("Employee Management");
    }

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnLoginActionPerformed
        if (user != null) {
            int confirm = JOptionPane.showConfirmDialog(this, "Are you sure to logout ?", "Confirm Logout",
                    JOptionPane.WARNING_MESSAGE);
            if (confirm == 0) {
                user = null;
                this.dispose();
                MainFrame mainFrame = new MainFrame(user);
                mainFrame.setLocationRelativeTo(this);
                mainFrame.setVisible(true);
            }
        } else {
            newLoginFrame();
        }
    }

    private void newLoginFrame() {
        Login loginFrame = new Login(this);
        // set center of screen
        loginFrame.setLocationRelativeTo(this);
        loginFrame.setVisible(true);
        loginFrame.requestFocus();
    }

    private void btnProductMangementActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnProductMangementActionPerformed
        mainView.setViewportView(new ProductManagement());
        lbMainMenu.setText("Product Management");
    }// GEN-LAST:event_btnProductMangementActionPerformed

    private void btnCsRecordsActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnCsRecordsActionPerformed
        mainView.setViewportView(new CheckStockRecords());
        lbMainMenu.setText("Check Stock Records");
    }// GEN-LAST:event_btnCsRecordsActionPerformed

//    public static void main(String args[]) {
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        // </editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new MainFrame().setVisible(true);
//            }
//
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCheckTime;
    private javax.swing.JButton btnCsRecords;
    private javax.swing.JButton btnCustomer;
    private javax.swing.JButton btnEmp;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnHide;
    private javax.swing.JButton btnLogin;
    private javax.swing.JButton btnMainMenu;
    private javax.swing.JButton btnOrder;
    private javax.swing.JButton btnPos;
    private javax.swing.JButton btnProductMangement;
    private javax.swing.JButton btnPromotionMangement;
    private javax.swing.JButton btnRcRecords;
    private javax.swing.JButton btnSalary;
    private javax.swing.JButton btnStock;
    private javax.swing.JButton btnUser;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel labelName;
    private javax.swing.JLabel labelStatus;
    private javax.swing.JLabel lbDateTime;
    private javax.swing.JLabel lbLogo;
    private javax.swing.JLabel lbLogoTitle;
    private javax.swing.JLabel lbMainMenu;
    private javax.swing.JLabel lbTitleBar;
    private javax.swing.JLabel lbUser;
    private javax.swing.JLabel lbUser1;
    private javax.swing.JScrollPane mainView;
    private javax.swing.JPanel panelLogo;
    private javax.swing.JPanel panelMenu;
    private javax.swing.JPanel panelMenuList;
    private javax.swing.JPanel panelTitle;
    private javax.swing.JPanel panelTop;
    private javax.swing.JScrollPane scrPanelMenu;
    // End of variables declaration//GEN-END:variables

    @Override
    public void onLoginSuccess(User user, int role) {
        int allBtnMenu = 13;
        this.user = user;
        if (role != 0) {
            setRowPanleMenuLayout(allBtnMenu - 5, true);
        }
        reloadUser();
        ShortcutCheckTime sct = new ShortcutCheckTime(user);
        sct.setLocationRelativeTo(this);
        sct.setVisible(true);
    }

    private void setRowPanleMenuLayout(int rows, boolean emp) {
        if (emp) {
            JButton[] btns = {btnCustomer, btnEmp, btnSalary, btnOrder, btnUser};
            for (JButton btn : btns) {
                panelMenu.remove(btn);
            }

            panelMenu.setLayout(new GridLayout(rows, 0, 0, 6));
            panelMenu.revalidate();
            panelMenu.repaint();
        }
    }
}
