package com.simpledev.d.coffee.ui.check_time;

import com.simpledev.d.coffee.common.SearchSystem;
import com.simpledev.d.coffee.models.CheckTime;
import com.simpledev.d.coffee.models.User;
import com.simpledev.d.coffee.publisher.LoginCallback;
import com.simpledev.d.coffee.services.CheckTimeServices;
import com.simpledev.d.coffee.ui.Login;
import com.simpledev.d.coffee.ui.ShortcutCheckTime;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.function.Predicate;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;

public class CheckTimeRecords extends javax.swing.JPanel implements LoginCallback {

    private ArrayList<CheckTime> listCheckTime;
    private final CheckTimeServices checkTimeServices;
    private CheckTime editedCheckTime;
    private SearchSystem<CheckTime> searchSystem;
    private Predicate<CheckTime> filterFunction;
    private String searchValue;
    private int searchValueInt;
    private User user;

    public CheckTimeRecords() {
        initComponents();
        checkTimeServices = new CheckTimeServices();
        listCheckTime = checkTimeServices.getAll();
        initTables();
        initSearchSystem();
        Login.setListenr(this);

    }

    private void setFilter() {
        switch (searchFilter.getSelectedIndex()) {
            case 0 -> {
                // id
                filterFunction = ch -> ch.getId() == searchValueInt;
            }

            default ->
                throw new AssertionError();
        }
    }

    private int parseInputAsInt(String input) {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            // Handle invalid input here, e.g., display an error message
            return 0; // Default value or any suitable value for invalid input
        }
    }

    private void initSearchSystem() {
        searchValue = ""; // Initialize searchValue
        searchValueInt = 0; // Initialize searchValueInt
        setFilter(); // Initialize the filter function
        searchSystem = new SearchSystem<>(searchBar, listCheckTime);

        searchBar.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // Handle changedUpdate if needed
            }
        });
    }

    private void updateSearchAndReloadTables() {
        searchValue = searchBar.getText();
        searchValueInt = parseInputAsInt(searchValue);
        setFilter(); // Update the filter function
        searchSystem.update(searchValue, filterFunction);
        reloadTables();
    }

    private void initTables() {
        String[] filterOptions = {"ch id"};
        searchFilter.setModel(new DefaultComboBoxModel<>(filterOptions));
        searchFilter.setSelectedIndex(0);

        tableCheckTimeList.setModel(new AbstractTableModel() {
            String[] headers = {
                "Checktime id",
                "Date",
                "Employee id",
                "Checkin",
                "Checkout",
                "Total Worked",
                "Type"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return listCheckTime.size();
            }

            @Override
            public int getColumnCount() {
                return headers.length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckTime che = listCheckTime.get(rowIndex);

                switch (columnIndex) {
                    case 0:
                        return che.getId();
                    case 1:
                        return che.getDate();
                    case 2:
                        return che.getEmployee().getId();
                    case 3:
                        return che.getCheckIn();
                    case 4:
                        return che.getCheckOut();
                    case 5:
                        return che.getTotalWorked();
                    case 6:
                        return che.getType();
                    default:
                        return "unknow";
                }
            }

        });
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        checkTimeHead = new javax.swing.JPanel();
        searchBar = new javax.swing.JTextField();
        searchFilter = new javax.swing.JComboBox<>();
        btnAdd = new javax.swing.JButton();
        btnRegister = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableCheckTimeList = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 204, 204));

        checkTimeHead.setBackground(new java.awt.Color(255, 153, 153));

        searchFilter.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnRegister.setText("Register");
        btnRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout checkTimeHeadLayout = new javax.swing.GroupLayout(checkTimeHead);
        checkTimeHead.setLayout(checkTimeHeadLayout);
        checkTimeHeadLayout.setHorizontalGroup(
            checkTimeHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, checkTimeHeadLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(searchBar, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnRegister)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAdd)
                .addContainerGap())
        );
        checkTimeHeadLayout.setVerticalGroup(
            checkTimeHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(checkTimeHeadLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(checkTimeHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAdd)
                    .addComponent(btnRegister))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(tableCheckTimeList);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(checkTimeHead, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 946, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(checkTimeHead, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 505, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        editedCheckTime = new CheckTime(); // create new user
        openDialog();
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnRegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterActionPerformed
//        new ShortcutCheckTime(user).setVisible(true);
        ShortcutCheckTime sct = new ShortcutCheckTime(user);
        sct.setLocationRelativeTo(this);
        sct.setVisible(true);
    }//GEN-LAST:event_btnRegisterActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnRegister;
    private javax.swing.JPanel checkTimeHead;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField searchBar;
    private javax.swing.JComboBox<String> searchFilter;
    private javax.swing.JTable tableCheckTimeList;
    // End of variables declaration//GEN-END:variables

    private void reloadTables() {
        listCheckTime = searchSystem.getResults();
        tableCheckTimeList.validate();
        tableCheckTimeList.repaint();
    }

    private void refreshTables() {
        listCheckTime = checkTimeServices.getAll();
        tableCheckTimeList.revalidate();
        tableCheckTimeList.repaint();
    }

    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        CheckTimeDialog checkTimeDialog = new CheckTimeDialog(frame, editedCheckTime);
        checkTimeDialog.setTitle("CheckTime In-Out");
        checkTimeDialog.setLocationRelativeTo(this);
        checkTimeDialog.setVisible(true);
        checkTimeDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTables();
            }
        });
    }

    @Override
    public void onLoginSuccess(User user, int role) {
        this.user = user;
        System.out.println(user);
    }

}
