package com.simpledev.d.coffee.ui;

import com.simpledev.d.coffee.models.User;
import com.simpledev.d.coffee.publisher.LoginCallback;
import javax.swing.SwingUtilities;

public class MainApp implements LoginCallback {

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        SwingUtilities.invokeLater(() -> {
            MainApp mainApp = new MainApp();
            new Login(mainApp).setVisible(true);
        });
    }

    @Override
    public void onLoginSuccess(User user, int role) {
        MainFrame mf = new MainFrame(user);
        mf.setLocationRelativeTo(null);
        mf.setVisible(true);
        ShortcutCheckTime sct = new ShortcutCheckTime(user);
        sct.setLocationRelativeTo(null);
        sct.setVisible(true);

    }
}
