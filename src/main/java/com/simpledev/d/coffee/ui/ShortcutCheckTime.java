package com.simpledev.d.coffee.ui;

import com.simpledev.d.coffee.common.CommonDateTime;
import com.simpledev.d.coffee.component.RoundedButton;
import com.simpledev.d.coffee.models.CheckTime;
import com.simpledev.d.coffee.models.User;
import com.simpledev.d.coffee.services.CheckTimeServices;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.RoundRectangle2D;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class ShortcutCheckTime extends javax.swing.JFrame {

    private final CheckTimeServices checkTimeServices = new CheckTimeServices();
    private LocalTime timeCheckIn;
    private LocalTime timeCheckOut;
    private Duration workedHours = Duration.ZERO;
    private final User user;

    public ShortcutCheckTime(User user) {
        initFrame();
        initComponents();
        initCustomComponents();
        this.user = user;
    }

    private void initCustomComponents() {
        String text = "<html>Your Time Check : <font color='red'>" + CommonDateTime.getDateTimeNowString() + "</font></html>";
        lbTime.setText(text);
        pnlBG.setBackground(new Color(30, 13, 3));
        pnlButtons.setOpaque(false);
    }

    private void initFrame() {
        final int width = 455;
        final int height = 141;
        setPreferredSize(new Dimension(width, height));
        setUndecorated(true);
        setShape(new RoundRectangle2D.Double(0, 0, width, height, 20, 20));
        setResizable(false);
        setBackground(new Color(30, 13, 3));

    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlBG = new javax.swing.JPanel();
        pnlButtons = new javax.swing.JPanel();
        btnCheckIn = new RoundedButton("Check In",
            new Color(129, 192, 70,90), Color.white);
        btnLater = new RoundedButton("Later",
            new Color(59, 119, 188,95), Color.white);
        btnCheckOut = new RoundedButton("Check Out",
            new Color(222, 72, 43,95), Color.white);
        lbTime = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pnlButtons.setLayout(new java.awt.GridLayout(1, 3, 8, 0));

        btnCheckIn.setText("Check In");
        btnCheckIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckInActionPerformed(evt);
            }
        });
        pnlButtons.add(btnCheckIn);

        btnLater.setText("Later");
        btnLater.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLaterActionPerformed(evt);
            }
        });
        pnlButtons.add(btnLater);

        btnCheckOut.setText("Check Out");
        btnCheckOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckOutActionPerformed(evt);
            }
        });
        pnlButtons.add(btnCheckOut);

        lbTime.setFont(new java.awt.Font("Helvetica Neue", 0, 18)); // NOI18N
        lbTime.setForeground(new java.awt.Color(255, 255, 255));
        lbTime.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTime.setText("Time :");

        javax.swing.GroupLayout pnlBGLayout = new javax.swing.GroupLayout(pnlBG);
        pnlBG.setLayout(pnlBGLayout);
        pnlBGLayout.setHorizontalGroup(
            pnlBGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBGLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(pnlBGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pnlButtons, javax.swing.GroupLayout.DEFAULT_SIZE, 419, Short.MAX_VALUE)
                    .addComponent(lbTime, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18))
        );
        pnlBGLayout.setVerticalGroup(
            pnlBGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBGLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(lbTime)
                .addGap(24, 24, 24)
                .addComponent(pnlButtons, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBG, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBG, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCheckInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckInActionPerformed

        checkTimeServices.addNew(new CheckTime(CommonDateTime.getDateNowString(), user.getEmployee(), CommonDateTime.getTimeNowString(), "", 0, ""));
        System.out.println("check in!");

        dispose();
    }//GEN-LAST:event_btnCheckInActionPerformed

    private void btnLaterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLaterActionPerformed
        dispose();
    }//GEN-LAST:event_btnLaterActionPerformed

    private void btnCheckOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckOutActionPerformed
        CheckTime selectedCheckTime = checkTimeServices.getByEmpId(this.user.getEmployee().getId());
        String timeCheckInString = selectedCheckTime.getCheckIn();
        String timeCheckOutString = CommonDateTime.getTimeNowString();
        String type;
        timeCheckIn = LocalTime.parse(timeCheckInString, CommonDateTime.getTimeFormatter());
        timeCheckOut = LocalTime.parse(timeCheckOutString, CommonDateTime.getTimeFormatter());
        workedHours = Duration.between(timeCheckIn, timeCheckOut);
        int totalWorked = workedHours.toHoursPart();
        if (totalWorked > 7) {
            type = "normal";
        } else if (totalWorked > 9) {
            type = "normal+ot";
        } else {
            type = "missing";
        }
        if (this.user != null) {
            checkTimeServices.updateCheckOut(user.getEmployee(), timeCheckOutString, totalWorked, type);
            System.out.println("check out!");
        }
        dispose();
    }//GEN-LAST:event_btnCheckOutActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCheckIn;
    private javax.swing.JButton btnCheckOut;
    private javax.swing.JButton btnLater;
    private javax.swing.JLabel lbTime;
    private javax.swing.JPanel pnlBG;
    private javax.swing.JPanel pnlButtons;
    // End of variables declaration//GEN-END:variables

}
