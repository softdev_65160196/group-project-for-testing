package com.simpledev.d.coffee.ui.user;

import com.simpledev.d.coffee.common.SearchSystem;
import com.simpledev.d.coffee.models.Employee;
import com.simpledev.d.coffee.models.User;
import com.simpledev.d.coffee.services.EmployeeServices;
import com.simpledev.d.coffee.services.UserServices;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.function.Predicate;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;

public class UserManagement extends javax.swing.JPanel {

    private ArrayList<User> users;
    private final UserServices userServices = new UserServices();
    private final EmployeeServices employeeServices = new EmployeeServices();
    private Predicate<User> filterFunction;
    private SearchSystem<User> searchSystem;
    private String searchValue;
    private int searchValueInt;
    private boolean editMode;
    private User newUser;
    private User selectUser;

    public UserManagement() {
        initComponents();
        users = (ArrayList<User>) userServices.getAll();
        initComboBox();
        initTables();
        initSearchSystem();
    }

    private void initComboBox() {
        ArrayList<Employee> listEmp = employeeServices.getAll();
        ArrayList<String> listForComboBox = new ArrayList<>();

        for (Employee emp : listEmp) {
            String rest = "ID:" + emp.getId() + " " + emp.getFirstName() + " " + emp.getLastName();
            listForComboBox.add(rest);
        }
        cmbEmp.setModel(new DefaultComboBoxModel<>(listForComboBox.toArray(String[]::new)));
    }

    private void setFilter() {
        switch (searchFilter.getSelectedIndex()) {
            case 0 -> {
                // Username
                filterFunction = user -> user.getLogin().contains(searchValue);
            }
            case 1 -> {
                // User ID
                filterFunction = user -> user.getId() == searchValueInt;
            }
            case 2 -> {
                // Employee ID
                filterFunction = user -> user.getEmployee().getId() == searchValueInt;
            }
            default ->
                throw new AssertionError();
        }
    }

    private int parseInputAsInt(String input) {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            // Handle invalid input here, e.g., display an error message
            return 0; // Default value or any suitable value for invalid input
        }
    }

    private void reloadTables() {
        users = searchSystem.getResults();
        tableUserLIst.revalidate();
        tableUserLIst.repaint();
    }

    private void refreshTables() {
        users = userServices.getAll();
        tableUserLIst.revalidate();
        tableUserLIst.repaint();
    }

    private void initSearchSystem() {
        searchValue = ""; // Initialize searchValue
        searchValueInt = 0; // Initialize searchValueInt
        setFilter(); // Initialize the filter function
        searchSystem = new SearchSystem<>(searchBar, users);

        searchBar.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // Handle changedUpdate if needed
            }
        });
    }

    private void updateSearchAndReloadTables() {
        searchValue = searchBar.getText().toLowerCase();
        searchValueInt = parseInputAsInt(searchValue);
        setFilter(); // Update the filter function
        searchSystem.update(searchValue, filterFunction);
        reloadTables();
    }

    private void initTables() {
        String[] filterOptions = {"username", "user_id", "emp_id"};
        searchFilter.setModel(new DefaultComboBoxModel<>(filterOptions));
        searchFilter.setSelectedIndex(0);
        //set font if need
//        tableUserLIst.setFont(CommonFont.getFont());
//        JTableHeader header = tableUserLIst.getTableHeader();
//        header.setFont(CommonFont.getFont());
        tableUserLIst.setModel(new AbstractTableModel() {
            String[] headers = {"ID", "EmployeeID", "Title", "Username", "Password"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return users.size();
            }

            @Override
            public int getColumnCount() {
                return headers.length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                User user = users.get(rowIndex);
                return switch (columnIndex) {
                    case 0 ->
                        user.getId();
                    case 1 ->
                        user.getEmployee().getId();
                    case 2 ->
                        user.getTitle();
                    case 3 ->
                        user.getLogin();
                    case 4 ->
                        user.getPassword();
                    default ->
                        "unknow";
                };

            }

        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dlAddUser = new javax.swing.JDialog();
        lbUser = new javax.swing.JLabel();
        btnOK = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        lbPassword = new javax.swing.JLabel();
        tfUser = new javax.swing.JTextField();
        lbEmp = new javax.swing.JLabel();
        cmbEmp = new javax.swing.JComboBox<>();
        tfPassword = new javax.swing.JPasswordField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableUserLIst = new javax.swing.JTable();
        btnAdd = new javax.swing.JButton();
        searchBar = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        btnEdit = new javax.swing.JButton();
        btnDel = new javax.swing.JButton();
        searchFilter = new javax.swing.JComboBox<>();

        lbUser.setText("Username");

        btnOK.setText("Save");
        btnOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOKActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        lbPassword.setText("Password");

        lbEmp.setText("Employee");

        cmbEmp.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout dlAddUserLayout = new javax.swing.GroupLayout(dlAddUser.getContentPane());
        dlAddUser.getContentPane().setLayout(dlAddUserLayout);
        dlAddUserLayout.setHorizontalGroup(
            dlAddUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlAddUserLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnOK)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCancel)
                .addGap(27, 27, 27))
            .addGroup(dlAddUserLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(dlAddUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbPassword)
                    .addComponent(lbUser)
                    .addComponent(lbEmp))
                .addGap(76, 76, 76)
                .addGroup(dlAddUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tfUser, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
                    .addComponent(cmbEmp, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tfPassword))
                .addGap(18, 18, 18))
        );
        dlAddUserLayout.setVerticalGroup(
            dlAddUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlAddUserLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(dlAddUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbUser)
                    .addComponent(tfUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(dlAddUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbPassword)
                    .addComponent(tfPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(dlAddUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbEmp)
                    .addComponent(cmbEmp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(dlAddUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnOK)
                    .addComponent(btnCancel))
                .addGap(18, 18, 18))
        );

        setBackground(new java.awt.Color(255, 204, 204));

        tableUserLIst.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "User ID", "User Name", "Tel", "Type", "Login Time", "Logout Time", "Date in"
            }
        ));
        jScrollPane1.setViewportView(tableUserLIst);

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        searchBar.setForeground(new java.awt.Color(0, 0, 0));
        searchBar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchBarActionPerformed(evt);
            }
        });

        jLabel1.setText("User Search");

        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnDel.setText("Delete");
        btnDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelActionPerformed(evt);
            }
        });

        searchFilter.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        searchFilter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchFilterActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 917, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(searchBar, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAdd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEdit)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDel)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(searchBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(btnEdit)
                    .addComponent(btnDel)
                    .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 477, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        editMode = false;
        newUser = new User();
        tfUser.setText("");
        tfPassword.setText("");
        cmbEmp.setSelectedIndex(-1);
        newDialogProcessUser();

    }//GEN-LAST:event_btnAddActionPerformed

    private void newDialogProcessUser() {
        dlAddUser.setTitle("Add User");
        dlAddUser.setPreferredSize(new Dimension(307, 170));
        dlAddUser.setBounds(this.getWidth(), this.getHeight(), 307, 250);
        dlAddUser.setLocationRelativeTo(this);
        dlAddUser.setVisible(true);
        dlAddUser.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTables();
            }
        });
    }

    private void searchBarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchBarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchBarActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        editMode = true;
        selectUser = users.get(tableUserLIst.getSelectedRow());
        if (selectUser != null) {
            tfUser.setText(selectUser.getLogin());
            tfPassword.setText(selectUser.getPassword());
            cmbEmp.setSelectedIndex(selectUser.getEmployee().getId() - 1);
        } else {
            JOptionPane.showMessageDialog(dlAddUser, "Please select a row first !", "Error!", JOptionPane.ERROR);
        }
        newDialogProcessUser();
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelActionPerformed
        if (tableUserLIst.getSelectedRow() >= 0) {
            User editedUser = users.get(tableUserLIst.getSelectedRow());
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                userServices.delete(editedUser);
                users = userServices.getAll();
                refreshTables();
            }
        }
    }//GEN-LAST:event_btnDelActionPerformed

    private void btnOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOKActionPerformed
        Employee emp = employeeServices.getById(cmbEmp.getSelectedIndex());
        char[] passwordChars = tfPassword.getPassword();
        String password = new String(passwordChars);

        if (editMode) {
            if (selectUser != null) {
                selectUser.setLogin(tfUser.getText());
                selectUser.setPassword(password);
                selectUser.setEmployee(emp);
                selectUser.setTitle(emp.getTitle());
                userServices.update(selectUser);
                users = userServices.getAll();
            }
        } else {
            if (newUser != null) {
                newUser.setLogin(tfUser.getText());
                newUser.setPassword(password);
                newUser.setEmployee(emp);
                newUser.setTitle(emp.getTitle());
                userServices.addNew(newUser);
                users = userServices.getAll();
            } else {
                JOptionPane.showMessageDialog(dlAddUser, "Please enter valid data !", "Error!", JOptionPane.ERROR);
            }
        }
        dlAddUser.dispose();
    }//GEN-LAST:event_btnOKActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        dlAddUser.dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void searchFilterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchFilterActionPerformed
        setFilter();
    }//GEN-LAST:event_searchFilterActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDel;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnOK;
    private javax.swing.JComboBox<String> cmbEmp;
    private javax.swing.JDialog dlAddUser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbEmp;
    private javax.swing.JLabel lbPassword;
    private javax.swing.JLabel lbUser;
    private javax.swing.JTextField searchBar;
    private javax.swing.JComboBox<String> searchFilter;
    private javax.swing.JTable tableUserLIst;
    private javax.swing.JPasswordField tfPassword;
    private javax.swing.JTextField tfUser;
    // End of variables declaration//GEN-END:variables
}
