/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.simpledev.d.coffee.ui.stock;

import com.simpledev.d.coffee.common.CommonFont;
import com.simpledev.d.coffee.common.SearchSystem;
import com.simpledev.d.coffee.models.CheckStock;
import com.simpledev.d.coffee.models.Customer;
import com.simpledev.d.coffee.models.Employee;
import com.simpledev.d.coffee.models.Product;
import com.simpledev.d.coffee.models.Promotion;
import com.simpledev.d.coffee.services.CheckStockServices;
import com.simpledev.d.coffee.services.CustomerServices;
import com.simpledev.d.coffee.services.EmployeeServices;
import com.simpledev.d.coffee.ui.stock.CheckStockDialog;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.function.Predicate;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;

/**
 *
 * @author Lenovo
 */
public class CheckStockRecords extends javax.swing.JPanel {
    
    private final CheckStockServices checkStockServices;
    private ArrayList<CheckStock> checkStockList;
    private CheckStock editedCheckStock;
    private Predicate<CheckStock> filterFunction;
    private SearchSystem<CheckStock> searchSystem;
    private String searchValue;
    private int searchValueInt;

    /**
     * Creates new form CheckStockRecords
     */
    public CheckStockRecords() {
        initComponents();
        checkStockServices = new CheckStockServices();
        checkStockList = checkStockServices.getAll();
        initTables();
        initSearchSystem();
        tblCSRecords.setFont(CommonFont.getFont());
        panelHead.setFont(CommonFont.getFont());
        JTableHeader header = tblCSRecords.getTableHeader();
        header.setFont(CommonFont.getFont());
    }

    private void setFilter() {
        switch (searchFilter.getSelectedIndex()) {
            
            case 0 -> {
                // ID
                filterFunction = cst -> cst.getId() == searchValueInt;
            }
            case 1 -> {
                // Date
                filterFunction = cst -> cst.getDate().contains(searchValue);
            }
            default ->
                throw new AssertionError();
        }
    }

    private int parseInputAsInt(String input) {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            // Handle invalid input here, e.g., display an error message
            return 0; // Default value or any suitable value for invalid input
        }
    }

    private void initSearchSystem() {
        searchValue = ""; // Initialize searchValue
        searchValueInt = 0; // Initialize searchValueInt
        setFilter(); // Initialize the filter function
        searchSystem = new SearchSystem<>(searchBar, checkStockList);

        searchBar.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // Handle changedUpdate if needed
            }
        });
    }

    private void updateSearchAndReloadTables() {
        searchValue = searchBar.getText().toLowerCase();
        searchValueInt = parseInputAsInt(searchValue);
        setFilter(); // Update the filter function
        searchSystem.update(searchValue, filterFunction);
        reloadTables();
    }

    private void reloadTables() {
        checkStockList = searchSystem.getResults();
        tblCSRecords.revalidate();
        tblCSRecords.repaint();
    }
    
    private void refreshTables() {
        checkStockList = checkStockServices.getAll();
        tblCSRecords.revalidate();
        tblCSRecords.repaint();    
    }

    private void initTables() {
        String[] filterOptions = {"Id", "Date"};
        searchFilter.setModel(new DefaultComboBoxModel<>(filterOptions));
        searchFilter.setSelectedIndex(0);

        tblCSRecords.setModel(new AbstractTableModel() {
            String[] Header = {"ID", "Employee ID", "Description", "Value Loss", "Unit Loss", "Total Unit", "Date"};

            @Override
            public String getColumnName(int column) {
                return Header[column];
            }

            @Override
            public int getRowCount() {
                return checkStockList.size();
            }

            @Override
            public int getColumnCount() {
                return Header.length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckStock checkStock = checkStockList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return checkStock.getId();
                    case 1:
                        return checkStock.getEmployee().getId();
                    case 2:
                        return checkStock.getDescription();
                    case 3:
                        return checkStock.getValueLoss();
                    case 4:
                        return checkStock.getUnitLoss();
                    case 5:
                        return checkStock.getTotalUnit();
                    case 6:
                        return checkStock.getDate();
                    default:
                        return null;
                }
            }

        });
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelHead = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        btnProductAdd = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        searchBar = new javax.swing.JTextField();
        searchFilter = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCSRecords = new javax.swing.JTable();

        panelHead.setBackground(new java.awt.Color(255, 204, 102));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Checkstock Search");

        btnProductAdd.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnProductAdd.setText("Add");
        btnProductAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProductAddActionPerformed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnEdit.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        searchFilter.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout panelHeadLayout = new javax.swing.GroupLayout(panelHead);
        panelHead.setLayout(panelHeadLayout);
        panelHeadLayout.setHorizontalGroup(
            panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHeadLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelHeadLayout.createSequentialGroup()
                        .addComponent(searchBar, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnProductAdd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEdit))
                    .addGroup(panelHeadLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(0, 803, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelHeadLayout.setVerticalGroup(
            panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHeadLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(searchBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnDelete)
                        .addComponent(btnProductAdd)
                        .addComponent(btnEdit)))
                .addContainerGap(7, Short.MAX_VALUE))
        );

        tblCSRecords.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "No.", "Stock ID", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblCSRecords);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelHead, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 948, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelHead, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnProductAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProductAddActionPerformed
        editedCheckStock = new CheckStock(); //create new user
        openDialog();
    }//GEN-LAST:event_btnProductAddActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedIndex = tblCSRecords.getSelectedRow();
        if (selectedIndex >= 0) {
            editedCheckStock = checkStockList.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                checkStockServices.delete(editedCheckStock);
            }
            refreshTables();
            checkStockServices.delete(editedCheckStock);
        }
        refreshTables();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        int selectedIndex = tblCSRecords.getSelectedRow();
        if (selectedIndex >= 0) {
            editedCheckStock = checkStockList.get(selectedIndex);
            openDialog();

        }
        refreshTables();
    }//GEN-LAST:event_btnEditActionPerformed
    
    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        CheckStockDialog checkStockDialog = new CheckStockDialog(frame, editedCheckStock);
        checkStockDialog.setTitle("Check Stock");
        checkStockDialog.setLocationRelativeTo(this);
        checkStockDialog.setVisible(true);
        checkStockDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTables();
            }

        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnProductAdd;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panelHead;
    private javax.swing.JTextField searchBar;
    private javax.swing.JComboBox<String> searchFilter;
    private javax.swing.JTable tblCSRecords;
    // End of variables declaration//GEN-END:variables
}
