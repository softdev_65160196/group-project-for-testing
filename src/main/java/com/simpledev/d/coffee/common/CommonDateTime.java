package com.simpledev.d.coffee.common;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class CommonDateTime {

    private static final DateTimeFormatter STANDARD_FORMATTER = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy/MM/dd");
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss");
    private static final LocalDateTime currentDateTime = LocalDateTime.now();

    public static DateTimeFormatter getFormatter() {
        return STANDARD_FORMATTER;
    }

    public static DateTimeFormatter getDateFormatter() {
        return DATE_FORMATTER;
    }

    public static DateTimeFormatter getTimeFormatter() {
        return TIME_FORMATTER;
    }
    
    public static LocalDateTime stringToDateTime(String dateString) {
        try {
            return LocalDateTime.parse(dateString, STANDARD_FORMATTER);
        } catch (DateTimeParseException e) {
            // Handle the exception or log an error.
            System.out.println(e);
            return null; // Return null as a sign of failure.
        }
    }

    public static String dateTimeToString(LocalDateTime dateTime) {
        return dateTime.format(STANDARD_FORMATTER);
    }

    public static String getDateTimeNowString() {
        return currentDateTime.format(STANDARD_FORMATTER);
    }

    public static String getDateNowString() {
        return currentDateTime.format(DATE_FORMATTER);
    }

    public static String getTimeNowString() {
        return currentDateTime.format(TIME_FORMATTER);
    }

}
