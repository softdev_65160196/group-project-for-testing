package com.simpledev.d.coffee.common;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CommonFont {

    public static Font getFont() {
        // change font here
        try {
            Font customFont = java.awt.Font.createFont(Font.TRUETYPE_FONT, new File("font/db_adman_x.ttf"));
            return customFont.deriveFont(java.awt.Font.PLAIN, 16); // Change font style and size as needed
        } catch (FontFormatException | IOException ex) {
            Logger.getLogger(CommonFont.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Font getFontTitle() {
        // change font here
        try {
            Font customFont = java.awt.Font.createFont(Font.TRUETYPE_FONT, new File("font/db_adman_x.ttf"));
            return customFont.deriveFont(java.awt.Font.PLAIN, 24); // Change font style and size as needed
        } catch (FontFormatException | IOException ex) {
            Logger.getLogger(CommonFont.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Font getFont2() {
        // change font here
        try {
            Font customFont = java.awt.Font.createFont(Font.TRUETYPE_FONT, new File("font/THSarabunNew.ttf"));
            return customFont.deriveFont(java.awt.Font.PLAIN, 16); // Change font style and size as needed
        } catch (FontFormatException | IOException ex) {
            Logger.getLogger(CommonFont.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
