package com.simpledev.d.coffee.common;

import java.util.ArrayList;
import java.util.function.Predicate;
import javax.swing.JTextField;

public class SearchSystem<T> {

    private ArrayList<T> current;
    private final ArrayList<T> dataList;

    public SearchSystem(JTextField component, ArrayList<T> dataList) {
        this.dataList = dataList;
        this.current = dataList;
    }

    public void update(String searchName, Predicate<T> localFunc) {
        current = fetchResult(searchName, localFunc);
        // Notify listeners or update your UI components with the new search results
    }

    private ArrayList<T> fetchResult(String searchName, Predicate<T> localFunc) {
        if (searchName.isEmpty()) {
            return dataList;
        } else {
            ArrayList<T> filtered = new ArrayList<>();
            for (T item : dataList) {
                if (localFunc.test(item)) {
                    filtered.add(item);
                }
            }
            return filtered;
        }
    }

    public ArrayList<T> getResults() {
        return current;
    }
}
